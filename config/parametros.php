<?php

return [
    'url_template_admin' => env('APP_URL', 'http://localhost') . '/templates/admin',
    'tipo_marca_llanta' => 'llanta',
    'tipo_marca_vehiculo' => 'vehiculo',
    'path_plantillas_mailing' => 'views/admin/mailing/plantillas',
    'email_contacto' => env('EMAIL_CONTACTO', 'info@tototire.com'),
];
