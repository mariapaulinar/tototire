@extends('admin.layout.master')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3>Bienvenid@ <strong>{{ auth()->user()->name }}</strong>.</h3> 
            </div>
        </div>
    </div>
</div>
@endsection