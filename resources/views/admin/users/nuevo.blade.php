@extends('admin.layout.master')

@section('titulo')
Nuevo usuario
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.users.nuevo.guardar') }}" method="post">
                        @include('admin.users.campos', ['user' => null])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection