<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
        <title>Tototire - Creación de contraseña</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ config('parametros.url_template_admin') }}/css/helper.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/style.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
        <!--[if lt IE 9]>
        <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-header fix-sidebar">
        <!-- Preloader - style you can find in spinners.css -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">
            <div class="unix-login">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        @if(session()->has('ok') && !session()->get('ok'))
                        <div class="col-12 alert alert-{{ (session()->get('ok')) ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 style="text-align: center;">{{ session()->get('mensaje') }}</h4>
                        </div>
                        @endif
                        <div class="col-lg-4">
                            <div class="login-content card">
                                <div class="login-form">
                                    <h4>Crear contraseña</h4>
                                    <form method="post" action="{{ route('admin.users.crear-contrasena.guardar-contrasena') }}">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="token" value="{{ $token }}">
                                        <div class="form-group{{ ($errors->has('email')) ? ' has-error' : '' }}">
                                            <label>Correo electrónico</label>
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Correo electrónico registrado">
                                            @if($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ ($errors->has('password')) ? ' has-error' : '' }}">
                                            <label>Contraseña</label>
                                            <input type="password" name="password" class="form-control" placeholder="Contraseña" autocomplete="off">
                                            @if($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmar contraseña</label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirme la contraseña" autocomplete="off">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Crear contraseña</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Wrapper -->
        <!-- All Jquery -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap/js/popper.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/jquery.slimscroll.js"></script>
        <!--Menu sidebar -->
        <script src="{{ config('parametros.url_template_admin') }}/js/sidebarmenu.js"></script>
        <!--stickey kit -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
        <!--Custom JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/custom.min.js"></script>
    </body>
</html>