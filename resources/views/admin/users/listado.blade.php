@extends('admin.layout.master')

@section('titulo')
Usuarios
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-6">
                        <label for="perfil">Filtrar por perfil</label>
                        <select name="perfil" id="perfil" class="form-control">
                            <option value="">Todos</option>
                            <option value="cliente">Cliente</option>
                            <option value="vendedor">Vendedor</option>
                            <option value="admin">Administrador</option>
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="estado">Filtrar por estado</label>
                        <select name="estado" id="estado" class="form-control">
                            <option value="">Todos</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
$(document).ready(function () {
    $("#dataTableBuilder").on('preXhr.dt', function(e, settings, data) {
        data.perfil = $("#perfil").val();
        data.estado = $("#estado").val();
    });
    
    $("#perfil").on("change", function () {
        window.LaravelDataTables["dataTableBuilder"].draw();
    });
    
    $("#estado").on("change", function () {
        window.LaravelDataTables["dataTableBuilder"].draw();
    });
});
</script>
@endsection