{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-6">
        <label>Nombres</label>
        <input type="text" name="name" class="form-control" placeholder="Nombre completo del usuario" maxlength="191" value="{{ ($user) ? $user->name : '' }}" required>
    </div>
    <div class="form-group col-6">
        <label>Email</label>
        <input type="text" name="email" class="form-control" placeholder="Correo electrónico" maxlength="191" value="{{ ($user) ? $user->email : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <label>Dirección</label>
        <input type="text" name="direccion" class="form-control" placeholder="Dirección" maxlength="255" value="{{ ($user) ? $user->direccion : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-3">
        <label>Fecha de nacimiento (Año-mes-día)</label>
        <input type="text" name="fecha_nacimiento" class="form-control datepicker mask-fecha" placeholder="Fecha de nacimiento" maxlength="10" value="{{ ($user) ? $user->fecha_nacimiento : '' }}">
    </div>
    <div class="form-group col-3">
        <label>Sexo</label>
        <select name="sexo" class="form-control">
            <option value="M"{{ ($user && $user->sexo == 'M') ? ' selected' : '' }}>Masculino</option>
            <option value="F"{{ ($user && $user->sexo == 'F') ? ' selected' : '' }}>Femenino</option>
        </select>
    </div>
    <div class="form-group col-3">
        <label>Teléfono 1</label>
        <input type="text" name="telefono1" class="form-control" placeholder="Teléfono 1" maxlength="15" value="{{ ($user) ? $user->telefono1 : '' }}" required>
    </div>
    <div class="form-group col-3">
        <label>Teléfono 2</label>
        <input type="text" name="telefono2" class="form-control" placeholder="Teléfono 2" maxlength="15" value="{{ ($user) ? $user->telefono2 : '' }}">
    </div>
</div>
<div class="row">
    <div class="form-group col-4">
        <label>Estado</label>
        @include('admin.layout.estados', ['estado_seleccionado' => ($user) ? $user->estado : null])
    </div>
    <div class="form-group col-4">
        <label>Perfil</label>
        <select name="perfil" id="perfil" class="form-control" required>
            <option value="cliente"{{ ($user && $user->perfil == 'cliente') ? ' selected' : '' }}>Cliente</option>
            <option value="vendedor"{{ ($user && $user->perfil == 'vendedor') ? ' selected' : '' }}>Vendedor</option>
            <option value="admin"{{ ($user && $user->perfil == 'admin') ? ' selected' : '' }}>Administrador</option>
        </select>
    </div>
    <div class="form-group col-4" id="div_cliente"{!! ($user && $user->perfil != 'cliente') ? ' style="display: none;"' : '' !!}>
        <label for="cliente_id">Cliente</label>
        <select name="cliente_id" id="cliente_id" class="form-control" required>
            @foreach($clientes as $cliente)
            @php
            $selected = ($user && $user->cliente_id == $cliente->id) ? ' selected' : '';
            @endphp
            <option value="{{ $cliente->id }}"{{ $selected }}>{{ $cliente->razon_social }}</option>
            @endforeach
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
<button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.users.listado') }}";
    });
    
    $("#tipo_ident").on("change", function () {
        $("#digito_verif").val("");
        if($(this).val() != "nit") {
            $("#div_digito_verif").attr("style", "display: none");
        } else {
            $("#div_digito_verif").removeAttr("style");
        }
    });
    
    $("#perfil").on("change", function () {
        if($(this).val() != "cliente") {
            $("#div_cliente").attr("style", "display: none");
        } else {
            $("#div_cliente").removeAttr("style");
        }
    });
});
</script>
@endsection