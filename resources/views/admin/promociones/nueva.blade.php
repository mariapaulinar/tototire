@extends('admin.layout.master')

@section('titulo')
Nueva promocion
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.promociones.nueva.guardar') }}" method="post" enctype="multipart/form-data">
                        @include('admin.promociones.campos', ['promocion' => null])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.promociones.listado') }}";
    });
});
</script>
@endsection