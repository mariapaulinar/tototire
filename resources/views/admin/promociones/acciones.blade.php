<a href="{{ route('admin.promociones.editar', [$promocion->id]) }}" data-toggle="tooltip" data-placement="top" title="Editar este registro"><i class="fa fa-pencil"></i></a>
<a href="{{ route('admin.promociones.clientes', [$promocion->id]) }}" data-toggle="tooltip" data-placement="top" title="Asignar clientes"><i class="fa fa-user-circle-o"></i></a>
<a href="{{ route('admin.promociones.mailing', [$promocion->id]) }}" data-toggle="tooltip" data-placement="top" title="Mailing"><i class="fa fa-envelope-o"></i></a>
