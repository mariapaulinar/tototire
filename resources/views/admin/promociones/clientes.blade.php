@extends('admin.layout.master')
@section('css')
<style>
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        background-color: #dee2e694;
        border-color: #dee2e6 #dee2e6 #dee2e6;
    }
    
    .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
        background-color: #dee2e694;
        border-color: #e9ecef #e9ecef #dee2e6;
    }
</style>
@endsection
@section('titulo')
Clientes de la promoción
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link{{ (!request()->has('registrados')) ? ' active' : '' }}" data-toggle="tab" href="#clientes-promocion" role="tab"><h3>Aplicados a la promoción</h3></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link{{ (request()->has('registrados')) ? ' active' : '' }}" data-toggle="tab" href="#segmentacion" role="tab"><h3>Agregar por segmentación de clientes</h3></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#manual" role="tab"><h3>Agregar por selección manual</h3></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane{{ (!request()->has('registrados')) ? ' active' : '' }}" id="clientes-promocion" role="tabpanel">
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        @if(!$clientes_promocion->isEmpty())
                                        <table class="table table-condensed table-hover" id="table-clientes-promocion">
                                            <thead>
                                                <tr>
                                                    <th>Identificación</th>
                                                    <th>Razón social</th>
                                                    <th>Antigüedad</th>
                                                    <th>Eliminar de la promoción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($clientes_promocion as $cliente)
                                                <tr>
                                                    <td>{{ $cliente->identificacion }}</td>
                                                    <td>{{ $cliente->razon_social }}</td>
                                                    <td>{{ $cliente->antiguedad }}</td>
                                                    <td>
                                                        <form action="{{ route('admin.promociones.clientes.eliminar', [$promocion->id]) }}" method="post" onsubmit="if(!confirm('¿Confirma que desea eliminar este cliente de la promoción?')) { return false; }">
                                                            {!! csrf_field() !!}
                                                            {!! method_field('DELETE') !!}
                                                            <input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
                                                            <button type="submit" class="btn btn-danger btn-sm" title="Eliminar"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @else
                                        <p>&nbsp;</p>
                                        <div class="alert alert-info">
                                            <h3 style="text-align: center;">Esta promoción aplica a todos los clientes</h3>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div col-lg-12 col-md-12 col-sm-12 col-xs-12>
                                        <button type="button" class="cancelar btn btn-default"><i class="fa fa-arrow-left"></i> Regresar al listado de promociones</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane{{ (request()->has('registrados')) ? ' active' : '' }}" id="segmentacion" role="tabpanel">
                                <form action="{{ route('admin.promociones.clientes.guardar', [$promocion->id]) }}" method="post" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <fieldset>
                                        <legend>Filtrar clientes</legend>
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label class="filter-col">Registrados</label>
                                                <select class="form-control" id="registrados">
                                                    <option value="todos">Todos</option>
                                                    <option value="no-realizado-compras"{{ (request()->registrados == 'no-realizado-compras') ? ' selected' : '' }}>No han realizado compras</option>
                                                    <option value="nunca-visitado-tienda"{{ (request()->registrados == 'nunca-visitado-tienda') ? ' selected' : '' }}>Nunca han visitado la tienda</option>
                                                    <option value="no-visitan-tienda-mas-3-meses"{{ (request()->registrados == 'no-visitan-tienda-mas-3-meses') ? ' selected' : '' }}>No visitan la tienda hace más de 3 meses</option>
                                                    <option value="no-visitan-tienda-mas-6-meses"{{ (request()->registrados == 'no-visitan-tienda-mas-6-meses') ? ' selected' : '' }}>No visitan la tienda hace más de 6 meses</option>
                                                    <option value="no-visitan-tienda-mas-12-meses"{{ (request()->registrados == 'no-visitan-tienda-mas-12-meses') ? ' selected' : '' }}>No visitan la tienda hace más de 1 año</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-12">
                                            <p>&nbsp;</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <table class="table table-condensed table-hover" id="table-segmentacion">
                                                <thead>
                                                    <tr>
                                                        <th><input type="checkbox" class="form-control" value="" id="checkall" checked></th>
                                                        <th>Identificación</th>
                                                        <th>Razón social</th>
                                                        <th>Antigüedad</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($clientes as $cliente)
                                                    <tr>
                                                        <td><input type="checkbox" class="form-control" name="cliente_id[]" value="{{ $cliente->id }}" checked></td>
                                                        <td>{{ $cliente->identificacion }}</td>
                                                        <td>{{ $cliente->razon_social }}</td>
                                                        <td>{{ $cliente->antiguedad }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aplicar</button>
                                    <button type="button" class="cancelar btn btn-default"><i class="fa fa-close"></i> Cancelar</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="manual" role="tabpanel">
                                <form action="{{ route('admin.promociones.clientes.guardar', [$promocion->id]) }}" method="post" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-12">
                                            <p>&nbsp;</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <label>Escriba parte del nombre o la identificación del (los) cliente(s) y selecciónelo(s)</label>
                                            <select name="cliente_id[]" class="form-control select2" data-width="100%" multiple="">
                                                @foreach($clientes as $cliente)
                                                    <option value="{{ $cliente->id }}">{{ $cliente->identificacion }} - {{ $cliente->razon_social }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aplicar</button>
                                    <button type="button" class="cancelar btn btn-default"><i class="fa fa-close"></i> Cancelar</button>
                                </form>
                            </div>
                        </div>
                        
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $(".cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.promociones.listado') }}";
    });
    
    $("#registrados").on("change", function () {
        location.href = "{{ url()->current() }}?registrados=" + $(this).val();
    });
});
</script>
@endsection