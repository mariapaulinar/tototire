@extends('admin.layout.master')

@section('titulo')
Editar promoción: {{ $promocion->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.promociones.editar.actualizar', [$promocion->id]) }}" method="post" enctype="multipart/form-data">
                        {!! method_field('PUT') !!}
                        @include('admin.promociones.campos', ['promocion' => $promocion])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.promociones.listado') }}";
    });
});
</script>
@endsection