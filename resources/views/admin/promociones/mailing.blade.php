@extends('admin.layout.master')

@section('titulo')
Campañas de mailing para: {{ $promocion->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-title">
                <h3>Programar una campaña de mailing</h3>
            </div>
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.promociones.mailing.guardar', [$promocion->id]) }}" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="form-group col-4">
                                <label>Desde</label>
                                <input type="text" name="fecha_hora_inicio" class="form-control fecha-hora mask-fecha-hora" placeholder="" value="" required>
                            </div>
                            <div class="form-group col-4">
                                <label>Hasta</label>
                                <input type="text" name="fecha_hora_fin" class="form-control fecha-hora mask-fecha-hora" placeholder="" value="" required>
                            </div>
                            <div class="form-group col-4">
                                <label>Frecuencia de envíos</label>
                                <select name="frecuencia" class="form-control" required="">
                                    <option value="">Seleccione</option>
                                    <option value="una_vez">Una vez</option>
                                    <option value="diario">Diario</option>
                                    <option value="semanal">Semanal</option>
                                    <option value="mensual">Mensual</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-calendar-plus-o"></i> Programar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-title">
                <h3>Campañas de mailing programadas</h3>
            </div>
            <div class="card-body">
                {!! $datatable_mailing->table() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{!! $datatable_mailing->scripts() !!}
@endsection