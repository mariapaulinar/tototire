{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>Producto en promoción</label>
        <select name="producto_id" class="form-control select2" required>
            @if($promocion)
                @foreach($productos as $producto)
                    @php
                        $selected = ($producto->id == $promocion->producto_id) ? ' selected' : '';
                    @endphp
                    <option value="{{ $producto->id }}"{{ $selected }}>{{ $producto->descripcion }}</option>
                @endforeach
            @else
                @foreach($productos as $producto)
                    <option value="{{ $producto->id }}">{{ $producto->descripcion }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Código</label>
        <input type="text" name="codigo" id="codigo" class="form-control" placeholder="Código de la promocion" maxlength="20" value="{{ ($promocion) ? $promocion->codigo : '' }}">
    </div>
    <div class="form-group col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <label>Descripción</label>
        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripción de la promocion" maxlength="255" value="{{ ($promocion) ? $promocion->descripcion : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>Descripción larga</label>
        <textarea name="descripcion_larga" id="descripcion_larga" class="form-control" placeholder="Descripción larga de la promocion" rows="5" style="height: 150px;">{{ ($promocion) ? $promocion->descripcion_larga : '' }}</textarea>
    </div>
</div>
<div class="row">
    <div class="form-group col-4">
        <label>Desde</label>
        <input type="text" name="fecha_hora_inicio" class="form-control fecha-hora mask-fecha-hora" placeholder="" value="{{ ($promocion) ? $promocion->fecha_hora_inicio : '' }}" required>
    </div>
    <div class="form-group col-4">
        <label>Hasta</label>
        <input type="text" name="fecha_hora_fin" class="form-control fecha-hora mask-fecha-hora" placeholder="" value="{{ ($promocion) ? $promocion->fecha_hora_fin : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-3">
        <label>Precio de venta</label>
        <input type="text" name="precio_venta" class="form-control" placeholder="" value="{{ ($promocion) ? $promocion->precio_venta : '' }}" required>
    </div>
    <div class="form-group col-3">
        <label>Cantidad dispuesta</label>
        <input type="text" name="cantidad_dispuesta" class="form-control" placeholder="" value="{{ ($promocion) ? $promocion->cantidad_dispuesta : '' }}">
    </div>
    <div class="form-group col-3">
        <label>Cantidad mínima compra</label>
        <input type="text" name="cantidad_min" class="form-control" placeholder="" value="{{ ($promocion) ? $promocion->cantidad_min : '' }}">
    </div>
    <div class="form-group col-3">
        <label>Cantidad máxima compra</label>
        <input type="text" name="cantidad_max" class="form-control" placeholder="" value="{{ ($promocion) ? $promocion->cantidad_max : '' }}">
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
<button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>