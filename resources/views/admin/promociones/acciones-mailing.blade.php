<form method="post" action="{{ route('admin.promociones.mailing.eliminar', [$mailing->promocion_id, $mailing->id]) }}" onsubmit="return confirm('¿Confirma que desea eliminar esta campaña de mailing? Tenga en cuenta que al eliminarla suspenderá los envíos de esta campaña que estén en curso.');">
    {!! csrf_field() !!}
    {!! method_field('DELETE') !!}
    <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar esta campaña de mailing"><i class="fa fa-trash"></i></button>
</form>