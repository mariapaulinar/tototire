@extends('admin.layout.master')

@section('titulo')
Editar categoria: {{ $categoria->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.categorias.editar.actualizar', [$categoria->id]) }}" method="post">
                        {!! method_field('PUT') !!}
                        @include('admin.categorias.campos', ['categoria' => $categoria])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.categorias.listado') }}";
    });
});
</script>
@endsection