{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <label>Descripción</label>
        <input type="text" name="descripcion" class="form-control" placeholder="Descripción de la categoría" maxlength="255" value="{{ ($categoria) ? $categoria->descripcion : '' }}">
    </div>
    <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Estado</label>
        @include('admin.layout.estados', ['estado_seleccionado' => ($categoria) ? $categoria->estado : null])
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
<button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>