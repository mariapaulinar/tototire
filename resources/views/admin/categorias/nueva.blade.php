@extends('admin.layout.master')

@section('titulo')
Nueva categoria
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.categorias.nueva.guardar') }}" method="post">
                        @include('admin.categorias.campos', ['categoria' => null])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.categorias.listado') }}";
    });
});
</script>
@endsection