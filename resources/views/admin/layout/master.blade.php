<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ config('parametros.url_template_admin') }}/images/favicon.png">
        <title>Tototire - Admin</title>
        <!-- Bootstrap Core CSS -->
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ config('parametros.url_template_admin') }}/css/helper.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/style.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/js/lib/dropify/dist/css/dropify.min.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap-rating/bootstrap-rating.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/select2/select2.min.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap-multiselect/bootstrap-multiselect.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/daterangepicker/daterangepicker.css" rel="stylesheet">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
        <!--[if lt IE 9]>
        <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
        <style>
            ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: #bfc8cc !important;
                opacity: 1; /* Firefox */
            }

            :-ms-input-placeholder { /* Internet Explorer 10-11 */
                color: #bfc8cc !important;
            }

            ::-ms-input-placeholder { /* Microsoft Edge */
                color: #bfc8cc !important;
            }
            
            .filter-col{
                padding-left:10px;
                padding-right:10px;
            }
            
            .left-sidebar {
                width: 255px;
            }
        </style>
        @yield('css')
    </head>

    <body class="fix-header fix-sidebar">
        <!-- Preloader - style you can find in spinners.css -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
            </svg>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">
            <!-- header header  -->
            <div class="header">
                <nav class="navbar top-navbar navbar-expand-md navbar-light">
                    <!-- Logo -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ route('admin.index') }}">
                            <!-- Logo icon -->
                            <b><img src="{{ asset('img/tototire-logo1.png') }}" style="width: 167px;" alt="homepage" class="dark-logo" /></b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <!--<span><img src="{{ config('parametros.url_template_admin') }}/images/logo-text.png" alt="homepage" class="dark-logo" /></span>-->
                        </a>
                    </div>
                    <!-- End Logo -->
                    <div class="navbar-collapse">
                        <!-- toggle and nav items -->
                        <ul class="navbar-nav mr-auto mt-md-0">
                            <!-- This is  -->
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                            <!--<li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>-->
                        </ul>
                        <!-- User profile and search -->
                        <ul class="navbar-nav my-lg-0">

                            <!-- Search -->
<!--                            <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                                <form class="app-search">
                                    <input type="text" class="form-control" placeholder="Search here"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                            </li>-->
                            <!-- Comment -->
<!--                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
                                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                    <ul>
                                        <li>
                                            <div class="drop-title">Notifications</div>
                                        </li>
                                        <li>
                                            <div class="message-center">
                                                 Message 
                                                <a href="#">
                                                    <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>This is title</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>This is another title</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="btn btn-info btn-circle m-r-10"><i class="ti-settings"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>This is title</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="btn btn-primary btn-circle m-r-10"><i class="ti-user"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>This is another title</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                        <li>
                                            <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>-->
                            <!-- End Comment -->
                            <!-- Messages -->
<!--                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted  " href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope"></i>
                                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
                                    <ul>
                                        <li>
                                            <div class="drop-title">You have 4 new messages</div>
                                        </li>
                                        <li>
                                            <div class="message-center">
                                                 Message 
                                                <a href="#">
                                                    <div class="user-img"> <img src="{{ config('parametros.url_template_admin') }}/images/users/5.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                    <div class="mail-contnet">
                                                        <h5>Michael Qin</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="user-img"> <img src="{{ config('parametros.url_template_admin') }}/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                                    <div class="mail-contnet">
                                                        <h5>John Doe</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="user-img"> <img src="{{ config('parametros.url_template_admin') }}/images/users/3.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                                    <div class="mail-contnet">
                                                        <h5>Mr. John</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span>
                                                    </div>
                                                </a>
                                                 Message 
                                                <a href="#">
                                                    <div class="user-img"> <img src="{{ config('parametros.url_template_admin') }}/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                                    <div class="mail-contnet">
                                                        <h5>Michael Qin</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </li>
                                        <li>
                                            <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>-->
                            <!-- End Messages -->
                            <!-- Profile -->
                            <li class="nav-item dropdown">
                                <strong>Usuario conectado:</strong> {{ auth()->user()->name }} (<a href="#" onclick="event.preventDefault(); $('form#logout-form').submit();"><i class="fa fa-power-off"></i> Salir</a>)
<!--                                <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                    <ul class="dropdown-user">
                                        <li></li>
                                    </ul>
                                </div>-->
                            </li>
                        </ul>
                        <form method="post" action="{{ route('logout') }}" id="logout-form">
                                {!! csrf_field() !!}
                        </form>
                    </div>
                </nav>
            </div>
            <!-- End header header -->
            @include('admin.layout.menu')
            <!-- Page wrapper  -->
            <div class="page-wrapper">
                <!-- Bread crumb -->
                <div class="row page-titles">
                    <div class="col-md-12 align-self-center">
                        <h3 class="text-primary">@yield('titulo')</h3>
                    </div>
<!--                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>-->
                </div>
                <!-- End Bread crumb -->
                <!-- Container fluid  -->
                <div class="container-fluid">
                    <!-- Start Page Content -->
                    @include('admin.layout.alert')
                    @yield('contenido')
                    <!-- End PAge Content -->
                </div>
                <!-- End Container fluid  -->
                <!-- footer -->
                <footer class="footer"> © 2018 <a href="https://tototire.com" target="_blank">Tototire</a></footer>
                <!-- End footer -->
            </div>
            <!-- End Page wrapper  -->
        </div>
        <!-- End Wrapper -->
        <!-- All Jquery -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap/js/popper.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/jquery.slimscroll.js"></script>
        <!--Menu sidebar -->
        <script src="{{ config('parametros.url_template_admin') }}/js/sidebarmenu.js"></script>
        <!--stickey kit -->
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
        <!--Custom JavaScript -->
        <script src="{{ config('parametros.url_template_admin') }}/js/custom.min.js"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="/vendor/datatables/buttons.server-side.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/dropify/dist/js/dropify.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap-rating/bootstrap-rating.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/datepicker/bootstrap-datepicker.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/datepicker/bootstrap-datepicker.es.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery-mask/jquery.mask.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/select2/select2.full.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/moment/moment.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/moment/moment-with-locales.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap-multiselect/bootstrap-multiselect.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/daterangepicker/daterangepicker.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery.number.min.js"></script>
        <script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery.blockUI.js"></script>
        <!--<script src="{{ config('parametros.url_template_admin') }}/js/jquery.sticky-kit.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.3.3/ace.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                $(".dropify").dropify({
                    messages: {
                        default: "Clic o arrastre un archivo hasta aquí",
                        replace: "Arrastre un archivo o clic para reemplazar",
                        remove:  "Eliminar",
                        error:   "Ha ocurrido un error"
                    }
                });
                
                $(".datepicker").datepicker({
                    format: "yyyy-mm-dd",
                    language: "es"
                });
                
                moment.locale("es");
                $(".fecha-hora").daterangepicker({
                    timePicker: true,
                    locale: {
                        format: "YYYY-MM-DD HH:mm:ss",
                        applyLabel: "Aplicar",
                        cancelLabel: "Cancelar",
                        fromLabel: "Desde",
                        toLabel: "Hasta"
                    },
                    singleDatePicker: true,
                    showDropdowns: true,
                    startDate: "{{ date('Y-m-d H:i:s') }}"
                });
                
                $(".mask-fecha").mask("0000-00-00");
                $(".mask-fecha-hora").mask("0000-00-00 00:00:00");
                
                $(".select2").select2({
                    placeholder: "Seleccione"
                });
                
                $(".multiple").multiselect({
                    buttonClass: 'btn btn-secondary',
                    templates: {
                      li: '<li><a tabindex="0" class="dropdown-item"><label style="color: #000 !important;"></label></a></li>',
                    }
                });
                
                $(".timepicker").timepicker({
                    icons: {
                        up: "fa fa-chevron-up",
                        down: "fa fa-chevron-down"
                    }
                });
                
                window.setTimeout(function() {
                    $(".alert").not(".alert-info").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 3000);
                
                $("#dataTableBuilder").on("draw.dt", function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
                
                $('[data-toggle="tooltip"]').tooltip();
            });
            
            function mostrarLoading()
            {
                $.blockUI({ 
                    message: '<img src="{{ asset('img/loading.gif') }}"><h4 style="font-weight: bolder;">Por favor espere un momento</h4>', 
                    css: { 
                        border: "0",
                        background: "transparent"
                    },
                    overlayCSS:  { 
                        backgroundColor: "#fff", 
                        opacity:         0.6, 
                        cursor:          "wait" 
                    }
                });
            }
        </script>
        @yield('scripts')
    </body>

</html>