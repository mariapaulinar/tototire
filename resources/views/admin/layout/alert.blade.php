@if(session()->has('ok'))
<div class="row">
    <div class="col-12 alert alert-{{ (session()->get('ok')) ? 'success' : 'danger' }} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h2 style="text-align: center;">{{ session()->get('mensaje') }}</h2>
    </div>
</div>
@endif