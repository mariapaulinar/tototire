<!-- Left Sidebar  -->
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label"><a href="{{ route('admin.index') }}">Inicio</a></li>
                <li class="nav-label">Administradores</li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tag"></i><span class="hide-menu"> Marcas</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.marcas.listado') }}">Listado de marcas</a></li>
                        <li><a href="{{ route('admin.marcas.nueva') }}">Nueva marca</a></li>
                        <li><a href="{{ route('admin.marcas.cargue-masivo') }}">Cargue masivo de marcas</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-object-group"></i><span class="hide-menu"> Categorías</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.categorias.listado') }}">Listado de categorías</a></li>
                        <li><a href="{{ route('admin.categorias.nueva') }}">Nueva categoría</a></li>
                        <li><a href="{{ route('admin.categorias.cargue-masivo') }}">Cargue masivo de categorias</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu"> Productos</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.productos.listado') }}">Listado de productos</a></li>
                        <li><a href="{{ route('admin.productos.nuevo') }}">Nuevo producto</a></li>
                        <li><a href="{{ route('admin.productos.cargue-masivo') }}">Cargue masivo de productos</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-address-book"></i><span class="hide-menu"> Clientes</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.clientes.listado') }}">Listado de clientes</a></li>
                        <li><a href="{{ route('admin.clientes.nuevo') }}">Nuevo cliente</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu"> Usuarios</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.users.listado') }}">Listado de usuarios</a></li>
                        <li><a href="{{ route('admin.users.nuevo') }}">Nuevo usuario</a></li>
                    </ul>
                </li>
                <li class="nav-label">Pedidos</li>
                <li><a href="{{ route('admin.pedidos.listado') }}"><i class="fa fa-shopping-cart"></i>Listado de pedidos</a></li>
                <li><a href="{{ route('admin.pedidos.nuevo') }}"><i class="fa fa-cart-plus"></i>Nuevo pedido</a></li>
                <li class="nav-label">Promociones y descuentos</li>
                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-certificate"></i><span class="hide-menu"> Promociones</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.promociones.listado') }}">Listado de promociones</a></li>
                        <li><a href="{{ route('admin.promociones.nueva') }}">Nueva promoción</a></li>
                        <li><a href="{{ route('admin.promociones.cargue-masivo') }}">Cargue masivo de promociones</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  -->