@php
$selected_activo = ($estado_seleccionado && $estado_seleccionado == '1') ? ' selected' : '';
$selected_inactivo = (!$estado_seleccionado && $estado_seleccionado == '0') ? ' selected' : '';
@endphp
<select class="form-control" name="estado" id="estado">
    <option value="1"{{ $selected_activo }}>Activo</option>
    <option value="0"{{ $selected_inactivo }}>Inactivo</option>
</select>