{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-3">
        <label>PLU</label>
        <input type="text" name="plu" id="plu" class="form-control" placeholder="PLU del producto" maxlength="20" value="{{ ($producto) ? $producto->plu : '' }}" required>
    </div>
    <div class="form-group col-9">
        <label>Descripción</label>
        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripción del producto" maxlength="255" value="{{ ($producto) ? $producto->descripcion : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <label>Descripción larga</label>
        <textarea name="descripcion_larga" id="descripcion_larga" class="form-control" placeholder="Descripción larga del producto" rows="5" style="height: 150px;">{{ ($producto) ? $producto->descripcion_larga : '' }}</textarea>
    </div>
</div>
<div class="row">
    <div class="form-group col-6">
        <label>Marca</label>
        <select name="marca_id" id="marca_id" class="form-control" required>
            @foreach($marcas as $marca)
            @php
            $selected = ($producto && ($marca->id == $producto->marca_id)) ? ' selected' : ''
            @endphp
            <option value="{{ $marca->id }}"{{ $selected }}>{{ $marca->descripcion }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-6">
        <label>Código de barras</label>
        <input type="text" name="codigo_barras" id="codigo_barras" class="form-control" placeholder="Código de barras" maxlength="20" value="{{ ($producto) ? $producto->codigo_barras : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-4">
        <label>Ancho</label>
        <input type="number" name="ancho" id="ancho" class="form-control" placeholder="Ancho" min="1" maxlength="3" value="{{ ($producto) ? $producto->ancho : '' }}">
    </div>
    <div class="form-group col-4">
        <label>Perfil</label>
        <input type="number" name="perfil" id="perfil" class="form-control" placeholder="Perfil" min="1" maxlength="2" value="{{ ($producto) ? $producto->perfil : '' }}">
    </div>
    <div class="form-group col-4">
        <label>Rin</label>
        <input type="number" name="rin" id="rin" class="form-control" placeholder="Rin" min="1" maxlength="2" value="{{ ($producto) ? $producto->rin : '' }}">
    </div>
</div>
<div class="row">
    <div class="form-group col-4">
        <label>Precio de venta</label>
        <input type="number" name="precio_venta" id="precio_venta" class="form-control" placeholder="Precio de venta" min="0" value="{{ ($producto) ? $producto->precio_venta : '' }}" required>
    </div>
    <div class="form-group col-4">
        <label>Existencia</label>
        <input type="number" name="existencia" id="existencia" class="form-control" placeholder="Existencia" min="0" maxlength="10" value="{{ ($producto) ? $producto->existencia : '' }}" required>
    </div>
    <div class="form-group col-4">
        <label>Estado</label>
        @include('admin.layout.estados', ['estado_seleccionado' => ($producto) ? $producto->estado : null])
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <label>Foto principal</label>
        <input type="file" name="imagen" id="imagen" class="dropify" data-default-file="{{ ($producto && $producto->foto_principal) ? asset('storage/productos/' . $producto->foto_principal) : asset('img/no-image.png') }}">
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
        <button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>
    </div>
</div>