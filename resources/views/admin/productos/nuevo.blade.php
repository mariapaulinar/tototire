@extends('admin.layout.master')

@section('titulo')
Nuevo producto
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.productos.nuevo.guardar') }}" method="post" enctype="multipart/form-data">
                        @include('admin.productos.campos', ['producto' => null])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.productos.listado') }}";
    });
});
</script>
@endsection