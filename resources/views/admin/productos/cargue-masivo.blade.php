@extends('admin.layout.master')

@section('titulo')
Cargue masivo de productos
@endsection

@section('contenido')
<div class="row">
    <div class="col-12 alert alert-info">
        <h4 class="text-center"><i class="fa fa-info-circle"></i> Cargue un archivo .xls, .xlsx o .csv</h4>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <a href="{{ asset('docs/admin/productos_13_09_2018.xlsx') }}" target="_blank" class="btn btn-secondary pull-right" role="button"><i class="fa fa-file-excel-o"></i> Descargar archivo modelo</a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.productos.cargue-masivo.cargar') }}" method="post" enctype="multipart/form-data" onsubmit="mostrarLoading();">
                        {!! csrf_field() !!}
                        <input type="file" name="archivo" id="archivo" class="dropify" data-allowed-file-extensions="xls xlsx csv" required>
                        <hr>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-play"></i> Empezar la carga</button>
                        <button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.productos.listado') }}";
    });
});
</script>
@endsection