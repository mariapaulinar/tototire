@extends('admin.layout.master')

@section('titulo')
Editar producto: {{ $producto->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <h4 class="pull-right"><small><a href="#comentarios" class="btn btn-secondary"><i class="fa fa-comments"></i> Ver comentarios y calificaciones</a></small></h4>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.productos.editar.actualizar', [$producto->id]) }}" method="post" enctype="multipart/form-data">
                        {!! method_field('PUT') !!}
                        @include('admin.productos.campos', ['producto' => $producto])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="comentarios">
    <div class="col-12">
        <div class="card">
            <div class="card-title">
                <h2>Comentarios y calificaciones</h2>
            </div>
            <div class="card-body">
                @if(!$comentarios->isEmpty())
                <ul>
                    @foreach($comentarios as $comentario)
                    <li>
                        <hr>
                        <strong>{{ $comentario->nombres . ' ' . $comentario->apellidos}} ({{ $comentario->fecha->format('d/m/Y h:i a') }})</strong>: {{ $comentario->comentario }}<br>
                        <input type="hidden" class="rating" value="{{ $comentario->rating }}" data-filled="fa fa-star text-danger" data-empty="fa fa-star-o text-danger" data-readonly/>
                    </li>
                    @endforeach
                </ul>
                {!! $comentarios->links() !!}
                @else
                <h3>No hay comentarios y calificaciones para este producto.</h3>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.productos.listado') }}";
    });
});
</script>
@endsection