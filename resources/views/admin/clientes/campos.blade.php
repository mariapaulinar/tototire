{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-6">
        <label>Razón social</label>
        <input type="text" name="razon_social" class="form-control" placeholder="Nombre completo del cliente" maxlength="191" value="{{ ($cliente) ? $cliente->razon_social : '' }}" required>
    </div>
    <div class="form-group col-6">
        <label>Email</label>
        <input type="text" name="email" class="form-control" placeholder="Correo electrónico" maxlength="191" value="{{ ($cliente) ? $cliente->email : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <label>Dirección</label>
        <input type="text" name="direccion" class="form-control" placeholder="Dirección" maxlength="255" value="{{ ($cliente) ? $cliente->direccion : '' }}" required>
    </div>
</div>
<div class="row">
    <div class="form-group col-3">
        <label>Tipo de identificación</label>
        <select name="tipo_ident" id="tipo_ident" class="form-control" required>
            <option value="cc"{{ ($cliente && $cliente->tipo_ident == 'cc') ? ' selected' : '' }}>CC</option>
            <option value="nit"{{ ($cliente && $cliente->tipo_ident == 'nit') ? ' selected' : '' }}>NIT</option>
            <option value="ce"{{ ($cliente && $cliente->tipo_ident == 'ce') ? ' selected' : '' }}>CE</option>
        </select>
    </div>
    <div class="form-group col-3">
        <label>No. Identificación</label>
        <input type="text" name="identificacion" class="form-control" placeholder="No. Identificación" maxlength="20" value="{{ ($cliente) ? $cliente->identificacion : '' }}" required>
    </div>
    <div class="form-group col-2" id="div_digito_verif" @if($cliente && $cliente->tipo_ident != 'nit')style="display: none;"@endif>
        <label>Dígito de verificación</label>
        <input type="text" name="digito_verif" id="digito_verif" class="form-control" placeholder="Dígito de verificación" maxlength="1" value="{{ ($cliente) ? $cliente->digito_verif : '' }}">
    </div>
    <div class="form-group col-4">
        <label>Fecha de nacimiento (Año-mes-día)</label>
        <input type="text" name="fecha_nacimiento" class="form-control datepicker mask-fecha" placeholder="Fecha de nacimiento" maxlength="10" value="{{ ($cliente) ? $cliente->fecha_nacimiento : '' }}">
    </div>
</div>
<div class="row">
    <div class="form-group col-3">
        <label>Sexo</label>
        <select name="sexo" class="form-control">
            <option value="M"{{ ($cliente && $cliente->sexo == 'M') ? ' selected' : '' }}>Masculino</option>
            <option value="F"{{ ($cliente && $cliente->sexo == 'F') ? ' selected' : '' }}>Femenino</option>
        </select>
    </div>
    <div class="form-group col-3">
        <label>Teléfono 1</label>
        <input type="text" name="telefono1" class="form-control" placeholder="Teléfono 1" maxlength="15" value="{{ ($cliente) ? $cliente->telefono1 : '' }}" required>
    </div>
    <div class="form-group col-3">
        <label>Teléfono 2</label>
        <input type="text" name="telefono2" class="form-control" placeholder="Teléfono 2" maxlength="15" value="{{ ($cliente) ? $cliente->telefono2 : '' }}">
    </div>
    <div class="form-group col-3">
        <label>Estado</label>
        @include('admin.layout.estados', ['estado_seleccionado' => ($cliente) ? $cliente->estado : null])
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
<button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>
