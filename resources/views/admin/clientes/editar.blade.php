@extends('admin.layout.master')

@section('titulo')
Editar cliente: {{ $cliente->name }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.clientes.editar.actualizar', [$cliente->id]) }}" method="post">
                        {!! method_field('PUT') !!}
                        @include('admin.clientes.campos', ['cliente' => $cliente])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.clientes.listado') }}";
    });
    
    $("#tipo_ident").on("change", function () {
        $("#digito_verif").val("");
        if($(this).val() != "nit") {
            $("#div_digito_verif").attr("style", "display: none");
        } else {
            $("#div_digito_verif").removeAttr("style");
        }
    });
});
</script>
@endsection