@extends('admin.layout.master')

@section('titulo')
Clientes
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-6">
                        <label for="estado">Filtrar por estado</label>
                        <select name="estado" id="estado" class="form-control">
                            <option value="">Todos</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
$(document).ready(function () {
    $("#dataTableBuilder").on('preXhr.dt', function(e, settings, data) {
        data.estado = $("#estado").val();
    });
    
    $("#estado").on("change", function () {
        window.LaravelDataTables["dataTableBuilder"].draw();
    });
});
</script>
@endsection