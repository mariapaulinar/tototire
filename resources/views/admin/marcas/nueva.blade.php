@extends('admin.layout.master')

@section('titulo')
Nueva marca
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.marcas.nueva.guardar') }}" method="post" enctype="multipart/form-data">
                        @include('admin.marcas.campos', ['marca' => null])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.marcas.listado') }}";
    });
});
</script>
@endsection