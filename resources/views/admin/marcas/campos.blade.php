{!! csrf_field() !!}
<div class="row">
    <div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <label>Descripción</label>
        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripción de la marca" maxlength="255" value="{{ ($marca) ? $marca->descripcion : '' }}">
    </div>
    <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <label>Tipo</label>
        <select class="form-control" name="tipo_marca" id="tipo_marca">
            <option value="{{ config('parametros.tipo_marca_llanta') }}">{{ strtoupper(config('parametros.tipo_marca_llanta')) }}</option>
            <option value="{{ config('parametros.tipo_marca_vehiculo') }}">{{ strtoupper(config('parametros.tipo_marca_vehiculo')) }}</option>
        </select>
    </div>
    <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <label>Estado</label>
        @include('admin.layout.estados', ['estado_seleccionado' => ($marca) ? $marca->estado : null])
    </div>
</div>
<div class="row">
    <div class="form-group col-12">
        <label>Imagen</label>
        <input type="file" name="imagen" id="imagen" class="dropify" @if($marca && $marca->imagen)data-default-file="{{ asset('storage/marcas/' . $marca->imagen) }}"@endif>
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
<button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>