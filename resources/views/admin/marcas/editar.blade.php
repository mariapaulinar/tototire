@extends('admin.layout.master')

@section('titulo')
Editar marca: {{ $marca->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="basic-form">
                    <form action="{{ route('admin.marcas.editar.actualizar', [$marca->id]) }}" method="post" enctype="multipart/form-data">
                        {!! method_field('PUT') !!}
                        @include('admin.marcas.campos', ['marca' => $marca])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.marcas.listado') }}";
    });
});
</script>
@endsection