@extends('admin.layout.master')

@section('titulo')
Editar producto: {{ $producto->descripcion }}
@endsection

@section('contenido')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3>Datos básicos</h3>
            </div>
            <div class="card-body">
                <div class="basic-form form-inline">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Pedido No.</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->id }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cliente</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->nombres_cliente }} {{ $pedido->apellidos_cliente }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Fecha</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->fecha_hora }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Dirección despacho</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->direccion_despacho_cliente }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Teléfono/Celular</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->telefono_cliente }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Correo electrónico</label>
                        <div class="col-sm-10">
                          <p class="form-control-static">{{ $pedido->email_cliente }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.pedidos.listado') }}";
    });
});
</script>
@endsection