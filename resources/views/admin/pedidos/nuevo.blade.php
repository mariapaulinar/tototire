@extends('admin.layout.master')

@section('titulo')
Nuevo pedido
@endsection

@section('css')
<style>
    .frame {
        height: 1000px;
    }
    
    #totales > .form-group {
        margin-bottom: 0px;
    }
</style>
@endsection

@section('contenido')
<form action="{{ route('admin.pedidos.guardar') }}" method="post">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="basic-form">
                        <div class="frame">
                            <div class="ui-layout-east" style="font-size: small; overflow-x: none;">
                                <h3 style="text-align: center;">Productos seleccionados</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table table-condensed table-hover" id="productos-seleccionados" style="width: 50%">
                                            <thead>
                                                <tr>
                                                    <th>Referencia</th>
                                                    <th>Descripción</th>
                                                    <th>Precio Uni.</th>
                                                    <th>Cantidad</th>
                                                    <th>Total</th>
                                                    <th>Quitar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr style="display: none;">
                                                    <td class="plu"><span></span><input type="hidden" id="producto_id" value=""></td>
                                                    <td class="descripcion"></td>
                                                    <td class="precio"></td>
                                                    <td class="cantidad"><input type="number" min="1" name="cantidad" class="form-control col-12" value="" data-precio-venta="" onkeyup="calcularTotalProductoCarrito(this)" onchange="calcularTotalProductoCarrito(this)" onmouseup="calcularTotalProductoCarrito(this)"></td>
                                                    <td class="total"><span></span><input type="hidden" id="total" value=""></td>
                                                    <td class="borrar"><button class="btn btn-sm btn-danger" title="Quitar" onclick="event.preventDefault(); quitar(this)"><i class="fa fa-times"></i></button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="totales" class="float-right" style="display: none;">
                                    <div class="col-12 form-group row">
                                        <label class="col-sm-6 col-form-label text-right">Subtotal:</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="subtotal" readonly class="form-control-plaintext" value="0">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group row">
                                        <label class="col-sm-6 col-form-label text-right">IVA:</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="iva" readonly class="form-control-plaintext" value="0">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group row">
                                        <label class="col-sm-6 col-form-label text-right">Total:</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="total" readonly class="form-control-plaintext" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui-layout-center">
                                <h3 style="text-align: center;">Seleccione productos y agregue al pedido</h3>
                                <div class="row">
                                    <div class="form-group col-md-12" style="width: 30%;">
                                        {!! $datatable->table() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="ui-layout-south">
                                <h3 style="text-align: center;">Datos del cliente</h3>
                                <div class="row">
                                    <div class="form-group col-3">
                                        <label>Tipo de identificación</label>
                                        <select name="tipo_ident" id="tipo_ident" class="form-control" onchange="buscarCliente();" required>
                                            <option value="cc">CC</option>
                                            <option value="nit">NIT</option>
                                            <option value="ce">CE</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-3">
                                        <label>No. Identificación</label>
                                        <input type="text" name="identificacion" class="form-control" onchange="buscarCliente();" placeholder="No. Identificación" maxlength="20" value="" required>
                                    </div>
                                    <div class="form-group col-2" id="div_digito_verif" style="display: none;">
                                        <label>Dígito de verificación</label>
                                        <input type="text" name="digito_verif" id="digito_verif" class="form-control" placeholder="Dígito de verificación" maxlength="1" value="">
                                    </div>
                                    <div class="form-group col-4">
                                        <label>Razón social</label>
                                        <input type="text" name="razon_social" class="form-control" placeholder="Nombre completo del cliente" maxlength="191" value="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4">
                                        <label>Teléfono 1*</label>
                                        <input type="text" name="telefono1" class="form-control" placeholder="Teléfono 1" maxlength="20" value="" required>
                                    </div>
                                    <div class="form-group col-4">
                                        <label>Teléfono 2</label>
                                        <input type="text" name="telefono2" class="form-control" placeholder="Teléfono 2" maxlength="20" value="">
                                    </div>
                                    <div class="form-group col-4">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Correo electrónico" maxlength="255" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Dirección de despacho*</label>
                                        <input type="text" name="direccion" class="form-control" placeholder="Dirección" maxlength="255" value="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
            <button type="button" class="btn btn-default" id="cancelar"><i class="fa fa-close"></i> Cancelar</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ config('parametros.url_template_admin') }}/js/lib/jquery.layout.js"></script>
{!! $datatable->scripts() !!}
<script type="text/javascript">
$(document).ready(function () {
    $("#cancelar").on("click", function (e) {
        e.preventDefault();
        location.href = "{{ route('admin.pedidos.listado') }}";
    });
    $(".frame").layout({
        applyDemoStyles: true,
        resizable: true,
        livePaneResizing: true,
        east__size: '40%',
        south__size: '400',
        east__resizable: false,
        east__spacing_open: 0,
        center__resizable: false
    });
});

function calcularTotalProducto(i)
{
    var cantidad = Number($(i).val());
    var precio_venta = Number($(i).data("precio-venta"));
    var total = precio_venta * cantidad;
    $(i).parents("tr").find(".total").text("$" + $.number(total, 0, ".", ","));
}

function calcularTotalProductoCarrito(i)
{
    var cantidad = Number($(i).val());
    var precio_venta = Number($(i).data("precio-venta"));
    var total = precio_venta * cantidad;
    $(i).parents("tr").children("td.total").find("span").text($.number("$" + total, 0, ".", ","));
    $(i).parents("tr").children("td.total").find("input").val(total);
    calcularTotal();
}

function agregar(producto_id, plu, descripcion_producto, precio_venta, b)
{
    var cantidad = $(b).parents("tr").find("input.cantidad").val();
    var total = cantidad * precio_venta;
    var tr = $("#productos-seleccionados tbody tr").eq(0).clone();
    
    $(tr).children("td.plu").find("input").val(producto_id).attr("name", "producto_id[]");
    $(tr).children("td.plu").find("span").text(plu);
    $(tr).children("td.descripcion").text(descripcion_producto);
    $(tr).children("td.precio").text($.number(precio_venta, 0, ".", ","));
    $(tr).children("td.cantidad").find("input").val(cantidad).attr("name", "cantidad[]").attr("data-precio-venta", precio_venta);
    $(tr).children("td.total").find("span").text($.number("$" + total, 0, ".", ","));
    $(tr).children("td.total").find("input").val(total);
    $(tr).removeAttr("style");
    $("#productos-seleccionados tbody").append($(tr));
    
    if($("#productos-seleccionados tbody tr").length > 1) {
        $("#totales").removeAttr("style");
    } else {
        $("#totales").attr("style", "display: none");
    }
    
    calcularTotal();
}

function quitar(b)
{
    $(b).parents("tr").remove();
    if($("#productos-seleccionados tbody tr").length > 1) {
        $("#totales").removeAttr("style");
    } else {
        $("#totales").attr("style", "display: none");
    }
    
    calcularTotal();
}

function calcularTotal()
{
    var total = 0;
    $("#productos-seleccionados tbody tr").each(function () {
        var tr = $(this);
        
        if(tr.children("td.plu").find("input").val() !== "") {
            total += Number(tr.children("td.total").find("input").val());
        }
    });
    
    var subtotal = total / 1.19;
    var iva = total - subtotal;
    
    $("#totales").find("#subtotal").val($.number("$" + Math.floor(subtotal), 0, ".", ","));
    $("#totales").find("#iva").val($.number("$" + Math.floor(iva), 0, ".", ","));
    $("#totales").find("#total").val($.number("$" + Math.floor(total), 0, ".", ","));
}

function buscarCliente()
{
    $.ajax({
        url: "{{ route('') }}"
    });
}
</script>
@endsection