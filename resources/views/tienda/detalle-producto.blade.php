@include('layouts.tienda.master') 


        <!--section class="flat-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumbs">
                            <li class="trail-item">
                                <a href="#" title="">Home</a>
                                <span><img src="images/icons/arrow-right.png" alt=""></span>
                            </li>
                            <li class="trail-item">
                                <a href="#" title="">Productos</a>
                                <span><img src="images/icons/arrow-right.png" alt=""></span>
                            </li>
                            <li class="trail-end">
                                <a href="#" title="">Smartphones</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section -->

        <section class="flat-product-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="flexslider">
                            <ul class="slides">
                                <li data-thumb="{{ asset('img/productos/bridgestone.png') }}">
                                  <a href='#' id="zoom" class='zoom'><img src="{{ asset('img/productos/bridgestone.png') }}" alt='' width='400' height='300' /></a>
                                  <span>NEW</span>
                                </li>
                                <li data-thumb="{{ asset('img/productos/bridgestone.png') }}">
                                  <a href='#' id="zoom1" class='zoom'><img src="{{ asset('img/productos/bridgestone.png') }}" alt='' width='400' height='300' /></a>
                                  <span>NEW</span>
                                </li>
                               
                            </ul><!-- /.slides -->
                        </div><!-- /.flexslider -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-7">
                        <div class="product-detail style4">
                            <div class="header-detail">
                                <h1 class="name">{{ $detalleproducto->descripcion}}</h1>
                                <div class="category">
                                    {{ $detalleproducto->marca}}
                                </div>
                                <div class="reviewed">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="review">
                                                <div class="queue">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </div>
                                                <!--div class="text">
                                                    <span>3 Vistas</span>
                                                    <span class="add-review">Add Your Review</span>
                                                </div-->
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="status-product">
                                                Disponible
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.reviewed -->
                            </div><!-- /.header-detail -->
                            <div class="content-detail">
                                <div class="price">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="regular">
                                                ${{ number_format($detalleproducto->precio_venta) }}
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="sale">
                                                ${{ number_format($detalleproducto->precio_venta) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-text">
                                    {{ $detalleproducto->descripcion}} Con la información adicional de cada llanta.
                                </div>
                                <div class="product-id">
                                    Codigo: <span class="id">FW511948218</span>
                                </div>
                            </div><!-- /.content-detail -->
                            <div class="footer-detail">
                                <div class="quanlity-box">
                                    <!--div class="colors">
                                        <select name="color">
                                            <option value="">Select Color</option>
                                            <option value="">Black</option>
                                            <option value="">Red</option>
                                            <option value="">White</option>
                                        </select>
                                    </div-->
        
                                        <div class="col-md-8">
                                            <div class="quanlity">
                                                <!--span class="btn-down"></span-->
                                                <input type="number" name="number" value="4" min="1" max="100" placeholder="Cantidad" id="cantidad_finalizar">
                                                <!--span class="btn-up"></span-->
                                            </div>
                                        </div>
    
                                    
                                </div><!-- /.quanlity-box -->
                                <div class="box-cart style2">
                                    <div class="btn-add-cart">
                                        <a href="#" title=""><img src="{{ asset('img/images/icons/add-cart.png') }}" alt="">Agregar Al Carrito</a>
                                    </div>
                                    
                                </div><!-- /.box-cart style2 -->
                                <div class="social-single">
                                    <span>Redes</span>
                                    <ul class="social-list style2">
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <!--li>
                                            <a href="#" title="">
                                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-dribbble" aria-hidden="true"></i>
                                            </a>
                                        </li-->
                                        <li>
                                            <a href="#" title="">
                                                <i class="fa fa-google" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div><!-- /.social-single -->
                            </div><!-- /.footer-detail -->
                        </div><!-- /.product-detail style4 -->
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-product-detail -->

        <section class="flat-product-content">
            <ul class="product-detail-bar">
                <li class="active">Descripción General</li>
                <li>Especificaciones Tecnicas</li>
                <li>Calificaciones</li>
                <li>Video</li>
            </ul><!-- /.product-detail-bar -->
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="description-text">
                            <div class="box-text">
                                <h4>Todas las descripciones posibles</h4>
                                <p style="text-align: justify;">
                                    La Llanta Bridgestone GIII – 185 60 R14 es la llanta deportiva que necesitas cuando buscas seguridad y el máximo desempeño en todas las condiciones.  
                                <br/>
                                    Su diseño optimizado de banda de rodamiento con compuesto de alta duración y de surcos de evacuación de agua en los hombros garantiza un excelente desempeño frente al condiciones de sueño mojado. 
                                <br/>
                                    Su compuesto "Green Chili Compound" reduce la fricción interna ofreciendo menor resistencia al rodaje y un excelente desempeño en maniobrabilidad. La tecnología Hydro Sipes con efecto limpiaparabrisa asegura las menores distancias de frenado incluso a altas velocidades.
                                </p>
                            </div>
                             <div class="box-text">
                                <h4>Diseño</h4>
                                <p style="text-align: justify;">
                                    Desempeño versátil para conductores de camiones, SUV y CUV <br/>
                                    Diseñado para una conducción confortable<br/>
                                    Tracción confiable en superficies mojadas y secas<br/>
                                    Armado con tecnologías que ayudan a ofrecer durabilidad con una buena vida de la banda
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="description-text">
                            <div class="box-text">
                                <h4>Generalidades</h4>
                                <p style="text-align: justify;">La Llanta Bridgestone GIII – 185 60 R14 es la llanta deportiva que necesitas cuando buscas seguridad y el máximo desempeño en todas las condiciones.  </p>
                            </div>
                           

                            <div class="box-text">
                                <h4>Beneficios</h4>
                                <p style="text-align: justify;">
                                    <strong>Precisión:</strong> ofrece respuesta precisa en la dirección y un desempeño excepcional en piso mojado.<br/>
                                    <strong>Adherencia:</strong> garantiza máxima adherencia en todas las condiciones.<br/>
                                    <strong>Corrección:</strong> permite corregir la alineación del vehículo, evitando el desgaste irregular de la llanta y garantizando una mayor vida útil de la llanta.
                                <br/>
                                    <strong>RTM Technology:</strong> informa al conductor cuando la llanta ya no es segura para rodar. <br/>
                                    <strong>Visual Alignment Indicator Technology:</strong> cuenta con el sistema que alerta al conductor sobre la condición de alineación del vehículo.  
                                <br/>
                                    <strong>Reactive Contour Technology: </strong> su banda de rodamiento optimizada mejora la estabilidad direccional y contribuye al ahorro de combustible.
                                </p>
                            </div>
                        </div><!-- /.description-text -->
                    </div><!-- /.col-md-6 -->
                   
                    
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="tecnical-specs">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="name">
                                        Especificaciones de la llanta 
                                    </h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="box-cart style3">
                                        <div class="btn-add-cart">
                                            <a href="{{ asset('docs/fichaTecnica/Hankook.pdf') }}" target="_blank" title="">Ver Ficha Técnica</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Velocidad</td>
                                        <td>220 KM/Hora</td>
                                    </tr>
                                    <tr>
                                        <td>Meterial</td>
                                        <td>Lona Resistente</td>
                                    </tr>
                                    <tr>
                                        <td>Peso</td>
                                        <td>40g</td>
                                    </tr>
                                    <tr>
                                        <td>Color</td>
                                        <td>blue, gray, green, light blue, lime, purple, red, yellow</td>
                                    </tr>
                                    <tr>
                                        <td>Medidas</td>
                                        <td>10.5mm</td>
                                    </tr>
                                    <tr>
                                        <td>Ancho</td>
                                        <td>33.3mm</td>
                                    </tr>
                                    <tr>
                                        <td>Tamaño</td>
                                        <td>Grande, Mediano, Pequeño</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /.tecnical-specs -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating">
                            <div class="title">
                                Basado en {{ $nro_comentarios }} calificaciones
                            </div>
                            <div class="score">
                                <div class="average-score">
                                    <p class="numb">{{ number_format($promedio_comentarios, 2) }}</p>
                                    <p class="text">Puntaje</p>
                                </div>
                                <div class="queue">
                                    <input type="hidden" class="rating" value="{{ $promedio_comentarios }}" data-filled="fa fa-star fa-2x rating-color" data-empty="fa fa-star-o fa-2x rating-color" data-readonly/>
                                </div>
                            </div>
                            <ul class="queue-box">
                                <li class="five-star">
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                    <span class="numb-star">5</span>
                                </li>
                                <li class="four-star">
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                    <span class="numb-star">4</span>
                                </li>
                                <li class="three-star">
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                    <span class="numb-star">3</span>
                                </li>
                                <li class="two-star">
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                    <span class="numb-star">2</span>
                                </li>
                                <li class="one-star">
                                    <span>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </span>
                                    <span class="numb-star">1</span>
                                </li>
                            </ul>
                        </div><!-- /.rating -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <div class="form-review">
                            <div class="title">
                                Agregar Calificación
                            </div>
                            <form action="{{ route('guardar-calificacion') }}" method="post">
                                <div class="your-rating queue">
                                    <span>Su calificación</span>
                                    <input type="hidden" class="rating" name="rating" value="0" data-filled="fa fa-star fa-2x rating-color" data-empty="fa fa-star-o fa-2x rating-color" required/>
                                </div>
                                {!! csrf_field() !!}
                                <input type="hidden" name="producto_id" value="{{ $detalleproducto->id }}">
                                <div class="review-form-name">
                                    <input type="text" name="nombres" value="" placeholder="Nombres" required>
                                </div>
                                <div class="review-form-name">
                                    <input type="text" name="apellidos" value="" placeholder="Apellidos" required>
                                </div>
                                <div class="review-form-email">
                                    <input type="email" name="email" value="" placeholder="Email" required>
                                </div>
                                <div class="review-form-comment">
                                    <textarea name="comentario" placeholder="Comentarios" required></textarea>
                                </div>
                                <div class="btn-submit">
                                    <button type="submit">Agregar calificación</button>
                                </div>
                            </form>
                        </div><!-- /.form-review -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-12">
                        @if($comentarios && $comentarios->count() > 0)
                        <ul class="review-list">
                            @foreach($comentarios as $comentario)
                            <li>
                                <div class="review-metadata">
                                    <div class="name">
                                        {{ $comentario->nombres }} {{ $comentario->apellidos }}: <span>{{ $comentario->fecha->format('d/m/Y h:i:s a') }}</span>
                                    </div>
                                    <div class="queue">
                                        <input type="hidden" class="rating" value="{{ $comentario->rating }}" data-filled="fa fa-star rating-color" data-empty="fa fa-star-o rating-color" data-readonly/>
                                    </div>
                                </div><!-- /.review-metadata -->
                                <div class="review-content">
                                    <p>
                                        {{ $comentario->comentario }} 
                                    </p> 
                                </div><!-- /.review-content -->
                            </li>
                            @endforeach
                        </ul><!-- /.review-list -->
                        @else
                        <h2>Este producto aún no tiene calificaciones.</h2>
                        @endif
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <iframe width="853" height="480" src="https://www.youtube.com/embed/eIkn0laTtRw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                       
                    </div>
                </div>
            </div><!-- /.container -->
        </section><!-- /.flat-product-content -->

        <section class="flat-imagebox style4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h3>Productos Relacionados</h3>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel-3">
                            @foreach($productos as $pro)
                            <div class="imagebox style4">
                                <div class="box-image">
                                    <a href="{{ route('detalle-producto')}}?producto={{ $pro->id }}" title="">
                                        <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                    </a>
                                </div><!-- /.box-image -->
                                <div class="box-content">
                                    <div class="cat-name">
                                        <a href="#" title="">{{ $pro->descripcion }}</a>
                                    </div>
                                    <div class="product-name">
                                        <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                    </div>
                                    <div class="price">
                                        <span class="sale">${{ number_format($pro->precio_venta) }}</span>
                                        <span class="regular">${{ number_format($pro->precio_venta) }}</span>
                                    </div>
                                </div><!-- /.box-content -->
                            </div><!-- /.imagebox style4 -->
                            @endforeach
                           
                        </div><!-- /.owl-carousel-3 -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-imagebox style4 -->

  @include('layouts.tienda.footer')  