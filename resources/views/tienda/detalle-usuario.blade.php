@include('layouts.tienda.master') 

      <section class="flat-checkout">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-checkout">
                            <form action="#" method="get" class="checkout" accept-charset="utf-8">
                                <div class="billing-fields">
                                    <div class="fields-title">
                                        <h3>Mi Cuenta</h3>
                                        <span></span>
                                        <div class="clearfix"></div>
                                    </div><!-- /.fields-title -->
                                    <div class="fields-content">
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="first-name">Nombres *</label>
                                                <input type="text" id="first-name" name="first-name" placeholder="Luis">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="last-name">Apellidos *</label>
                                                <input type="text" id="last-name" name="last-name" placeholder="Acosta">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="typeIdent">Tipo de Identificación *</label>
                                                <select style="border: 2px solid #e6e6e6; height: 40px;" id="typeIdent" >
                                                    <option value="cc">Cedula de Ciudadania</option>
                                                    <option value="nit">NIT</option>
                                                    <option value="ce">Cedula de Extrangeria</option>
                                                </select>
                                            </p>
                                            <p class="field-one-half">
                                                <label for="identication">Número de Identificación *</label>
                                                <input type="text" id="identication" name="identication" placeholder="123456">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="email-address">Email *</label>
                                                <input type="email" id="email-address" name="email-address">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="phone">Fecha de nacimiento *</label>
                                                <input type="text" id="phone" name="phone">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="celular">Celular *</label>
                                                <input type="number" id="celular" name="celular">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="phone">Teléfono *</label>
                                                <input type="text" id="phone" name="phone">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="address">Dirección *</label>
                                                <input type="text" id="address" name="address" placeholder="Indique su dirección">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="town-city">Ciudad *</label>
                                                <input type="text" id="town-city" name="town-city">
                                            </p>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="address">Contraseña *</label>
                                                <input type="text" id="address" name="address" placeholder="Indique su dirección">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="town-city">Repetir Contraseña *</label>
                                                <input type="text" id="town-city" name="town-city">
                                            </p>
                                        </div>
                                     
                                       
                                    </div><!-- /.fields-content -->
                                </div><!-- /.billing-fields -->
                            </form><!-- /.checkout -->
                        </div><!-- /.box-checkout -->
                    </div><!-- /.col-md-7 -->
                   
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-checkout -->

     
  
  @include('layouts.tienda.footer')  