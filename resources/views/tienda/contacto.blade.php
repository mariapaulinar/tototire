@include('layouts.tienda.master') 
@if(session()->has('ok'))
@if(session()->get('ok'))
<div class="row">
    <div class="col-md-12 alert alert-success text-center">
        <h3>{{ session()->get('mensaje') }}</h3>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12 alert alert-danger text-center">
        <h3>{{ session()->get('mensaje') }}</h3>
    </div>
</div>
@endif
@endif
<section class="flat-map">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="flat-map" class="pdmap">

                     <div class="col-md-12">
                        <div id="map" style="width:100%; height:400px"></div>
       
                    </div>
            <script>
               

                function initMap() {
                         var uluru = {lat: 4.604721, lng: -74.091383};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 17,
                            center: uluru
                        });
                        var marker = new google.maps.Marker({
                            position: uluru,
                            map: map
                        });
                    }
            </script>
                    
                </div><!-- /#flat-map -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /#flat-map -->

<section class="flat-contact style2">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h3>Déjenos su mensaje</h3>
                        <p>
                            Estamos atentos para ayudarlo. Escríbanos y con gusto, en el menor tiempo posibe, le responderemos.
                        </p>
                    </div><!-- /.form-contact-header -->
                    <div class="form-contact-content">
                        <form action="{{ route('contacto.enviar-email') }}" method="post" id="form-contact" accept-charset="utf-8">
                            {!! csrf_field() !!}
                            <div class="form-box one-half name-contact">
                                <label for="nombres">Nombres*</label>
                                <input type="text" id="nombres" name="nombres" placeholder="Escriba sus nombres" required>
                            </div>
                            <div class="form-box one-half password-contact">
                                <label for="apellidos">Apellidos*</label>
                                <input type="text" id="apellidos" name="apellidos" placeholder="Escriba sus apellidos" required>
                            </div>
                            <div class="form-box one-half password-contact">
                                <label for="telefono">Teléfono o Celular*</label>
                                <input type="text" id="telefono" name="telefono" placeholder="Escriba un número de teléfono o celular" required>
                            </div>
                            <div class="form-box one-half password-contact">
                                <label for="email">Correo electrónico*</label>
                                <input type="text" id="email" name="email" placeholder="Escriba un email de contacto" required>
                            </div>
                            <div class="form-box">
                                <label for="asunto">Asunto*</label>
                                <select  id="asunto" name="asunto" required style="border: 2px solid #e1e1e1;">
                                    <option value="" class="dropdown" selected="selected">Elija aquí el asunto de contacto</option>
                                    <option value="COTIZACION" class="dropdown">Cotización</option>
                                    <option value="PETICION" class="dropdown">Petición</option>
                                    <option value="QUEJA" class="dropdown">Queja</option>
                                    <option value="RECLAMO" class="dropdown">Reclamo</option>
                                    <option value="SUGERENCIA" class="dropdown">Sugerencia</option>
                                    <option value="FELICITACION" class="dropdown">Felicitación</option>
                                </select>
                                <!--input type="text" id="asunto" name="asunto" placeholder="Escriba una breve descripción de su mensaje" required-->
                            </div>
                            <div class="form-box">
                                <label for="mensaje">Su mensaje*</label>
                                <textarea id="mensaje" name="mensaje" placeholder="Escriba aquí su mensaje o solicitud" required></textarea>
                            </div>
                            <div class="form-box">
                                <button type="submit" class="contact">Enviar</button>
                            </div>
                        </form><!-- /#form-contact -->
                    </div><!-- /.form-contact-content -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-7 -->
             <div class="col-md-5">
                            <div class="box-contact">
                                <ul>
                                    <li class="address">
                                        <h3>Dirección</h3>
                                        <p>
                                            <strong>Vitrina Principal:</strong> Calle 8 No 23 - 07 <br/>
                                            <strong>Bodega:</strong> Carrera 23 # 7-37 <br/>
                                            Barrio Ricaurte <br />
                                            Bogotá Colombia.
                                        </p>
                                    </li>
                                    <li class="phone">
                                        <h3>Celular</h3>
                                        <p>
                                            (+57) 313 852 35 07 
                                        </p>
                                        <p>
                                            (+57) 310 564 04 90
                                        </p>
                                    </li>
                                    <li class="phone">
                                        <h3>Télefono</h3>
                                        <p>
                                            (+57) 031 371 14 83 
                                        </p>
                                        <p>
                                            (+57) 031 277 94 05
                                        </p>
                                    </li>
                                    <li class="email">
                                        <h3>Email</h3>
                                        <p>
                                            info@tototire.com<br/>
                                            gerencia@tototire.com<br/>
                                            llantastoto@gmail.com
                                        </p>
                                    </li>
                                    <li class="address">
                                        <h3>Nuestros Horarios</h3>
                                        <p>
                                            Lunes a Sabado: 8am a 6pm
                                        </p>
                                        <p>
                                            Domingo Y Festivo: Cerrado
                                        </p>
                                    </li>
                                    <li>
                                        <h3>Síganos</h3>
                                        <ul class="social-list style2">
                                            <li>
                                                <a href="#" title="">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="">
                                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="">
                                                    <i class="fa fa-google" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul><!-- /.social-list style2 -->
                                    </li>
                                </ul>
                            </div><!-- /.box-contact -->
                        </div><!-- /.col-md-5 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-contact style2 -->


@include('layouts.tienda.footer')  