@include('layouts.tienda.master')  

            <section class="flat-row flat-slider style4">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            <div class="slider style1 owl-carousel-11">
                                <div class="slider-item style6">
                                    <div class="item-text">
                                        <div class="header-item">
                                            <p>&nbsp;</p>
                                            <h2 class="name">  <span> </span></h2>
                                            <p> <br />  <br />  <br /> </p>
                                        </div>
                                        
                                    </div><!-- /.item-text -->
                                   
                                </div><!-- /.slider-item style6 -->
                                <div class="slider-item style4">
                                    <div class="item-text">
                                        <div class="header-item">
                                            <p>&nbsp;</p>
                                            <h2 class="name"><span style="font-size: 50px"><br/> </span></h2>
                                            <p><br /> </p>
                                        </div>
                                       
                                    </div><!-- /.item-text -->
                                    
                                </div><!-- /.slider-item style6 -->
                               
                                
                                
                            </div><!-- /.owl-carousel-11 style1 -->
                        </div><!-- /.col-lg-9 col-md-8 -->
                        <div class="col-lg-3 col-md-4">
                            <div class="counter style1 v1">
                                <!--span class="sale">Ahorre $48.100</span -->
                                <span class="offer">
                                    Oferta Especial
                                </span>
                                <div class="counter-content">
                                    <div class="box-content">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div>
                                        <div class="product-name">
                                            <a href="#" title=""><strong>BRIDGESTONE</strong> <br/> POTENZA 760</a>
                                        </div>
                                        <!--div class="price">
                                            <span class="sale">$250.000</span>
                                            <span class="regular">$298.100</span>
                                        </div-->
                                    </div><!-- /.box-content -->
                                    <div class="count-down">
                                        <div class="square">
                                            <div class="numb">
                                                18
                                            </div>
                                            <div class="text">
                                                DIAS
                                            </div>
                                        </div>
                                        <div class="square">
                                            <div class="numb">
                                                09
                                            </div>
                                            <div class="text">
                                                HORAS
                                            </div>
                                        </div>
                                        <div class="square">
                                            <div class="numb">
                                                23
                                            </div>
                                            <div class="text">
                                                SEGUNDOS 
                                            </div>
                                        </div>
                                    </div><!-- /.count-down -->
                                </div><!-- /.counter-content -->
                            </div><!-- /.counter -->
                        </div><!-- /.col-lg-3 col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-slider style4 -->

            <section class="flat-imagebox">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tab style2">
                                <ul class="tab-list">
                                    <li class="active">Nuevos Productos</li>
                                    <li>Marcas</li>
                                    <li>Mas vendidos</li>
                                </ul>
                            </div><!-- /.product-tab -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                    <div class="box-product">
                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <span class="item-new">Nuevo</span>
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content" style="padding: 8px">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div -->
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                           
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>  
                          
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3"> 
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content" style="padding: 8px">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div-->
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            <!--div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                Total: <span  id="totalProducto" style="display: inherit;"> $1.440.000</span>
                                            </div-->
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>  
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3"> 
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content" style="padding: 8px">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div-->
                                        </div><!-- /.box-content -->
                                        
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            <!--div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                Total: <span  id="totalProducto" style="display: inherit;"> $1.440.000</span>
                                            </div -->
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>  
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <span class="item-sale">SALE</span>
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content" style="padding: 8px">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div-->
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            <!--div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                Total: <span  id="totalProducto" style="display: inherit;"> $1.440.000</span>
                                            </div-->
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <span class="item-sale">SALE</span>
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div-->
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            <!--div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                Total: <span  id="totalProducto" style="display: inherit;"> $1.440.000</span>
                                            </div-->
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            <!--div class="price">
                                                <span class="sale">$380.000</span>
                                                <span class="regular">$396.000</span>
                                            </div -->
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            <!--div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                Total: <span  id="totalProducto" style="display: inherit;"> $1.440.000</span>
                                            </div -->
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                           
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                           
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                              
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                           
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                               
                            </div><!-- /.col-sm-6 col-lg-3 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <span class="item-sale">SALE</span>
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                               
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                           
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                               
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                           
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                               
                            </div><!-- /.col-sm-6 col-lg-3 -->
                            <div class="col-sm-6 col-lg-3">
                                <div class="product-box style4">
                                    <div class="imagebox">
                                        <span class="item-sale">SALE</span>
                                        <div class="box-image owl-carousel-1">
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                            </a>
                                            <a href="#" title="">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </a>
                                        </div><!-- /.box-image -->
                                        <div class="box-content">
                                            <div class="cat-name">
                                                <a href="#" title="">Bridgestone</a>
                                            </div>
                                            <div class="product-name">
                                                <a href="#" title="">235/75 R15<br />Dueler AT693</a>
                                            </div>
                                            
                                        </div><!-- /.box-content -->
                                        <div class="box-bottom pay_button">
                                            <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                <div class="btn-count">
                                                    <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                    <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                </div>
                                                <div class="btn-add-cart ">
                                                    <a href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_1" data-cod="1" data-precio-venta="380000" onclick="carrito(this);" >Agregar
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.imagebox -->
                                </div>
                                
                            </div><!-- /.col-sm-6 col-lg-3 -->
                        </div><!-- /.row -->
                    </div><!-- /.box-product -->
                    <div class="divider10"></div>
                </div><!-- /.container -->
            </section><!-- /.flat-imagebox -->

            <section class="flat-row flat-banner-box">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="banner-box one-half">
                                <div class="inner-box">
                                    <a href="#" title="">
                                        <img src="{{ asset('img/images/banner_boxes/home-01.jpg') }} " alt="" style="height: 255px">
                                    </a>
                                </div><!-- /.inner-box -->
                                <div class="inner-box">
                                    <a href="#" title="">
                                        <img src="{{ asset('img/images/banner_boxes/home-05.jpg') }}" alt="" style="height: 255px">
                                    </a>
                                </div><!-- /.inner-box -->
                                <div class="clearfix"></div>
                            </div><!-- /.box -->
                            <div class="banner-box">
                                <div class="inner-box">
                                    <a href="#" title="">
                                        <img src="{{ asset('img/images/banner_boxes/home-04.jpg') }}" alt="">
                                    </a>
                                </div>
                            </div><!-- /.box -->
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-5">
                            <div class="banner-box">
                                <div class="inner-box">
                                    <a href="#" title="">
                                        <img src="{{ asset('img/images/banner_boxes/home-03.jpg') }}" alt="">
                                    </a>
                                </div><!-- /.inner-box -->
                            </div><!-- /.box -->
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
                <div class="divider14">
                </div>
            </section><!-- /.flat-banner-box -->

      
            <section class="flat-imagebox">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="flat-row-title">
                                <h3>Más Vistas</h3>
                            </div>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="owl-carousel-3">
                                @foreach($productos as $pro)
                                
                                <div class="imagebox style4">
                                    <div class="box-image">
                                        <a href="#" title="">
                                            <img src="{{ asset('img/productos/bridgestone_dos.png') }}" alt="">
                                        </a>
                                    </div><!-- /.box-image -->
                                    <div class="box-content">
                                        <div class="cat-name">
                                            <a href="#" title="">{{ $pro->marca->descripcion }}</a>
                                        </div>
                                        <div class="product-name">
                                            <a href="#" title="">{{ $pro->descripcion }}<br />Existencia: {{ $pro->existencia }}</a>
                                        </div>
                                        <div class="price">
                                            <span class="sale">${{ number_format($pro->precio_venta) }}</span>
                                            <span class="regular">${{ number_format($pro->precio_venta) }}</span>
                                        </div>
                                        <div class="box-bottom" style="    opacity: inherit;   transform: none;    position: inherit;">
                                            <div class="btn-add-cart">
                                                <a href="#" title="">
                                                    <img src="{{ asset('img/images/icons/add-cart.png') }}" alt="">Agregar 
                                                </a>
                                            </div>
                                            
                                        </div><!-- /.box-bottom -->
                                    </div><!-- /.box-content -->
                                </div><!-- /.imagebox style4 -->
                                @endforeach
                                
                            </div><!-- /.owl-carousel-3 -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-imagebox -->

          
       
@include('layouts.tienda.footer')  