@include('layouts.tienda.master') 
<section class="flat-contact style2">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 60%"><strong>SERVICIO E INSTALACIÓN DE LLANTAS</strong></h2>
                        <p style="font-size: 1.1em; text-align: justify">
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
               <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 60%"><strong>ALINEACIÓN Y BALANCEO</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 60%"><strong>REPARACIÓN Y PINTURA DE RINES</strong></h2>
                        <p style="font-size: 1.1em; text-align: justify">
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
               <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 60%"><strong>ROTACIÓN DE LLANTAS</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 60%"><strong>REVISIÓN DE RINES Y VÁLVULAS</strong></h2>
                        <p style="font-size: 1.1em; text-align: justify">
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                           ............................................<br/>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div>
    </div><!-- /.container -->
</section><!-- /.flat-contact style2 -->

@include('layouts.tienda.footer')  