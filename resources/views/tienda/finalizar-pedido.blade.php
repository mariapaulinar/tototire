@include('layouts.tienda.master') 

      <section class="flat-checkout">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="box-checkout">
                            <h3 class="title">Finalizar Compra</h3>
                            <div class="checkout-login">
                                Eres cliente registrado? <a href="{{ route('registrarse-autenticarse') }}" title="">Click acá para ingresar</a>
                            </div>
                            <form action="#" method="get" class="checkout" accept-charset="utf-8">
                                <div class="billing-fields">
                                    <div class="fields-title">
                                        <h3>Detalle de la compra</h3>
                                        <span></span>
                                        <div class="clearfix"></div>
                                    </div><!-- /.fields-title -->
                                    <div class="fields-content">
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="first-name">Nombres *</label>
                                                <input type="text" id="first-name" name="first-name" placeholder="Luis">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="last-name">Apellidos *</label>
                                                <input type="text" id="last-name" name="last-name" placeholder="Acosta">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <p class="field-one-half">
                                                <label for="email-address">Email *</label>
                                                <input type="email" id="email-address" name="email-address">
                                            </p>
                                            <p class="field-one-half">
                                                <label for="phone">Teléfono *</label>
                                                <input type="text" id="phone" name="phone">
                                            </p>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="field-row">
                                            <label for="address">Dirección *</label>
                                            <input type="text" id="address" name="address" placeholder="Indique su dirección">
                                            <input type="text" id="address-2" name="address" placeholder="Apartamento, conjunto, torre (opcional)">
                                        </div>
                                        <div class="field-row">
                                            <label for="town-city">Ciudad *</label>
                                            <input type="text" id="town-city" name="town-city">
                                        </div>
                                        <div class="checkbox">
                                            <input type="checkbox" id="create-account" name="create-account" checked>
                                            <label for="create-account">Crear una cuenta?</label>
                                        </div>
                                        <div class="field-row">
                                            <label for="notes">Observaciones</label>
                                            <textarea id="notes" placeholder="Describa las caracteristicas especiales"></textarea>
                                        </div>
                                    </div><!-- /.fields-content -->
                                </div><!-- /.billing-fields -->
                            </form><!-- /.checkout -->
                        </div><!-- /.box-checkout -->
                    </div><!-- /.col-md-7 -->
                    <div class="col-md-5">
                        <?php $productos = App\Pedido::obtenerProductosCarrito() ?>
                        @if(count($productos) > 0)
                        <?php $total = 0 ; $cantidad = 0 ?>
                        <div class="cart-totals style2">
                            <h3>Su pedido</h3>
                            <form action="#" method="get" accept-charset="utf-8">
                                <table class="product">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">Producto</th>
                                            <th style="text-align: center">Cant.</th>
                                            <th style="text-align: center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productos as $codigo => $p)
                                    <?php $total += $p['precio_venta'] * $p['cantidad'] ; $cantidad += $p['cantidad']; ?>
                                        <tr>
                                            <td>{{ $p['descripcion'] }}</td>

                                            <td style="text-align: center">
                                                <div class="quanlity">
                                                    <!--span class="btn-down"></span-->
                                                    <input type="number" name="number" value="{{ number_format($p['cantidad']) }}" min="1" max="100" placeholder="Cantidad" id="cantidad_finalizar">
                                                    <!--span class="btn-up"></span-->
                                                </div>
                                            </td>
                                            <td>${{ number_format($p['precio_venta'] * $p['cantidad']) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table><!-- /.product -->
                                <?php
                                    $subtotal =  $total / 1.19;
                                    $iva = $total - $subtotal;
                                ?>
                                <br/>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td class="subtotal">${{ number_format($subtotal) }}</td>
                                        </tr>
                                        <tr>
                                            <td>IVA</td>
                                            <td class="subtotal">${{ number_format($iva) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td class="price-total">${{ number_format($total) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="btn-radio style2">
                                    <div class="radio-info">
                                        <input type="radio" id="flat-payment" checked name="radio-cash-2">
                                        <label for="flat-payment">Formas de pago</label>
                                        <p>Por favor seleccione la forma de pago que desea utilizar.</p>
                                    </div>
                                   
                                    <div class="radio-info">
                                        <input type="radio" id="paypal" name="radio-cash-2">
                                        <label for="paypal">Debito</label>
                                    </div>
                                    <div class="radio-info">
                                        <input type="radio" id="paypal" name="radio-cash-2">
                                        <label for="paypal">Credito</label>
                                    </div>
                                    <div class="radio-info">
                                        <input type="radio" id="cash-delivery" name="radio-cash-2">
                                        <label for="cash-delivery">Pago contra entrega</label>
                                    </div>
                                </div><!-- /.btn-radio style2 -->
                                <div class="checkbox">
                                    <input type="checkbox" id="checked-order" name="checked-order" checked>
                                    <label for="checked-order">Acepto terminos y condiciones *</label>
                                </div><!-- /.checkbox -->
                                <div class="btn-order">
                                    <a href="#" class="order" title="">Realizar Pago</a>
                                </div><!-- /.btn-order -->
                            </form>
                        </div><!-- /.cart-totals style2 -->
                        @endif
                    </div><!-- /.col-md-5 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-checkout -->

     
  
  @include('layouts.tienda.footer')  