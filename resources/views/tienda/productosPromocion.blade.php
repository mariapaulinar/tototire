@include('layouts.tienda.master') 

        <main id="shop" class="style2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-shop">
                            <div class="wrap-imagebox">
                                <div class="flat-row-title style4">
                                    <h3>Llantas para el vehiculo BMW 320D SPORT 2017</h3>
                                    <!--span>
                                        Mostrar 1–12 de 20 resultados
                                    </span-->
                                    <div class="clearfix"></div>
                                </div><!-- /.flat-row-title style4 -->
                                <div class="blog-pagination style1">
                                
                                <ul class="flat-pagination style1">
                                    {!! $productos->links('tienda.paginador') !!}
                                </ul><!-- /.flat-pagination -->
                                <div class="clearfix"></div>
                            </div><!-- /.blog-pagination -->
                                <!--div class="sort-product style1">
                                   
                                    
                                    <div class="sort">
                                        <div class="popularity">
                                            <select name="popularity">
                                                <option value="">Promociones</option>
                                                <option value="">Mas vendidos</option>
                                                <option value="">Mas baratos</option>
                                                <option value="">Nuevas</option>
                                            </select>
                                        </div>
                                        <div class="showed">
                                            <select name="showed">
                                                <option value="">Mostrar 12</option>
                                                <option value="">Mostrar 24</option>
                                                <option value="">Mostrar 36</option>
                                                <option value="">Mostrar 48</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.sort-product style1 -->
                                <div class="row">
                                    @foreach($productos as $pro)
                                    <div class="col-lg-4 col-md-4 col-sm-6" >
                                        <div class="product-box">
                                            <div class="imagebox">
                                                
                                                <span class="item-sale">PROMO</span>

                                                <div class="box-image owl-carousel-1">
                                                    <div class="image">
                                                        <a href="{{ route('detalle-producto')}}?producto={{ $pro->id }}" title="">
                                                            <img src="{{ asset('img/productos/bridgestone.png') }}" alt="" class="img_tamaño" style="width: 65%">
                                                        </a>
                                                    </div>
                                                    <div class="image">
                                                        <a href="#" title="">
                                                            <img src="{{ asset('img/productos/bridgestone.png') }}" alt="" class="img_tamaño"  style="width: 65%">
                                                        </a>
                                                    </div>
                                                   
                                                </div><!-- /.box-image -->
                                                    <div class="box-content">
                                                    <div class="cat-name" style="padding: 8px">
                                                        <a href="#" title="">{{ $pro->descripcion }}</a>
                                                    </div>
                                                    <div class="product-name">
                                                        <div class="col-md-12">
                                                            <span><strong>235/75R15</strong> Dueler AT693</span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="price" >
                                                                 <span class="sale" id="sale" data-valor-unitario="{{ $pro->precio_venta }}">Valor Unitario: ${{ number_format($pro->precio_venta) }}</span>
                                                                 <!--span class="regular">${{ number_format($pro->precio_venta) }}</span-->
                                                                 
                                                            </div>
                                                        </div>
                                                        <div class="producto col-md-12" style="text-align: center">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="box-buy" style="display: inline-flex;width: 100%" >
                                                                        <input type="text" value="4" class="cantidad" style="border: 1px solid #313130; font-size: 16px; height: 45px;  margin: 0 0 0 10px;  text-align: center;  outline: 0;  width: 70px; padding: 0px">
                                                                        <div class="btn-count">
                                                                            <a href="#" onclick="event.preventDefault(); aumentarCantidad(this);" data-valor-unitario="{{ $pro->precio_venta }}" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">+</a>
                                                                            <a href="#" onclick="event.preventDefault(); disminuirCantidad(this);" data-valor-unitario="{{ $pro->precio_venta }}" data-cant-promo="" style="    background: #313130; color: #FFF; display: block; font-size: 10px; height: 20px; margin: 0 3px 1px; padding: 0px 0px; text-align: center; width: 20px; border-radius: 10px; line-height: 19px">-</a>

                                                                        </div>
                                                                        <div class="btn-add-cart ">
                                                                            <a class="agregar_carrito" href="#" title="" style="width: 80px; text-align: left; padding: 0 15px;" id="addcarrito_{{ $pro->id }}" data-cod="{{ $pro->id }}" data-precio-venta="{{ floor($pro->precio_venta) }}" onclick="carrito(this);" >Agregar
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- /.box-bottom -->
                                                                <div class="col-md-2">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <br/>
                                                            </div>
                                                            <div class="row">
                                                                <div style="font-size: 18px; color:#ab191c; font-family: 'BloggerSans'; display: initial; " class=" ">
                                                                    Total: <span class="totalProducto" style="display: inherit;"> ${{ number_format($pro->precio_venta * 4) }} </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.box-content -->
                                            </div><!-- /.imagebox -->
                                        </div><!-- /.product-box -->
                                    </div><!-- /.col-lg-3 col-md-4 col-sm-6 -->
                                    @endforeach
                                   
                                    
                            </div><!-- /.wrap-imagebox -->
                            <div class="blog-pagination style1">
                             
                                <ul class="flat-pagination style1">
                                    {!! $productos->links('tienda.paginador') !!}
                                </ul><!-- /.flat-pagination -->
                                <div class="clearfix"></div>
                            </div><!-- /.blog-pagination -->
                        </div><!-- /.main-shop -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </main><!-- /#shop -->

  @include('layouts.tienda.footer')  