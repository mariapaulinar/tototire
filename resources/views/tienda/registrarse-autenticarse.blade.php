@include('layouts.tienda.master') 

    <section class="flat-account background">
            <div class="container">
                @if($mensaje != '')
                <div class="row">
                    <span>{{ $mensaje}}</span>
                </div>
                @endif
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-register" style="height: auto">
                            <div class="title">
                                <h3>Registrarse</h3>
                            </div>
                            <form action="#" method="post" id="formregister" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box">
                                        <label for="firstname">Nombre * </label>
                                        <input type="text" id="firtsname" name="firtsname" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box">
                                        <label for="lastname">Apellido * </label>
                                        <input type="text" id="lastname" name="lastname" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box" >
                                        <label for="type">Tipo Identificación * </label>
                                        <select style="border: 2px solid #e6e6e6; height: 40px;" id="typeIdent" name="typeIdent">
                                            <option value="cc">Cédula de Ciudadanía</option>
                                            <option value="nit">NIT</option>
                                            <option value="ce">Cédula de Extranjería</option>
                                        </select>
                                        
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box">
                                        <label for="identification">Identificación * </label>
                                        <input type="text" id="identification" name="identification" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-12" style="padding: 0">
                                    <div class="form-box">
                                        <label for="email">Email * </label>
                                        <input type="text" id="email" name="email" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box">
                                        <label for="password">Contraseña</label>
                                        <input type="password" id="password" name="password" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                                <div class="col-md-6" style="padding: 0">
                                    <div class="form-box">
                                        <label for="password-register">Repetir Contraseña</label>
                                        <input type="password" id="password-register" name="password-register" required="required">
                                    </div><!-- /.form-box -->
                                </div>
                               
                                <div class="form-box">
                                    <button type="submit" class="register">Registrarse</button>
                                </div><!-- /.form-box -->
                            </form><!-- /#form-register -->
                        </div><!-- /.form-register -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <div class="form-login">
                            <div class="title">
                                <h3>Autenticarse</h3>
                            </div>
                            <form action="{{ route('login') }}" method="post" id="form-login" accept-charset="utf-8">
                                {!! csrf_field() !!}
                                <div class="form-box">
                                    <label for="name-login">Email * </label>
                                    <input type="text" id="name-login" name="email" placeholder="sucorreo@email.com">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div><!-- /.form-box -->
                                <div class="form-box">
                                    <label for="password-login">Contraseña * </label>
                                    <input type="password" id="password-login" name="password" placeholder="******">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div><!-- /.form-box -->
                                <div class="form-box checkbox">
                                    <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="remember">Recordarme</label>
                                </div><!-- /.form-box -->
                                <div class="form-box">
                                    <button type="submit" class="login">Ingresar</button>
                                    <a href="{{ route('password.request') }}" title="">Olvidé mi contraseña</a>
                                </div><!-- /.form-box -->
                            </form><!-- /#form-login -->
                        </div><!-- /.form-login -->
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-account -->

   
  
 @include('layouts.tienda.footer')  
                        </div><!-- /.form-login -->
<script type="text/javascript">
            
            </script>