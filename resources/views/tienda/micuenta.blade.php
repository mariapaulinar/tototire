@include('layouts.tienda.master') 

            <section class="flat-contact style2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-contact left">
                                <div class="form-contact-header">
                                    <h3>Bienvenid@ <strong>{{ auth()->user()->name }}</strong></h3>
                                    <p>
                                       
                                    </p>
                                </div><!-- /.form-contact-header -->
                                <div class="form-contact-content">
                                    <form action="#" method="get" id="form-contact" accept-charset="utf-8">
                                        <div class="form-box one-half tel1-contact">
                                            <label for="tel1-contact">Teléfono Principal</label>
                                            <input type="text" id="tel1-contact" name="tel1-contact" placeholder="XXX XXX XX XX" value="{{ auth()->user()->telefono1 }}"> 
                                        </div>
                                        <div class="form-box one-half tel2-contact">
                                            <label for="tel2-contact">Teléfono Secundario</label>
                                            <input type="text" id="tel2-contact" name="tel2-contact" placeholder="XXX XXX XX XX" value="{{ auth()->user()->telefono2 }}"> 
                                        </div>
                                        <div class="form-box one-half dir-contact">
                                            <label for="dir-contact">Direccion</label>
                                            <input type="text" id="dir-contact" name="dir-contact" placeholder="Cra XX N° XX-XX" value="{{ auth()->user()->direccion }}"> 
                                        </div>
                                        <div class="form-box one-half sexo-contact">
                                            <label for="sexo-contact">Sexo</label>
                                            <input type="text" id="sexo-contact" name="sexo-contact" placeholder="M ó F" value="{{ auth()->user()->sexo }}"> 
                                        </div>
                                        <div class="form-box one-half sexo-contact">
                                            <label for="sexo-contact">Contraseña Actual</label>
                                            <input type="text" id="sexo-contact" name="sexo-contact" placeholder="" > 
                                        </div>
                                        <div class="form-box one-half sexo-contact">
                                            <label for="sexo-contact">Cambiar Contraseña</label>
                                            <input type="text" id="sexo-contact" name="sexo-contact" placeholder="" > 
                                        </div>
                                       
                                        <div class="form-box">
                                            <button type="submit" class="contact">Actualizar Datos</button>
                                        </div>
                                    </form><!-- /#form-contact -->
                                </div><!-- /.form-contact-content -->
                            </div><!-- /.form-contact left -->
                        </div><!-- /.col-md-7 -->
                        <div class="col-md-5">
                            <div class="box-contact">
                                <ul>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <li class="address">
                                                <h3>Dirección</h3>
                                                <p>
                                                   Calle 8 No 23 - 07 <br/>Barrio Ricaurte <br />Bogotá Colombia.
                                                </p>
                                            </li>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <li class="phone">
                                                <h3>Télefono</h3>
                                                <p>
                                                    (+57) 310 696 40 65
                                                </p>
                                                <p>
                                                    (+57) 310 696 40 65
                                                </p>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6 col-xs-6">
                                            <li class="address">
                                                <h3>Nuestros Horarios</h3>
                                                <p>
                                                    Lunes a viernes: 8am a 6pm
                                                </p>
                                                <p>
                                                    Sabados: 8am a 4pm
                                                </p>
                                                <p>
                                                    Domingo: 8am a 12pm
                                                </p>
                                            </li>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <li class="email">
                                                <h3>Email</h3>
                                                <p>
                                                    gerencia@tototire.com
                                                </p>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <li>
                                                <h3>Síganos</h3>
                                                <ul class="social-list style2">
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-dribbble" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" title="">
                                                            <i class="fa fa-google" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul><!-- /.social-list style2 -->
                                            </li>
                                        </div>
                                    </div>
                                </ul>
                            </div><!-- /.box-contact -->
                        </div><!-- /.col-md-5 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-contact style2 -->

@include('layouts.tienda.footer')  