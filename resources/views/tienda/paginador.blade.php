@if($paginator->count() > 0)
    @foreach ($elements as $element)
        @if (is_string($element))
        <li style="color: #000;">{{ $element }}</li>
        @endif
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                <li class="active">
                    <a href="#" class="waves-effect" onclick="event.preventDefault();" title="">{{ $page }}</a>
                </li>
                @else
                <li>
                    <a href="{{ $url }}" class="waves-effect" title="Ir a la página {{ $page }}">{{ $page }}</a>
                </li>
                @endif
            @endforeach
        @endif
    @endforeach
@endif