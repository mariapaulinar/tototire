<?php $productos = App\Pedido::obtenerProductosCarrito() ?>
<?php $total = 0 ; $cantidad = 0 ?>
@if(count($productos) > 0)
@foreach($productos as $codigo => $p)
<?php $total += $p['precio_venta'] * $p['cantidad'] ; $cantidad += $p['cantidad']; ?>
@endforeach
@endif
<a href="#" title="">
    <div class="icon-cart">
        <img src="{{ asset('img/images/icons/add-cart.png') }}" alt="" style="padding: 10px;">
        <span id="cantidad_carrito">{{$cantidad}}</span>
    </div>
    <div class="price" id="precio_carrito">
        ${{ number_format($total)}}
    </div>
</a>
<div class="dropdown-box">
    @if(count($productos) > 0)
    @foreach($productos as $codigo => $p)
    <ul>
        <li>
            <div class="img-product">
                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
            </div>
            <div class="info-product">
                <div class="name">
                    {{ $p['descripcion'] }}
                </div>
                <div class="price">
                    <span>{{ number_format($p['cantidad']) }} x</span>
                    <span>${{ number_format($p['precio_venta']) }}</span>
                </div>
            </div>
            <div class="clearfix"></div>
            <a href="#" title="" onclick="event.preventDefault(); borrarProductoCarrito('{{ $codigo }}');">
                <img src="{{ asset('img/images/icons/delete.png') }}" alt="">
            </a>

        </li>
    </ul>
    @endforeach
    <?php
    $subtotal = $total / 1.19;
    $iva = $total - $subtotal;
    ?>
    <div class="total">
        <span>Subtotal:</span>
        <span class="price">${{ number_format($subtotal) }}</span>
    </div>
    <div class="total">
        <span>IVA:</span>
        <span class="price">${{ number_format($iva) }}</span>
    </div>
    <div class="total">
        <span>Total:</span>
        <span class="price">${{ number_format($total) }}</span>
    </div>
    <div class="row btn-cart" style="    margin: 17px 0px 0 50px;">
        <div class="col-md-12">
            <a href="{{ route('finalizar-pedido')}}" class="view-cart" title="">Ver Carrito</a>
        </div>
        <!--a href="shop-checkout.html" class="check-out" title="">Finalizar</a-->
    </div>
    @else
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr class="trhead">
            <td>No hay productos</td>
        </tr>
    </table>
    @endif
</div>


