@include('layouts.tienda.master') 

    <div class="row">
        <div class="col-md-12 col-xs-12" style="padding:70px">
        <div class="cont maincont">
            <h1><span style="    width: 360px; font-size: 0.8em;">Rastreo de mis pedidos (Tracking)</span></h1>
            <span class="maincont-line1"></span>
            <span class="maincont-line2"></span>

         
            <div class="row">
                <div class="panel-group tracking table-responsive" id="accordion" role="tablist" aria-multiselectable="true">
                    <table class="bills-list table" width="100%">
                        <tbody>
                            <tr class="trhead">
                                <td class="bills-list-pedidos">Fecha Pedido</td>
                                <td class="bills-list-pedidos">Fecha Facturación</td>
                                <td class="bills-list-pedidos">Número Pedido</td>
                                <td class="bills-list-pedidos">Número Factura</td>
                                <td class="bills-list-pedidos">Estado Pedido</td>
                                <td class="bills-list-pedidos">Valor Total</td>
                                <td>Transportadora</td>
                                <td>Guia</td>
                            </tr>
                            <?php  $i=0; ?>

                            <tr class="trbody panel-heading" style="    background-position: 5px center;    cursor: pointer;  border-top: 5px solid #f4f5fb;">
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2005</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">12345</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">FACTURADO</td>
                                <td>$450.000</td>
                                <?php 
                                    $url =asset('img/deprisa.jpg');
                                ?>
                                <td> <img src="{{ asset('img/deprisa.jpg')}}"  height="38"  style="width:25%"/> </td>
                                <td><u><a href="https://www.deprisa.com//Tracking/index/?track=" class="btn" target="_blank"> #245454545</u></a></td>
                            </tr>
                            <tr class="trbody panel-heading" style="    background-position: 5px center;    cursor: pointer;  border-top: 5px solid #f4f5fb;">
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2005</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">12345</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">FACTURADO</td>
                                <td>$450.000</td>
                                <?php 
                                    $url =asset('img/deprisa.jpg');
                                ?>
                                <td> <img src="{{ asset('img/deprisa.jpg')}}"  height="38" style="width:25%" /> </td>
                                <td><u><a href="https://www.deprisa.com//Tracking/index/?track=" class="btn" target="_blank"> #245454545</u></a></td>
                            </tr>
                            <tr class="trbody panel-heading" style=" background-position: 5px center;    cursor: pointer;  border-top: 5px solid #f4f5fb;">
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2018-05-02</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">2005</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">12345</td>
                                <td class="panel-heading" title="Detalle" aria-expanded="false" data-toggle="collapse" href="#collapse<?php echo $i; ?>">FACTURADO</td>
                                <td>$450.000</td>
                                <?php 
                                    $url =asset('img/deprisa.jpg');
                                ?>
                                <td> <img src="{{ asset('img/deprisa.jpg')}}"  height="38"  style="width:25%"/> </td>
                                <td><u><a href="https://www.deprisa.com//Tracking/index/?track=" class="btn" target="_blank"> #245454545</u></a></td>
                            </tr>
                          
                            <?php $i++; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
 @include('layouts.tienda.footer')      