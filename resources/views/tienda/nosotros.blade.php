@include('layouts.tienda.master') 
<section class="flat-contact style2">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 20%"><strong>MISIÓN</strong></h2>
                        <p style="font-size: 1.1em; text-align: justify">
                           Ofrecer a nuestros clientes una completa gama de llantas, Rines. Adicional servicio, reparación y pintura . Acompañada de una asesoría especializada para satisfacer sus necesidades y requerimientos mejorando su seguridad y confort apoyados en nuestra experiencia, calidad y excelente servicio.
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
               <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 20%"><strong>VISIÓN</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                           Ser una de las principales empresas del sector automotriz en ofrecer los mejores productos de llantas y rines  disponibles en el mercado, que le den a su vehículo no solo mayor estabilidad y capacidad de respuesta sino también un toque único y original con excelentes precios, servicio y calidad.
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 45%"><strong>NUESTRA EMPRESA</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                           TOTO TIRE es una empresa abocada a la comercialización, distribución y venta de llantas, rines  nacionales e importados, con más de 20 años de trayectoria en el mercado, dotando a la industria automotriz, en vehículos pesados, camionetas y automóviles en Colombia. Comprometida con las políticas de calidad y seguridad vigentes, el cual garantiza el compromiso de esta para sus clientes.
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-7 -->

        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 45%"><strong>POLÍTICA DE CALIDAD</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                           Como consecuencia de nuestros principios, TOTO TIRE. Se compromete con la siguiente Política de Calidad:</strong>
                            <ul>
                                <li style="font-size: 1.1em;  text-align: justify">
                                   <strong> * </strong> Asegurar el cumplimiento de lo acordado con nuestros clientes, brindándoles un servicio y soluciones de calidad.
                                </li>
                                <li style="font-size: 1.1em;  text-align: justify">
                                   <strong> * </strong>Comprometer a toda empresa, proveedores y socios comerciales con elevados estándares de calidad en los servicios y productos.
                                </li>
                                <li style="font-size: 1.1em;  text-align: justify">
                                    <strong> * </strong>Evaluar y reconocer por la calidad, en trabajo realizado por los colaboradores, individual o colectivamente, así como por los proveedores y socios comerciales.
                                </li>
                            </ul>
                           
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-7 -->
            
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-7">
                <div class="form-contact left">
                    <div class="form-contact-header">
                        <h2 style="border-bottom: 2px solid #ab191c; width: 20%"><strong>SERVICIOS</strong></h2>
                        <p style="font-size: 1.1em;  text-align: justify">
                            TOTO TIRE. Ofrece un servicio de comercialización distribución y venta de llantas, rines y servicio de reencauche tipo vehículos pesados y camiones livianos. Rectificación y pintura, alineación y balanceo. Para el apoyo de la presentación del servicio TOTO TIRE, cuenta con la infraestructura necesaria para cumplir con los requisitos de sus clientes, donde destacamos:
                        </p>
                        <ul>
                            <li style="font-size: 1.1em;  text-align: justify">
                                <strong> * </strong>Oficinas e instalaciones propias, maquinaria y herramientas apropiadas para desarrollar nuestra actividad, con un gran inventario de productos en vitrina donde también atendemos a nuestros clientes, (ubicados en el sector estratégico para la operación hacia los distintos destinos).
                            </li>
                            <li style="font-size: 1.1em;  text-align: justify">
                                <strong> * </strong>Para la entrega de nuestra mercancía, en Bogotá disponemos de vehículos propios; a nivel nacional contamos con una alianza estratégica para mayor cumplimiento con nuestros clientes.
                            </li>
                        </ul>
                        <p style="font-size: 1.1em;  text-align: justify">
                            <strong>Estos servicios tienen una cobertura geográfica en todo el territorio Colombiano.</strong>
                        </p>
                    </div><!-- /.form-contact-header -->
                </div><!-- /.form-contact left -->
            </div><!-- /.col-md-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-contact style2 -->

@include('layouts.tienda.footer')  