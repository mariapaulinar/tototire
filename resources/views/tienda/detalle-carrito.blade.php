@include('layouts.tienda.master') 

        <section class="flat-shop-cart">
            <div class="container">
                <div class="row">
                    <?php $productos = App\Pedido::obtenerProductosCarrito() ?>
                    @if(count($productos) > 0)
                    <?php $total = 0 ; $cantidad = 0 ?>
                    <div class="col-lg-8" style="height: auto">
                        <div class="flat-row-title style1">
                            <h3>Detalle del pedido</h3>
                        </div>
                        <div class="table-cart">
                            <table>
                                <thead>
                                    <tr>
                                        <th style="text-align: center">Producto</th>
                                        <th style="text-align: center">Cantidad</th>
                                        <th style="text-align: center">Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                @foreach($productos as $codigo => $p)
                                  <?php $total += $p['precio_venta'] * $p['cantidad'] ; $cantidad += $p['cantidad']; ?>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="img-product">
                                                <img src="{{ asset('img/productos/bridgestone.png') }}" alt="">
                                            </div>
                                            <div class="name-product">
                                                {{ $p['descripcion'] }}
                                            </div>
                                            <!--div class="price">
                                                ${{ number_format($p['precio_venta']) }}
                                            </div-->
                                            <div class="clearfix"></div>
                                        </td>
                                        <td>
                                            <div class="quanlity">
                                                <!--span class="btn-down"></span-->
                                                <input type="number" name="number" value="{{ number_format($p['cantidad']) }}" min="1" max="100" placeholder="Cantidad">
                                                <!--span class="btn-up"></span-->
                                            </div>
                                        </td>
                                        <td>
                                            <div class="total">
                                                ${{ number_format($p['precio_venta'] * $p['cantidad']) }}
                                            </div>
                                        </td>
                                        <td>
                                            
                                            <a href="#" title="" onclick="event.preventDefault(); borrarProductoCarrito('{{ $codigo }}');">
                                                <img src="{{ asset('img/images/icons/delete.png') }}" alt="">
                                            </a>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                                 @endforeach
                            </table>
                            <!--div class="form-coupon">
                                <form action="#" method="get" accept-charset="utf-8">
                                    <div class="coupon-input">
                                        <input type="text" name="coupon code" placeholder="Coupon Code">
                                        <button type="submit">Apply Coupon</button>
                                    </div>
                                </form>
                            </div><!-- /.form-coupon -->
                        </div><!-- /.table-cart -->
                       
                        <?php
                            $subtotal =  $total / 1.19;
                            $iva = $total - $subtotal;
                        ?>
                    </div><!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="cart-totals">
                            <h3>Total del pedido</h3>
                            <form action="#" method="get" accept-charset="utf-8">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td class="subtotal">${{ number_format($subtotal) }}</td>
                                        </tr>
                                         <tr>
                                            <td>IVA</td>
                                            <td class="subtotal">${{ number_format($iva) }}</td>
                                        </tr>
                                       
                                        <tr>
                                            <td>Total</td>
                                            <td class="price-total">${{ number_format($total) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="btn-cart-totals">
                                    <!--a href="#" class="update" title="">Update Cart</a-->
                                    <a href="{{ route('finalizar-pedido')}}" class="checkout" title="">Finalizar el pedido</a>
                                </div><!-- /.btn-cart-totals -->
                            </form><!-- /form -->
                        </div><!-- /.cart-totals -->
                    </div><!-- /.col-lg-4 -->
                    @else
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="trhead">
                            <td>No hay productos</td>
                        </tr>
                    </table>
                    @endif
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-shop-cart -->

     
  
  @include('layouts.tienda.footer')  