
<select class="dropdown" id="lineas" name="lineas" style="border: 1.5px solid"> 
    <option value="" class="dropdown" selected="selected">Línea del vehículo</option> 
    @foreach($contenArray as $vhlineas)
    <option value="{{$vhlineas->tipcar_tipo}}">{{ $vhlineas->tipcar_tipo }}</option> 
    @endforeach
</select> 

<script type="text/javascript">
    jQuery("#lineas").on("change", function (event) {
        $.blockUI({ 
            message: '<img src="{{ asset('img/loading.gif') }}">', 
            css: { 
                border: "0",
                background: "transparent"
            },
            overlayCSS:  { 
                backgroundColor: "#fff", 
                opacity:         0.6, 
                cursor:          "wait" 
            }
        });
        var linea = $("#lineas option:selected").text().trim().split(' ').join('_').split('/').join('|').toLowerCase();
        window.location.href = "{{ url('llantas') }}/" + $("#marcas option:selected").text().trim() + "/" + $("#modelos option:selected").text().trim() + "/" + linea;
    });
</script>