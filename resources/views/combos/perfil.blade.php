<select class="dropdown" id="perfiles" name="perfiles" style="border: 1.5px solid; height: 42px">
    <option value="00" class="dropdown" selected="selected">Seleccione el perfil</option>
    @foreach($perfil as $valor)
    <option value="{{$valor->perfil}}">{{ $valor->perfil }}</option>
    @endforeach
</select>
<script type="text/javascript">
    jQuery("#perfiles").on("change", function(event){
        var perfil = jQuery(this).val();
        rines(perfil);
    });
</script>
