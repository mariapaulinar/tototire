<select class="dropdown" id="modelos" name="modelos" style="border: 1.5px solid">
    <option value="" class="dropdown" selected="selected">Modelo del vehiculo</option> 
    @foreach($contenArray as $vhmodelos)
    <option value="{{$vhmodelos->tipcar_ano}}">{{ $vhmodelos->tipcar_ano }}</option> 
    @endforeach
</select> 

<script type="text/javascript">
    jQuery("#modelos").on("change", function(event){
        var cod_modelo = jQuery("#modelos").val();
        jQuery.ajax({
            url: "{{ route('tienda.lineas') }}",
            type: 'get',
            dataType: 'html',
            data: {
                cod_modelo: cod_modelo,
                cod_marca: "{{ $cod_marca }}"
            },
            beforeSend: function () {
                $.blockUI({ 
                    message: '<img src="{{ asset('img/loading.gif') }}">', 
                    css: { 
                        border: "0",
                        background: "transparent"
                    },
                    overlayCSS:  { 
                        backgroundColor: "#fff", 
                        opacity:         0.6, 
                        cursor:          "wait" 
                    }
                });
            },
            success: function (data){
                jQuery('#div_lineas').html(data);
                $.unblockUI();
            }
        });
    });
</script>
