    
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="widget-ft widget-about">
                            <div class="logo logo-ft" style="margin: 0;text-align: center; border: 3px double gray;">
                                <a href="index.html" title="">
                                    <span style="font-size: 1em; font-weight: 600">
                                        <div class="widget-title">
                                            <h3 style="margin: 8px 0 0 0;">Distribuidor Autorizado</h3>
                                        </div>
                                        <img src="{{ asset('img/bridgestone-logo.png') }}" alt="" style="width: 65%; ">
                                    </span>
                                </a>
                            </div><!-- /.logo-ft -->
                            <div class="widget-content">
                                <div class="icon">
                                    <img src="{{ asset('img/images/icons/call.png') }}" alt="">
                                </div>
                                <div class="info">
                                    <p class="questions"></p>
                                    <p class="phone">Cel: 313 852 35 07 - 310 564 04 90</p>
                                    <p class="phone">Tel: 031 371 14 83 - 031 277 94 05</p>
                                    <p class="address">
                                        <strong>Vitrina Principal:</strong> Calle 8 No 23 - 07 <br/>
                                        <strong>Bodega:</strong> Carrera 23 # 7-37 <br/>
                                        Barrio Ricaurte <br />
                                        Bogotá Colombia.
                                    </p>
                                </div>
                                <div class="logo logo-ft" style="margin: 0;text-align: center;">
                                    <img src="{{ asset('img/tototire-logo.png') }}" alt="" style="width: 62%;">
                                </div>
                            </div><!-- /.widget-content -->
                            <ul class="social-list">
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-dribbble" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-google" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul><!-- /.social-list -->
                        </div><!-- /.widget-about -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                    <div class="col-lg-4 col-md-4  col-xs-6">
                        <div class="widget-ft widget-categories-ft">
                            <div class="widget-title">
                                <h3>Buscar por marcas</h3>
                            </div>
                            <ul class="cat-list-ft">
                                <li>
                                    <a href="#" title="">Bridgestone</a>
                                </li>
                                <li>
                                    <a href="#" title="">Firestone</a>
                                </li>
                                <li>
                                    <a href="#" title="">Continental</a>
                                </li>
                                <li>
                                    <a href="#" title="">Dunlop</a>
                                </li>
                                <li>
                                    <a href="#" title="">Goodyear</a>
                                </li>
                                <li>
                                    <a href="#" title="">Hankook</a>
                                </li>
                                <li>
                                    <a href="#" title="">Michelin</a>
                                </li>
                                <li>
                                    <a href="#" title="">Pirelli</a>
                                </li>
                            </ul><!-- /.cat-list-ft -->
                        </div><!-- /.widget-categories-ft -->
                    </div><!-- /.col-lg-3 col-md-6 -->
                    <div class="col-lg-4 col-md-4 col-xs-6">
                        <div class="widget-ft widget-menu">
                            <div class="widget-title">
                                <h3>Menú Principal</h3>
                            </div>
                            <ul>
                                <li>
                                    <a href="{{ route('inicio')}}" title="">Inicio</a>
                                </li>
                                <li>
                                    <a href="{{ route('listado.productos')}}" title="">Productos</a>
                                </li>
                                <li>
                                    <a href="{{ route('listado.productosPromocion')}}" title="">Promociones</a>
                                </li>
                                <li>
                                    <a href="{{ route('nosotros')}}" title="">Nosotros</a>
                                </li>
                                <li>
                                    <a href="{{ route('mispedidos')}}" title="">Mis_Pedidos</a>
                                </li>
                                <li>
                                    <a href="{{ route('contacto')}}" title="">Contactenos</a>
                                </li>
                                
                            </ul>
                        </div><!-- /.widget-menu -->
                    </div><!-- /.col-lg-2 col-md-6 -->
                    
                </div><!-- /.row -->
            </div><!-- /.container -->
        </footer><!-- /footer -->

        <section class="footer-bottom style3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="copyright">Todos los derechos reservados 2018</p>
                        <div class="btn-scroll">
                            <ul class="pay-list">
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-dribbble" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <i class="fa fa-google" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul><!-- /.pay-list -->
                            <a href="#" title="">
                                <img src="{{ asset('img/images/icons/top.png') }}" alt="">
                            </a>
                        </div><!-- /.btn-scroll -->
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.footer-bottom -->
    </div><!-- /.boxed -->

    <!-- Javascript -->


    <script type="text/javascript" src="{{ asset('js/javascript/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.circlechart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.zoom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.flexslider-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.number.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/javascript/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.mCustomScrollbar.js') }}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDm8zjn_QkHvhTETqtw7jKo0I9tke1lvyE&callback=initMap"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/gmap3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/waves.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/javascript/jquery.countdown.js') }}"></script>
    <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootstrap-rating/bootstrap-rating.min.js"></script>
    <script src="{{ config('parametros.url_template_admin') }}/js/lib/bootbox/bootbox.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/javascript/main.js') }}"></script>
    <script src="{{ asset('js/javascript/jquery.blockUI.js') }}"></script>
    <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("form#formregister").on("submit", function (e) {
                    e.preventDefault();
                    $("button.registrarse").prop("disabled", true);
                    jQuery.ajax({
                        url: "{{ route('registro-cliente') }}",
                        type: "post",
                        dataType: "json",
                        data: jQuery(this).serialize(),
                        beforeSend: function () {
                            jQuery.blockUI({ 
                                message: '', 
                                css: { 
                                    border: "0",
                                    background: "transparent"
                                },
                                overlayCSS:  { 
                                    backgroundColor: "#fff",
                                    opacity:         0.6, 
                                    cursor:          "wait" 
                                },
                                baseZ: 10000
                            });
                        },
                        success: function (data) {
                            jQuery.unblockUI();
                           if(data.ok) {
                               window.location.href = '/';
                           } else {
                               alert(data.mensaje);
                               $("button.registrarse").prop("disabled", false);
                           }
                        }
                    });
                });

                $("#marcas").on("change", function(event){
                    
                    var cod_marca = $("#marcas").val();
                    jQuery.ajax({
                    url: "{{ route('tienda.modelo') }}",
                            type: 'get',
                            dataType: 'html',
                            data: {
                                cod_marca: cod_marca,
                            },
                            beforeSend: function () {
                                $.blockUI({ 
                                    message: '<img src="{{ asset('img/loading.gif') }}">', 
                                    css: { 
                                        border: "0",
                                        background: "transparent"
                                    },
                                    overlayCSS:  { 
                                        backgroundColor: "#fff", 
                                        opacity:         0.6, 
                                        cursor:          "wait" 
                                    }
                                });
                            },
                            success: function (data) {
                                jQuery('#div_modelos').html(data);
                                $.unblockUI();
                            }

                    });
                });

            jQuery("#anchos").on("change", function(event){
                    var ancho = jQuery("#anchos").val();
                    jQuery.ajax({
                    url: "{{ route('tienda.perfil') }}",
                            type: 'get',
                            dataType: 'html',
                            data: {
                                ancho: ancho
                            },
                            beforeSend: function () {
                                $.blockUI({ 
                                    message: '<img src="{{ asset('img/loading.gif') }}">', 
                                    css: { 
                                        border: "0",
                                        background: "transparent"
                                    },
                                    overlayCSS:  { 
                                        backgroundColor: "#fff", 
                                        opacity:         0.6, 
                                        cursor:          "wait" 
                                    }
                                });
                            },
                            success: function (data){
                                jQuery('#div_perfiles').html(data);
                                $.unblockUI();
                            }
                    });
                });

               
            });

            
            function rines(perfil)
            {
                jQuery.ajax({
                    url: "{{ route('tienda.rin') }}",
                    type: 'get',
                    dataType: 'html',
                    data: {
                        perfil: perfil,
                        ancho: jQuery("#anchos").val()
                    },
                    beforeSend: function () {
                        $.blockUI({ 
                            message: '<img src="{{ asset('img/loading.gif') }}">', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff", 
                                opacity:         0.6, 
                                cursor:          "wait" 
                            }
                        });
                    },
                    success: function (data){
                        jQuery('#div_rines').html(data);
                        $.unblockUI();
                    }
                });
            }
            
            function productosMedida(select)
            {
                var rin = jQuery(select).val();
                var ancho = jQuery("#anchos").val();
                var perfil = jQuery("#perfiles").val();
                var marcas = "";
                var url = "{{ url('llantas-medida') }}/" + ancho + "-" + perfil + "R" + rin;
                jQuery(".marcas").each(function () {
                    if(jQuery(this).prop("checked")) {
                        var marca = jQuery(this).data("descripcion").trim();
                        marcas += (marcas) ? "_" + marca : marca;
                    }
                });

                if(marcas) {
                    url += "/" + marcas;
                }
                $.blockUI({ 
                    message: '<img src="{{ asset(config('parametros.url_imagen_cargando')) }}">', 
                    css: { 
                        border: "0",
                        background: "transparent"
                    },
                    overlayCSS:  { 
                        backgroundColor: "#fff", 
                        opacity:         0.6, 
                        cursor:          "wait" 
                    }
                });
                window.location.href = url;
            }
            function aumentarCantidad(b)
            {
                var valor = Number(jQuery(b).parent("div").siblings("input.cantidad").val());
                valor++;
                jQuery(b).parent("div").siblings("input.cantidad").val(valor);
                calcularTotal(b, jQuery(b).parent("div").siblings("input.cantidad"));
            }

            function disminuirCantidad(b)
            {
                var valor = Number(jQuery(b).parent("div").siblings("input.cantidad").val());
                valor--;
                jQuery(b).parent("div").siblings("input.cantidad").val((valor > 1) ? valor : 1);
                calcularTotal(b, jQuery(b).parent("div").siblings("input.cantidad"));
            }
            function calcularTotal(b, cant) {
                var precio = jQuery(b).data("valor-unitario");
                
                var total = precio * Number(jQuery(cant).val());
                jQuery(b).parents("div.producto").find("span.totalProducto").text(jQuery.number(total, '0', ',', ','));
            }

            function borrarProductoCarrito(cod_conten)
            {
                jQuery.ajax({
                    url: "{{ route('pedidos.carrito') }}",
                    type: "post",
                    data: { accion: "borrar", _token: "{{ csrf_token() }}", cod_conten: cod_conten },
                    dataType: "html",
                    async: false, 
                    beforeSend: function () {
                        $.blockUI({ 
                            message: '<img src="{{ asset(config('parametros.url_imagen_cargando')) }}">', 
                            css: { 
                                border: "0",
                                background: "transparent"
                            },
                            overlayCSS:  { 
                                backgroundColor: "#fff", 
                                opacity:         0.6, 
                                cursor:          "wait" 
                            }
                        });
                    },
                    success: function (data) {
//                        $("#carrito").html(data);
//                        $.unblockUI();
                        window.location.reload();
                    }
                });
            }
            
            function actualizarCantidadCarrito(i)
            {
                if($(i).val()) {
                    jQuery.ajax({
                        url: "{{ route('pedidos.carrito') }}",
                        type: "post",
                        data: { 
                            accion: "agregar",
                            _token: "{{ csrf_token() }}",
                            cod_prod: $(i).data("cod-prod"),
                            cantidad: $(i).val(),
                            precio_venta: $(i).data("precio-venta")
                        },
                        dataType: "html",
                        async: false,
                        beforeSend: function () {
                            $.blockUI({ 
                                message: '<img src="{{ asset(config('parametros.url_imagen_cargando')) }}">', 
                                css: { 
                                    border: "0",
                                    background: "transparent"
                                },
                                overlayCSS:  { 
                                    backgroundColor: "#fff", 
                                    opacity:         0.6, 
                                    cursor:          "wait" 
                                }
                            });
                        },
                        success: function (data) {
//                            $("#carrito").html(data);
//                            $.unblockUI();
                            window.location.reload();
                        }
                    });
                } else {
                    alert('Indique la cantidad');
                }
            }
            
            function carrito(i)
            {
                event.preventDefault();
                if($(".cantidad").val()) {
                    jQuery.ajax({
                        url: "{{ route('pedidos.carrito') }}",
                        type: "post",
                        data: {
                            accion: "agregar",
                            _token: "{{ csrf_token() }}",
                            cod_prod: $(i).data("cod"),
                            cantidad: $(i).parents(".box-buy").find("input.cantidad").val(),
                            precio_venta: $(i).data("precio-venta")
                        },
                        dataType: "html",
                        async: false,
                        beforeSend: function () {
                            $.blockUI({ 
                                message: '<img src="{{ asset(config('parametros.url_imagen_cargando')) }}">', 
                                css: { 
                                    border: "0",
                                    background: "transparent"
                                },
                                overlayCSS:  { 
                                    backgroundColor: "#fff", 
                                    opacity:         0.6, 
                                    cursor:          "wait" 
                                }
                            });
                        },
                        success: function (data) {
//                            jQuery.unblockUI();
//                            jQuery("#carrito").html(data);
                            //$('#cantidad_carrito').text($(i).parents(".box-buy").find("input.cantidad").val());

                            window.location.reload();
                        }
                    });
                } else {
                    alert('Indique la cantidad');
                }
            }
            
            
        </script>
    </body> 
</html>