<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>TOTOTIRE Leal Corporation</title>

        <meta name="author" content="CreativeLayers">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

         <!-- Boostrap style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('js/stylesheets/bootstrap.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('js/stylesheets/style.css') }}">
        
        <!--link rel="stylesheet" type="text/css" href="{{ asset('js/stylesheets/style_1.css') }}"-->

        <!-- Reponsive -->
        <link rel="stylesheet" type="text/css" href="{{ asset('js/stylesheets/responsive.css') }} ">
            
        <link href="{{ config('parametros.url_template_admin') }}/css/lib/bootstrap-rating/bootstrap-rating.css" rel="stylesheet">

        <link rel="shortcut icon" href="favicon/favicon.png">

    </head>
    <style type="text/css">
        @media screen and (max-width: 2400px){
        .img_tamaño{ max-width: 100%;}
        .pay_button{ margin: 10px 0px 0;}
        } 
        @media screen and (max-width: 1800px){
        .img_tamaño{ max-width: 100%;}
        .pay_button{ margin: 10px 0px 0;}
        } 
        @media screen and (max-width: 1200px){
        .img_tamaño{ max-width: 100%;}
        .pay_button{ margin: 10px 0px 0;}
        } 
        @media screen and (max-width: 700px){
        .img_tamaño{ max-width: 60%;}
        .pay_button{ margin: 12px 72px 0 !important;}
        } 
        
        .rating-color {
            color: #ab191c !important;
        }
        
        #container {
            margin: 20px;
            width: 400px;
            height: 8px;
        }

    </style>
    <!--Start of Tawk.to Script-->

    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5b4eaaffdf040c3e9e0bab6c/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <body class="header_sticky">
        <div class="boxed style1">
            <!-- Preloader -->
            <div class="preloader">
                <div class="clear-loading loading-effect-2">
                    <span></span>
                </div>
            </div><!-- /.preloader --> 
            
            <section id="header" class="header">
                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                             
                           
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <ul class="flat-unstyled">
                                    <li class="account" style="font-size: 18px; margin-top: 5px;">
                                        <a href="{{ (!auth()->check()) ? route('registrarse-autenticarse') : '#' }}" title="">
                                            <img src="{{ asset('img/images/icons/user.png') }}" alt="" style="width: 15%;">
                                            {{ (auth()->check()) ? auth()->user()->name : 'Ingresar a mi cuenta' }}
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>
                                        @if(auth()->check())
                                        <ul class="unstyled">
                                            @if(auth()->user()->perfil == 'cliente')
                                            <li>
                                                <a href="{{ route('micuenta')}}" title="">Mi Cuenta</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('detalle-carrito')}}" title="">Mi Carrito</a>
                                            </li>
                                            @else
                                            <li>
                                                <a href="{{ url('admin')}}" title="">Ir al administrador</a>
                                            </li>
                                            @endif
                                            <li>
                                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="logout">Cerrar sesión</a>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>
                                    <!--li>
                                        <a href="#" title="">$ Pesos<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <ul class="unstyled">
                                            <li>
                                                <a href="#" title="">Euro</a>
                                            </li>
                                            <li>
                                                <a href="#" title="">Dolar</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" title="">Español<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <ul class="unstyled">
                                            <li>
                                                <a href="#" title="">English</a>
                                            </li>
                                           
                                        </ul>
                                    </li-->
                                </ul><!-- /.flat-unstyled -->
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.header-top -->
                <div class="header-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12" >
                                <div id="logo" class="logo style1" >
                                    <a href="index.html" title="">
                                        <img src="{{ asset('img/tototire-logo1.png') }}" alt="" style="max-width: 100%">
                                    </a>
                                </div><!-- /#logo -->
                                <div class="nav-wrap">
                                    <div id="mainnav" class="mainnav style1">
                                        <ul class="menu">
                                            <li>
                                                <a href="{{ route('inicio')}}" title="">Inicio</a>
                                            </li>
                                            <li class="column-1">
                                                <a href="{{ route('listado.productos')}}" title="">Productos</a>
                                            </li>
                                            <li class="column-1">
                                                <a href="{{ route('listado.productosPromocion')}}" title="">Promociones</a>
                                            </li>
                                            <li class="column-1">
                                                <a href="{{ route('nosotros')}}" title="">Nosotros</a>
                                            </li><!-- /.column-1 -->
                                            <!--li>
                                                <a href="{{ route('mispedidos')}}" title="">Mis_Pedidos</a>
                                                <div class="submenu">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <h3 class="cat-title">Información</h3>
                                                            <ul>
                                                                <li>
                                                                    <a href="#" title="">Información Uno</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Dos</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Tres</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cuatro</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cinco</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Seis</a>
                                                                </li>
                                                                
                                                            </ul>
                                                            <div class="show">
                                                                <a href="#" title="">Comprar ahora</a>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-3">
                                                            <h3 class="cat-title">Información</h3>
                                                            <ul>
                                                                <li>
                                                                    <a href="#" title="">Información Uno</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Dos</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Tres</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cuatro</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cinco</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Seis</a>
                                                                </li>
                                                                
                                                            </ul>
                                                            <div class="show">
                                                                <a href="#" title="">Comprar ahora</a>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-3">
                                                            <h3 class="cat-title">Información</h3>
                                                            <ul>
                                                                <li>
                                                                    <a href="#" title="">Información Uno</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Dos</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Tres</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cuatro</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cinco</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Seis</a>
                                                                </li>
                                                                
                                                            </ul>
                                                            <div class="show">
                                                                <a href="#" title="">Comprar ahora</a>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-3">
                                                            <h3 class="cat-title">Información</h3>
                                                            <ul>
                                                                <li>
                                                                    <a href="#" title="">Información Uno</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Dos</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Tres</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cuatro</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Cinco</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" title="">Información Seis</a>
                                                                </li>
                                                                
                                                            </ul>
                                                            <div class="show">
                                                                <a href="#" title="">Comprar ahora</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </li -->
                                            <li class="column-1">
                                                <a href="{{ route('servicios')}}" title="">Servicios</a>
                                            </li>
                                            <li class="column-1">
                                                <a href="{{ route('contacto')}}" title="">Contactenos</a>
                                            </li>
                                        </ul><!-- /.menu -->
                                    </div><!-- /.mainnav -->
                                </div><!-- /.nav-wrap -->
                                <div class="btn-menu style1">
                                    <span></span>
                                </div><!-- //mobile menu button -->
                                <div>
                                    <span style="">
                                    <img src="{{ asset('img/bridgestone-logo.png') }}" alt="" style="width: 15%; margin: 14px 0px 0px -8px;"><br/><span>Distribuidor Autorizado</span>
                                    </span>
                                </div>
                                <!--ul class="flat-infomation style1">
                                    <li class="phone">
                                        <img src="{{ asset('img/images/icons/call-3.png') }}" alt=""> 
                                        Tel: <a href="#" title="">(+57) 313 852 35 07</a>
                                        
                                    </li>
                                    
                                </ul><!-- /.flat-infomation -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.header-middle -->
                <div class="header-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-2">
                                <div id="mega-menu">
                                    <div class="btn-mega"><span></span>CATEGORIAS </div>
                                    <ul class="menu">
                                        <li>
                                            <a href="#" title="" class="dropdown">
                                                <span class="menu-img">
                                                    <img src="{{ asset('img/images/icons/car.png')}}" alt="">
                                                </span>
                                                <span class="menu-title">
                                                    Marcas Llantas
                                                </span>
                                            </a>
                                            <div class="drop-menu">
                                                <div class="one-third">
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Bridgestone.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Firestone.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Continental.jpg') }}" alt="">
                                                    </div>
                                                    
                                                </div>
                                                <div class="one-third">
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Goodyear.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Dunlop.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Hankook.jpg') }}" alt="">
                                                    </div>
                                                   
                                                </div>
                                                <div class="one-third">
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Michelin.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/Pirelli.jpg') }}" alt="">
                                                    </div>
                                                    <div class="cat-title">
                                                        <img src="{{ asset('img/marcas/BfGoodrich.jpg') }}" alt="">
                                                    </div>
                                                   
                                                </div>
                                                
                                            </div><!-- /.drop-menu -->
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <span class="menu-img">
                                                    <img src="{{ asset('img/images/icons/car.png')}}" alt="">
                                                </span>
                                                <span class="menu-title">
                                                    Rines
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" title="">
                                                <span class="menu-img">
                                                    <img src="{{ asset('img/images/icons/car.png')}}" alt="">
                                                </span>
                                                <span class="menu-title">
                                                    Otros
                                                </span>
                                            </a>
                                        </li>
                                    </ul><!-- /.menu -->
                                </div>
                            </div><!-- /.col-md-3 col-2 -->
                           
                            <div class="col-md-9 col-10">
                                <div class="top-search style1">
                                    
                                </div><!-- /.top-search -->
                                <span class="show-search">
                                    <button></button>
                                </span><!-- /.show-search -->
                                <div class="box-cart style1">
                                    <div class="inner-box" id="carrito">
                                        @include('tienda.carrito')
                                    </div><!-- /.inner-box -->
                                </div><!-- /.box-cart -->
                            </div><!-- /.col-md-9 col-10 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.header-bottom -->
            </section><!-- /#header -->
            <br/>
            <section class="flat-imagebox">
                <div class="container" style="    border: 2px solid #ab191c;    padding: 0 17px;">
                    <div class="row" style="background-color: white;">
                        <div class="col-md-12">
                            <div class="product-tab style2" style="margin-bottom: 0px;border-bottom: 3px solid #e5e5e5;">
                                <ul class="tab-list">
                                    <li class="active" style="font-weight: bold">Llantas por vehiculo</li>
                                    <li style="font-weight: bold">Llantas por medida</li>
                                </ul>
                            </div><!-- /.product-tab -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                    <br/>
                    <div class="box-product">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="row">
                                    <div class="col-sm-5 col-lg-2 col-md-2 col-xs-5" style="padding: 11px 15px; text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black; ">Marca del vehículo</label>
                                    </div>
                                    <div class="col-sm-7 col-lg-2 col-md-2 col-xs-7" style="padding: 0px">
                                        <select class="dropdown" id="marcas" name="marcas" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">-- Seleccione --</option>
                                            @foreach($marcar as $valor)
                                            <option value="{{$valor->id}}">{{ $valor->descripcion }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-5 col-lg-2 col-md-2 col-xs-5" style="padding: 11px 15px; text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black">Modelo del vehículo</label>
                                    </div>
                                    <div id="div_modelos" class="col-sm-7 col-lg-2 col-md-2 col-xs-7" style="padding: 0px">
                                        <select class="dropdown" id="marcas" name="marcas" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">Seleccione la marca</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-5 col-lg-2 col-md-2 col-xs-5" style="padding: 11px 15px; text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black">Línea del vehículo</label>
                                    </div>
                                    <div id="div_lineas" class="col-sm-7 col-lg-2 col-md-2 col-xs-7" style="padding: 0px">
                                        <select class="dropdown" id="marcas" name="marcas" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">Seleccione el modelo</option>
                                        </select>
                                    </div>
                                    
                                </div>             
                            </div><!-- /.col-sm-6 col-lg-3 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12 col-lg-12">
                                <div class="row">
                                     <div class="col-sm-3 col-lg-2 col-md-2 col-xs-3" style="padding: 11px 33px; text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black; ">Ancho:</label>
                                    </div>
                                    <div class="col-sm-9 col-lg-2 col-md-2 col-xs-9" style="padding: 0px">
                                        <select class="dropdown" id="anchos" name="anchos" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">-- Seleccione --</option>
                                            @foreach($ancho as $valor)
                                            <option value="{{$valor->ancho}}">{{ $valor->ancho }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-3 col-lg-2 col-md-2 col-xs-3" style="padding: 11px 33px;text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black">Perfil:</label>
                                    </div>
                                    <div class="col-sm-9 col-lg-2 col-md-2 col-xs-9" id="div_perfiles" style="padding: 0px">
                                        <select class="dropdown" id="perfiles" name="perfiles" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">Seleccione el ancho</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-lg-2 col-md-2 col-xs-3" style="padding: 11px 33px;text-align: right;">
                                        <label style="font-size: 1em; font-weight: bold; color: black">Rin:</label>
                                    </div>
                                    <div class="col-sm-9 col-lg-2 col-md-2 col-xs-9" id="div_rines" style="padding: 0px">
                                        <select class="dropdown" id="rin" name="rin" style="border: 1.5px solid">
                                            <option value="00" class="dropdown" selected="selected">Seleccione el perfil</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                          
                        </div><!-- /.row -->
                        
                    </div><!-- /.box-product -->
                    <div class="col-sm-12 col-lg-12">
                        <br/>
                    </div>
                </div><!-- /.container -->
            </section><!-- /.flat-imagebox -->
            <br/>
            <!--div class="divider30"></div-->
