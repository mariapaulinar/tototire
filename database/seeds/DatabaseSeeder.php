<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        \DB::table('users')->insert([
            'email' => 'maria.paulina.r.v@gmail.com',
            'name' => 'María Ramírez',
            'password' => bcrypt('123456'),
            'perfil' => 'admin',
            'estado'  => '1',
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'fecha_modificacion' => date('Y-m-d H:i:s'),
        ]);
    }
}
