<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {  return view('tienda.index');});
Auth::routes();
Route::get('home', function () {
    return redirect()->to('/');
});
Route::get('/', 'HomeController@index')->name('inicio');
Route::get('modelo', 'HomeController@getModelos')->name('tienda.modelo');
Route::get('lineas', 'HomeController@getLineas')->name('tienda.lineas');
Route::get('perfil', 'HomeController@getPerfil')->name('tienda.perfil');
Route::get('rin', 'HomeController@getRin')->name('tienda.rin');
Route::get('llantas/{marca}/{modelo}/{linea}', 'Tienda\ProductosController@productosVehiculo')->name('listado.llantas');
Route::get('llantas-medida/{ancho}-{perfil?}R{rin}/{marcas?}', 'Tienda\ProductosController@productosMedida')->name('listado.llantas-medida');
Route::get('llantas', 'Tienda\ProductosController@productos')->name('listado.productos');
Route::get('llantasPromocion', 'Tienda\ProductosController@productosPromocion')->name('listado.productosPromocion');
Route::get('detalle-producto', 'Tienda\ProductosController@detalleproducto')->name('detalle-producto');
Route::group(['prefix' => 'contacto'], function() {
    Route::get('/', 'HomeController@contacto')->name('contacto');
    Route::post('enviar-email', 'HomeController@enviarEmailContacto')->name('contacto.enviar-email');
});
Route::get('nosotros', 'HomeController@nosotros')->name('nosotros');
Route::get('servicios', 'HomeController@servicios')->name('servicios');
Route::post('registro-cliente', 'HomeController@registro')->name('registro-cliente');
Route::post('carrito', 'Tienda\PedidosController@carrito')->name('pedidos.carrito');
Route::get('validar-disponibilidad', 'Tienda\PedidosController@validarDisponibilidad')->name('producto.validar-disponibilidad');
Route::get('detalle-carrito', 'Tienda\PedidosController@detallecarrito')->name('detalle-carrito');
Route::get('finalizar-pedido', 'Tienda\PedidosController@finalizarpedido')->name('finalizar-pedido');
Route::get('registrarse-autenticarse', 'HomeController@acceso')->name('registrarse-autenticarse');
Route::get('detalle-usuario', 'HomeController@detalleUsuario')->name('detalle-usuario');
Route::post('registrar-usuario', 'UsersController@guardarUsuarioTienda')->name('registrar-usuario');
Route::get('micuenta', 'HomeController@micuenta')->name('micuenta');
Route::post('guardar-calificacion', 'Tienda\ProductosController@guardarCalificacion')->name('guardar-calificacion');
Route::get('prueba', function () {
    return session()->get('productos');
}); 
Route::group(['middleware' => 'auth'], function () {
    Route::get('mispedidos', 'HomeController@mispedidos')->name('mispedidos');
});
