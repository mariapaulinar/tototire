<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('enviar-emails-promociones-diario', function () {
    //Obtener las promociones vigentes asociadas a esta campaña.
    $promociones_mailing = App\Mailing::where('fecha_hora_fin', '>=', date('Y-m-d H:i:s'))
            ->where('frecuencia', 'diario')
            ->pluck('promocion_id')
            ->toArray();
    //Obtener los usuarios de los clientes asociados a las promociones de esta campaña.
    $usuarios = \Db::table('promociones_clientes')
            ->join('clientes', 'clientes.id', 'promociones_clientes.cliente_id')
            ->join('users', 'users.cliente_id', 'clientes.id')
            ->whereIn('promociones_clientes.promocion_id', $promociones_mailing)
            ->get();
    //Enviar emails a los usuarios.
    foreach($usuarios as $u) {
        Mail::to($u->email)->send(new App\Mail\Promociones($promociones_mailing));
    }
    //Obtener las promociones que aplican a todos los clientes y que pertenecen a esta campaña.
    $promociones_todos = \Db::table('promociones')
            ->whereRaw('id not in (select promocion_id from promociones_clientes group by promocion_id)')
            ->whereIn('id', $promociones_mailing)
            ->pluck('id')
            ->toArray();
    //Obtener los usuarios de los clientes asociados a las promociones que aplican a todos.
    $usuarios_promo_todos = \Db::table('promociones_clientes')
            ->join('clientes', 'clientes.id', 'promociones_clientes.cliente_id')
            ->join('users', 'users.cliente_id', 'clientes.id')
            ->whereIn('promociones_clientes.promocion_id', $promociones_todos)
            ->get();
    //Enviar emails a los usuarios.
    foreach($usuarios_promo_todos as $u) {
        Mail::to($u->email)->send(new App\Mail\Promociones($promociones_todos));
    }
});
