<?php
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin.index');

    //Marcas
    Route::group(['prefix' => 'marcas'], function () {
        Route::get('/', 'Admin\MarcasController@listado')->name('admin.marcas.listado');
        Route::group(['prefix' => 'nueva'], function () {
            Route::get('/', 'Admin\MarcasController@nueva')->name('admin.marcas.nueva');
            Route::post('guardar', 'Admin\MarcasController@guardar')->name('admin.marcas.nueva.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\MarcasController@editar')->name('admin.marcas.editar');
            Route::put('actualizar', 'Admin\MarcasController@actualizar')->name('admin.marcas.editar.actualizar');
        });
        Route::group(['prefix' => 'cargue-masivo'], function () {
            Route::get('/', 'Admin\MarcasController@cargueMasivo')->name('admin.marcas.cargue-masivo');
            Route::post('cargar', 'Admin\MarcasController@cargar')->name('admin.marcas.cargue-masivo.cargar');
        });

    });

    //Categorías
    Route::group(['prefix' => 'categorias'], function () {
        Route::get('/', 'Admin\CategoriasController@listado')->name('admin.categorias.listado');
        Route::group(['prefix' => 'nueva'], function () {
            Route::get('/', 'Admin\CategoriasController@nueva')->name('admin.categorias.nueva');
            Route::post('guardar', 'Admin\CategoriasController@guardar')->name('admin.categorias.nueva.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\CategoriasController@editar')->name('admin.categorias.editar');
            Route::put('actualizar', 'Admin\CategoriasController@actualizar')->name('admin.categorias.editar.actualizar');
        });
        Route::group(['prefix' => 'cargue-masivo'], function () {
            Route::get('/', 'Admin\CategoriasController@cargueMasivo')->name('admin.categorias.cargue-masivo');
            Route::post('cargar', 'Admin\CategoriasController@cargar')->name('admin.categorias.cargue-masivo.cargar');
        });
    });

    //Productos
    Route::group(['prefix' => 'productos'], function () {
        Route::get('/', 'Admin\ProductosController@listado')->name('admin.productos.listado');
        Route::group(['prefix' => 'nuevo'], function () {
            Route::get('/', 'Admin\ProductosController@nuevo')->name('admin.productos.nuevo');
            Route::post('guardar', 'Admin\ProductosController@guardar')->name('admin.productos.nuevo.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\ProductosController@editar')->name('admin.productos.editar');
            Route::put('actualizar', 'Admin\ProductosController@actualizar')->name('admin.productos.editar.actualizar');
        });
        Route::group(['prefix' => 'cargue-masivo'], function () {
            Route::get('/', 'Admin\ProductosController@cargueMasivo')->name('admin.productos.cargue-masivo');
            Route::post('cargar', 'Admin\ProductosController@cargar')->name('admin.productos.cargue-masivo.cargar');
        });
    });

    //Clientes
    Route::group(['prefix' => 'clientes'], function () {
        Route::get('/', 'Admin\ClientesController@listado')->name('admin.clientes.listado');
        Route::group(['prefix' => 'nuevo'], function () {
            Route::get('/', 'Admin\ClientesController@nuevo')->name('admin.clientes.nuevo');
            Route::post('guardar', 'Admin\ClientesController@guardar')->name('admin.clientes.nuevo.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\ClientesController@editar')->name('admin.clientes.editar');
            Route::put('actualizar', 'Admin\ClientesController@actualizar')->name('admin.clientes.editar.actualizar');
        });
        
        Route::get('buscar', 'ClientesController@buscar');
    });

    //Usuarios
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'Admin\UsersController@listado')->name('admin.users.listado');
        Route::group(['prefix' => 'nuevo'], function () {
            Route::get('/', 'Admin\UsersController@nuevo')->name('admin.users.nuevo');
            Route::post('guardar', 'Admin\UsersController@guardar')->name('admin.users.nuevo.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\UsersController@editar')->name('admin.users.editar');
            Route::put('actualizar', 'Admin\UsersController@actualizar')->name('admin.users.editar.actualizar');
        });
    });

    //Promociones
    Route::group(['prefix' => 'promociones'], function () {
        Route::get('/', 'Admin\PromocionesController@listado')->name('admin.promociones.listado');
        Route::group(['prefix' => 'nueva'], function () {
            Route::get('/', 'Admin\PromocionesController@nueva')->name('admin.promociones.nueva');
            Route::post('guardar', 'Admin\PromocionesController@guardar')->name('admin.promociones.nueva.guardar');
        });
        Route::group(['prefix' => 'editar/{id}'], function () {
            Route::get('/', 'Admin\PromocionesController@editar')->name('admin.promociones.editar');
            Route::put('actualizar', 'Admin\PromocionesController@actualizar')->name('admin.promociones.editar.actualizar');
        });
        Route::group(['prefix' => 'cargue-masivo'], function () {
            Route::get('/', 'Admin\PromocionesController@cargueMasivo')->name('admin.promociones.cargue-masivo');
            Route::post('cargar', 'Admin\PromocionesController@cargar')->name('admin.promociones.cargue-masivo.cargar');
        });
        Route::group(['prefix' => '{id}/clientes'], function () {
            Route::get('/', 'Admin\PromocionesController@clientes')->name('admin.promociones.clientes');
            Route::post('guardar', 'Admin\PromocionesController@guardarClientes')->name('admin.promociones.clientes.guardar');
            Route::delete('eliminar', 'Admin\PromocionesController@eliminarCliente')->name('admin.promociones.clientes.eliminar');
        });
        Route::group(['prefix' => '{id}/mailing'], function () {
            Route::get('/', 'Admin\PromocionesController@mailing')->name('admin.promociones.mailing');
            Route::post('guardar', 'Admin\PromocionesController@guardarMailing')->name('admin.promociones.mailing.guardar');
            Route::delete('{mailing_id}/eliminar', 'Admin\PromocionesController@eliminarMailing')->name('admin.promociones.mailing.eliminar');
        });
    });
    
    //Reportes
    Route::group(['prefix' => 'pedidos'], function () {
        Route::get('/', 'Admin\PedidosController@index')->name('admin.pedidos.listado');
        Route::get('detalle/{}', 'Admin\PedidosController@detalle')->name('admin.pedidos.detalle');
        Route::get('nuevo', 'Admin\PedidosController@nuevo')->name('admin.pedidos.nuevo');
        Route::post('guardar', 'Admin\PedidosController@guardar')->name('admin.pedidos.guardar');
    });
});

Route::group(['prefix' => 'users/crear-contraseña'], function () {
    Route::get('/', 'Admin\UsersController@crearContrasena')->name('admin.users.crear-contrasena');
    Route::post('guardar-contrasena', 'Admin\UsersController@guardarContrasena')->name('admin.users.crear-contrasena.guardar-contrasena');
});
