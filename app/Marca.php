<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['descripcion', 'tipo_marca', 'estado'];
}
