<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['producto_id', 'nombres', 'apellidos', 'email',
        'comentario', 'rating'];
    
    protected $dates = [
        'fecha'
    ];
}
