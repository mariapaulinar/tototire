<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocion extends Model
{
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_modificacion';
    
    protected $table = 'promociones';
    
    protected $fillable = [
            'codigo',
            'producto_id',
            'descripcion',
            'descripcion_larga',
            'fecha_hora_inicio',
            'fecha_hora_fin',
            'precio_normal',
            'precio_venta',
            'cantidad_dispuesta',
            'cantidad_min',
            'cantidad_max',
        ];
    
    protected $dates = [
        'fecha_hora_inicio',
        'fecha_hora_fin',
    ];
        
    /**
     * Obtiene el modelo Producto asociado a la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Producto
    */
    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
    
    /**
     * Obtiene el modelo Cliente asociado a la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Cliente
    */
    public function clientes()
    {
        return $this->belongsToMany('App\Cliente', 'promociones_clientes', 'promocion_id', 'cliente_id');
    }
    
    /**
     * Obtiene el modelo Mailing asociado a la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Mailing
    */
    public function mailing()
    {
        return $this->hasMany('App\Mailing', 'promocion_id');
    }
}
