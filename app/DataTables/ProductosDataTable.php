<?php

namespace App\DataTables;

use App\Producto;
use Yajra\DataTables\Services\DataTable;

class ProductosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($producto) {
                        $label = ($producto->estado) ? 'Activo' : 'Inactivo';
                        $color = ($producto->estado) ? 'success' : 'danger';
                        return '<span class="label label-' . $color . '">' . $label . '</span>';
                    })
                    ->editColumn('marca_id', function ($producto) {
                        return $producto->marca->descripcion;
                    })
                    ->editColumn('precio_venta', function ($producto) {
                        return '$' . number_format($producto->precio_venta);
                    })
                    ->addColumn('rating', function ($producto) {
                        $suma_rating = $producto->comentarios()->sum('rating');
                        $count_comentarios = $producto->comentarios()->count();
                        $promedio_rating = ($count_comentarios > 0) ? $suma_rating/$count_comentarios : 0;
                        return view('admin.productos.rating', compact('promedio_rating'));
                    })
                    ->addColumn('acciones', function ($producto) {
                        return view('admin.productos.acciones', compact('producto'));
                    })
                    ->rawColumns(['estado', 'rating', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Producto $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Producto $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'plu', 'name' => 'plu', 'title' => 'Referencia'],
            ['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'],
            ['data' => 'marca_id', 'name' => 'marca_id', 'title' => 'Marca'],
            ['data' => 'precio_venta', 'name' => 'precio_venta', 'title' => 'Precio venta'],
            ['data' => 'existencia', 'name' => 'existencia', 'title' => 'Existencia'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'rating', 'name' => 'rating', 'title' => 'Calificaciones', 'orderable' => false, 'searchable'  => false, 'exportable' => false, 'printable' => false],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false, 'exportable' => false, 'printable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Productos_' . date('YmdHis');
    }
}
