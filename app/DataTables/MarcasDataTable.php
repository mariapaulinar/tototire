<?php

namespace App\DataTables;

use App\Marca;
use Yajra\DataTables\Services\DataTable;

class MarcasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($marca) {
                $label = ($marca->estado) ? 'Activa' : 'Inactiva';
                $color = ($marca->estado) ? 'success' : 'danger';
                return '<span class="label label-' . $color . '">' . $label . '</span>';
            })
            ->addColumn('acciones', function ($marca) {
                return view('admin.marcas.acciones', compact('marca'));
            })
            ->rawColumns(['estado', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Marca $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Marca $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Marcas_' . date('YmdHis');
    }
}
