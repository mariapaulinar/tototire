<?php

namespace App\DataTables;

use App\Cliente;
use Yajra\DataTables\Services\DataTable;

class ClientesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($cliente) {
                $label = ($cliente->estado) ? 'Activo' : 'Inactivo';
                $color = ($cliente->estado) ? 'success' : 'danger';
                return '<span class="label label-' . $color . '">' . $label . '</span>';
            })
            ->addColumn('acciones', function ($cliente) {
                return view('admin.clientes.acciones', compact('cliente'));
            })
            ->rawColumns(['estado', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Cliente $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Cliente $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'razon_social', 'name' => 'razon_social', 'title' => 'Razón social'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
            ['data' => 'direccion', 'name' => 'direccion', 'title' => 'Dirección'],
            ['data' => 'telefono1', 'name' => 'telefono1', 'title' => 'Teléfono'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false, 'exportable' => false, 'printable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Clientes_' . date('YmdHis');
    }
}
