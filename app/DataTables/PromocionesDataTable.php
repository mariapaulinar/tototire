<?php

namespace App\DataTables;

use App\Promocion;
use Yajra\DataTables\Services\DataTable;

class PromocionesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('producto_id', function ($promocion) {
                return $promocion->producto->descripcion;
            })
            ->editColumn('fecha_hora_inicio', function ($promocion) {
                return $promocion->fecha_hora_inicio->format('d/m/Y h:i:s a');
            })
            ->editColumn('fecha_hora_fin', function ($promocion) {
                return $promocion->fecha_hora_fin->format('d/m/Y h:i:s a');
            })
            ->editColumn('precio_normal', function ($promocion) {
                return number_format($promocion->precio_normal);
            })
            ->editColumn('precio_venta', function ($promocion) {
                return number_format($promocion->precio_venta);
            })
            ->addColumn('acciones', function ($promocion) {
                return view('admin.promociones.acciones', compact('promocion'));
            })
            ->rawColumns(['acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Promocion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Promocion $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'],
            ['data' => 'producto_id', 'name' => 'producto_id', 'title' => 'Producto'],
            ['data' => 'fecha_hora_inicio', 'name' => 'fecha_hora_inicio', 'title' => 'Desde'],
            ['data' => 'fecha_hora_fin', 'name' => 'fecha_hora_fin', 'title' => 'Hasta'],
            ['data' => 'precio_normal', 'name' => 'precio_normal', 'title' => 'Precio normal'],
            ['data' => 'precio_venta', 'name' => 'precio_venta', 'title' => 'Precio venta'],
            ['data' => 'cantidad_dispuesta', 'name' => 'cantidad_dispuesta', 'title' => 'Cant. dispuesta'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Promociones_' . date('YmdHis');
    }
}
