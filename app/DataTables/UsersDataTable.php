<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($user) {
                $label = ($user->estado) ? 'Activo' : 'Inactivo';
                $color = ($user->estado) ? 'success' : 'danger';
                return '<span class="label label-' . $color . '">' . $label . '</span>';
            })
            ->editColumn('perfil', function ($user) {
                return strtoupper($user->perfil);
            })
            ->addColumn('acciones', function ($user) {
                return view('admin.users.acciones', compact('user'));
            })
            ->rawColumns(['estado', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Nombres'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
            ['data' => 'direccion', 'name' => 'direccion', 'title' => 'Dirección'],
            ['data' => 'telefono1', 'name' => 'telefono1', 'title' => 'Teléfono'],
            ['data' => 'perfil', 'name' => 'perfil', 'title' => 'Perfil'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false, 'exportable' => false, 'printable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Usuarios_' . date('YmdHis');
    }
}
