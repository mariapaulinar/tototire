<?php

namespace App\DataTables;

use App\Categoria;
use Yajra\DataTables\Services\DataTable;

class CategoriasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($categoria) {
                $label = ($categoria->estado) ? 'Activa' : 'Inactiva';
                $color = ($categoria->estado) ? 'success' : 'danger';
                return '<span class="label label-' . $color . '">' . $label . '</span>';
            })
            ->addColumn('acciones', function ($categoria) {
                return view('admin.categorias.acciones', compact('categoria'));
            })
            ->rawColumns(['estado', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Categoria $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Categoria $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Categorias_' . date('YmdHis');
    }
}
