<?php

namespace App\DataTables;

use App\Pedido;
use Yajra\DataTables\Services\DataTable;

class PedidosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('estado', function ($pedido) {
                return strtoupper($pedido->estado);
            })
            ->editColumn('fecha_hora', function ($pedido) {
                return $pedido->fecha_hora->format('d/m/Y h:i:s a');
            })
            ->editColumn('user_id_compra', function ($pedido) {
                return $pedido->user->name;
            })
            ->addColumn('acciones', function ($pedido) {
                return view('admin.pedidos.acciones', compact('pedido'));
            })
            ->rawColumns(['estado', 'acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Pedido $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Pedido $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters([
                        'paging' => true,
                        'searching' => true,
                        'info' => true,
                        'language' => [
                            'url' => config('datatables.lang'),
                            'buttons' => [
                                'export' =>  'Exportar',
                                'print' => 'Imprimir'
                            ]
                        ],
                        'dom'          => 'Bfrtip',
                        'buttons'      => ['export', 'print'],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
            ['data' => 'fecha_hora', 'name' => 'fecha_hora', 'title' => 'Fecha y hora'],
            ['data' => 'user_id_compra', 'name' => 'user_id_compra', 'title' => 'Nombres'],
            ['data' => 'valor_total', 'name' => 'valor_total', 'title' => 'Vlr. Total'],
            ['data' => 'cantidad_total', 'name' => 'cantidad_total', 'title' => 'No. Productos'],
            ['data' => 'estado', 'name' => 'estado', 'title' => 'Estado'],
            ['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Pedidos_' . date('YmdHis');
    }
}
