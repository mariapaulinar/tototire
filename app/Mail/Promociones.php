<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Promocion;

class Promociones extends Mailable
{
    use Queueable, SerializesModels;
    
    public $promociones;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($promociones)
    {
        $this->promociones = $promociones;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $promociones = Promocion::whereIn('id', $this->promociones)->get();
        return $this->view('admin.emails.promociones')->with([
            'promociones' => $promociones,
        ]);
    }
}
