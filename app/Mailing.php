<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model
{
    protected $table = 'mailing';
    
    public $timestamps = false;
    
    protected $fillable = [
            'promocion_id',
            'descripcion',
            'fecha_hora_inicio',
            'fecha_hora_fin',
            'frecuencia',
            'plantilla_id'
        ];
    
    protected $dates = [
        'fecha_hora_inicio',
        'fecha_hora_fin',
    ];
}
