<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_modificacion';
    
    protected $fillable = [
            'plu',
            'descripcion',
            'descripcion_larga',
            'marca_id',
            'codigo_barras',
            'ancho',
            'perfil',
            'rin',
            'precio_venta',
            'existencia',
            'estado',
        ];
    
        
    /**
     * Obtiene el modelo Marca asociado al producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Marca
    */
    public function marca()
    {
        return $this->belongsTo('App\Marca');
    }
    
    /**
     * Obtiene los comentarios asociados al producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Comentario
    */
    public function comentarios()
    {
        return $this->hasMany('App\Comentario');
    }
}
