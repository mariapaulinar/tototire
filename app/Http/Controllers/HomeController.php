<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Producto;
use App\User;
use Auth;
use Mail;
use App\Mail\UsuarioCreado;
use App\Mail\EmailContacto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = Producto::paginate(20);
        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();

        return view('tienda.index')->with(compact('ancho', 'perfil', 'rin', 'productos', 'marcar', 'modelo'));
    }


    public function acceso()
    {
        if(auth()->check()) {
            return redirect()->to('/');
        }
        
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        $mensaje='';
       return view('tienda.registrarse-autenticarse')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo','mensaje'));
       
}

    public function getModelos(Request $request)
    {
      
        $cod_marca= $request->cod_marca;
        //dd($tipoMarca); 
        $contenArray = DB::table('tipcar')
              ->select(['tipcar_ano'] )
              ->where('tipcar_cod_marca', $cod_marca)
              ->groupBy('tipcar_ano')
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($contenArray);  
        return view('combos.modelo')->with(compact('contenArray', 'cod_marca'));

    }

    public function getLineas(Request $request)
    {
        $cod_modelo= $request->cod_modelo;
        $cod_marca = $request->cod_marca;
        //dd($tipoModelo); 
          $contenArray = DB::table('tipcar')
              ->select(['*'] )
              ->where('tipcar_ano', $cod_modelo)
              ->where('tipcar_cod_marca', $cod_marca)
              ->orderBy('tipcar_tipo','ASC')
              ->get();
        //dd($contenArray);  
        return view('combos.lineas')->with(compact('contenArray', 'cod_marca', 'cod_modelo'));

    }

    public function getPerfil(Request $request)
    {
        $ancho = $request->ancho;
        //dd($ancho);
        $perfil = DB::table('productos')
                ->select(['perfil'] )
                ->where('ancho','=', $ancho)
                ->groupBy('perfil')
                ->orderBy('perfil', 'asc')
                ->get();
        //var_dump($perfil);
        return view('combos.perfil')->with(compact('perfil','ancho'));

    }

    public function getRin(Request $request)
    {
        $ancho = $request->ancho;
        $perfil = $request->perfil;
        //dd($ancho);
        $rin = DB::table('productos')
                ->select(['rin'] )
                ->where('ancho','=', $ancho)
                ->where('perfil','=', $perfil)
                ->groupBy('rin')
                ->orderBy('rin', 'asc')
                ->get();


        return view('combos.rin')->with(compact('rin'));

    }
    public function contacto()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.contacto')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }
    
    public function enviarEmailContacto(Request $request)
    {
        Mail::to(config('parametros.email_contacto'))->send(new EmailContacto($request));
        
        return redirect()->back()->with(['ok' => true, 'mensaje' => 'Su mensaje ha sido enviado. Nos contactaremos con usted lo más pronto posible para atender su requerimiento.']);
    }

    public function micuenta()
    {
      
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.micuenta')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }

    public function nosotros()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.nosotros')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }


    public function servicios()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.servicios')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }

    public function mispedidos()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.mispedidos')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }

    public function detalleUsuario(Request $request)
    {
      //dd('ffffffffffff');
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        $user= $request->modelo;
        dd($user);
        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.detalle-usuario')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }


    public function registro(Request $request)
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();

        $user = DB::table('users')->select(['*'])->where('email', $request->email)->first();
        if($user)
        {
            return response()->json(['ok' => false, 'mensaje' => 'Cliente con el email registrado ya existe por favor validar la información, ó digite su usuario y contraseña']);
        }
        else
        {
            $cliente = DB::table('clientes')->select(['*'])->where('identificacion', $request->identification)->first();
            if(!$cliente)
            {
                $insert_cliente = DB::table('clientes')->insertGetId([
                    'razon_social' => $request->firtsname . ' ' . $request->lastname,
                    'tipo_ident' =>$request->typeIdent,
                    'identificacion' => $request->identification,
                    'email' => $request->email,
                    'estado' => '1',
                    'fecha_creacion' => date('Y-m-d'),
                    'fecha_modificacion' => date('Y-m-d'),
                ]);
            }
            
            $cliente = DB::table('clientes')->select(['*'])
                    ->where('tipo_ident', $request->typeIdent)
                    ->where('identificacion', $request->identification)
                    ->first();
            //Inserto el usuario en users
            if($cliente) {
                $user = new User();
                
                $user->name = $request->firtsname . ' ' . $request->lastname;
                $user->email = $request->email;
                $user->cliente_id = $cliente->id;
                $user->password = bcrypt($request->password);
                $user->perfil = 'cliente';
                $user->estado = '1';

                if($user->save()) {
                    Mail::to($user->email)->send(new UsuarioCreado($user));
                    if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                        return response()->json(['ok' => true, 'mensaje' => '']);
                    } else {
                        return response()->json(['ok' => false, 'mensaje' => 'Email o contraseña incorrectos']);
                    }
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
                }
            } else {
                return response()->json(['ok' => false, 'mensaje' => 'No existe este cliente/usuario']);
            }
        }
    }
}
