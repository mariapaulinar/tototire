<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Datatables;
use App\DataTables\PedidosDataTable;
use App\Pedido;
use App\Cliente;
use App\Producto;

class PedidosController extends Controller
{
    protected $htmlBuilder;

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }
    
    /**
     * Muestra los pedidos.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param PedidosDataTable $dataTable
     *
     * @return View
    */
    public function index(PedidosDataTable $dataTable)
    {
        return $dataTable->render('admin.pedidos.listado');
    }
    
    /**
     * Muestra los pedidos.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id ID del pedido
     *
     * @return View
    */
    public function detalle($id)
    {
        $pedido = Pedido::find($id);
        
        if(!$pedido) {
            return redirect()->back()->with(['ok' => false, 'mensaje' => 'Pedido no encontrado']);
        }
        
        $productos = $pedido->productos;
        $user = $pedido->user;
        
        return view('pedidos.detalle', compact('pedido', 'productos', 'user'));
    }
    
    /**
     * Muestra los pedidos.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function nuevo(Request $request)
    {
        if ($request->ajax()) {
            $productos = Producto::where('estado', 1);
            return Datatables::of($productos)
                    ->editColumn('precio_venta', function ($productos) {
                        return '$' . number_format($productos->precio_venta);
                    })
                    ->addColumn('cantidad', function ($productos) {
                        return '<input type="number" min="1" class="cantidad form-control col-12" value="1" data-precio-venta="' . $productos->precio_venta . '" onkeyup="calcularTotal(this)" onchange="calcularTotal(this)" onmouseup="calcularTotal(this)">';
                    })
                    ->addColumn('total', function ($productos) {
                        return '<span class="total">$' . number_format($productos->precio_venta) . '</span>';
                    })
                    ->addColumn('agregar', function ($productos) {
                        return '<button class="btn btn-success" onclick="event.preventDefault(); agregar(\'' . $productos->id . '\', \''  . $productos->plu . '\', \'' . $productos->descripcion . '\', \''. $productos->precio_venta . '\', this);">Agregar</button>';
                    })
                    ->rawColumns(['cantidad', 'total', 'agregar'])
                    ->make(true);
        }

        $datatable = $this->htmlBuilder
//            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Id'])
            ->addColumn(['data' => 'plu', 'name' => 'plu', 'title' => 'Ref.'])
            ->addColumn(['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'])
            ->addColumn(['data' => 'precio_venta', 'name' => 'precio_venta', 'title' => 'Precio'])
            ->addColumn(['data' => 'cantidad', 'name' => 'cantidad', 'title' => 'Cant.', 'orderable' => false, 'searchable'  => false])
            ->addColumn(['data' => 'total', 'name' => 'total', 'title' => 'Total', 'orderable' => false, 'searchable'  => false])
            ->addColumn(['data' => 'agregar', 'name' => 'agregar', 'title' => 'Agregar al pedido', 'orderable' => false, 'searchable'  => false])
            ->parameters([
                'paging' => true,
                'searching' => true,
                'info' => true,
                'order' => [[1, 'asc']],
                'language' => [
                    'url' => config('datatables.lang')
                ],
            ]);
        $clientes = Cliente::where('estado', 1)->get();
        return view('admin.pedidos.nuevo', compact('datatable', 'clientes'));
    }
    
    /**
     * Guarda un pedido nuevo.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function guardar(Request $request)
    {
//        dd($request->all());
        $pedido = new Pedido();
    }
}
