<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\ProductosDataTable;
use App\Producto;
use App\Marca;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class ProductosController extends Controller
{
    
    /**
     * Muestra el listado de productos.
     *
     * Obtiene todas los productos y las muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param ProductosDataTable $dataTable
     *
     * @return View
    */
    public function listado(ProductosDataTable $dataTable)
    {
        $marcas = Marca::all();
        return $dataTable->render('admin.productos.listado', compact('marcas'));
    }
    
    /**
     * Muestra la vista para crear un producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nuevo(Request $request)
    {
        $marcas = Marca::all();
        return view('admin.productos.nuevo', compact('marcas'));
    }
    
    /**
     * Guarda un producto nuevo.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $producto = new Producto();
        
        $producto->plu = $request->plu;
        $producto->descripcion = $request->descripcion;
        $producto->descripcion_larga = $request->descripcion_larga;
        $producto->marca_id = $request->marca_id;
        $producto->codigo_barras = $request->codigo_barras;
        $producto->ancho = $request->ancho;
        $producto->perfil = $request->perfil;
        $producto->rin = $request->rin;
        $producto->precio_venta = $request->precio_venta;
        $producto->existencia = $request->existencia;
        $producto->estado = $request->estado;
        
        if($producto->save()) {
            if ($request->hasFile('imagen')) {
                $archivo = $request->file('imagen');
                $producto = $this->guardarImagen($archivo, $producto);
            }
            $response = ['ok' => true, 'mensaje' => 'Producto guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar un producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id
     *
     * @return View
    */
    public function editar($id)
    {
        $producto = Producto::find($id);
        $marcas = Marca::all();
        $comentarios = $producto->comentarios()->paginate(5);
        return view('admin.productos.editar', compact('producto', 'marcas', 'comentarios'));
    }
    
    /**
     * Actualiza los datos de un producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $producto = Producto::find($id);
        if(!$producto) {
            $response = ['ok' => false, 'mensaje' => 'Producto no encontrado.'];
            return redirect()->back()->with($response);
        }
        
        $producto->plu = $request->plu;
        $producto->descripcion = $request->descripcion;
        $producto->descripcion_larga = $request->descripcion_larga;
        $producto->marca_id = $request->marca_id;
        $producto->codigo_barras = $request->codigo_barras;
        $producto->ancho = $request->ancho;
        $producto->perfil = $request->perfil;
        $producto->rin = $request->rin;
        $producto->precio_venta = $request->precio_venta;
        $producto->existencia = $request->existencia;
        $producto->estado = $request->estado;
        
        if($producto->save()) {
            if ($request->hasFile('imagen')) {
                $archivo = $request->file('imagen');
                $producto = $this->guardarImagen($archivo, $producto);
            }
            $response = ['ok' => true, 'mensaje' => 'Producto guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para cargue masivo de excel de productos.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function cargueMasivo()
    {
        return view('admin.productos.cargue-masivo');
    }
    
    /**
     * Carga el archivo de productos y los guarda.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function cargar(Request $request)
    {
        $archivo = $request->archivo;
//        dd($archivo);
        if($archivo) {
            try {
                $reader = IOFactory::createReader(ucfirst($archivo->extension()));
//                $reader->setLoadAllSheets();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($archivo->path());
                $worksheet = $spreadsheet->getActiveSheet();
                $highestRow = $worksheet->getHighestRow();
                $data = $spreadsheet->getActiveSheet()
                    ->rangeToArray(
                        'A1:K' . $highestRow,
                        null,
                        true,
                        true,
                        true
                    );
//                dd($data);
                $array_productos = [];
                foreach($data as $i => $d) {
                    if($i > 1 && $d['A'] && !empty($d['A'])) {
                        $plu = trim($d['A']);
                        $descripcion = trim($d['B']);
                        $descripcion_larga = trim($d['C']);
                        
                        $marca = trim($d['D']);
                        $modelo_marca = Marca::where('descripcion', $marca)->first();
                        if($modelo_marca) {
                            $marca_id = $modelo_marca->id;
                        } else {
                            $marca = new Marca();
                            $marca->descripcion = trim($d['D']);
                            $marca->tipo_marca = 'llanta';
                            $marca->estado = 1;
                            $marca->save();
                            
                            $marca_id = $marca->id;
                        }
                        
                        $codigo_barras = trim($d['E']);
                        $ancho = trim($d['F']);
                        $perfil = trim($d['G']);
                        $rin = trim($d['H']);
                        $precio_venta = trim($d['I']);
                        $existencia = trim($d['J']);
                        
                        $array_productos = [
                            'plu' => $plu,
                            'descripcion' => $descripcion,
                            'descripcion_larga' => $descripcion_larga,
                            'marca_id' => $marca_id,
                            'codigo_barras' => $codigo_barras,
                            'ancho' => (int)$ancho,
                            'perfil' => (int)$perfil,
                            'rin' => (float)$rin,
                            'precio_venta' => (int)$precio_venta,
                            'existencia' => (int)$existencia,
                            'estado' => 1,
                        ];
//                        dd($array_productos);
                        $producto = Producto::updateOrCreate(
                            ['plu' => $plu],
                            $array_productos
                        );
                    }
                }
                if($producto) {
                    $response = ['ok' => true, 'mensaje' => 'Productos cargados con éxito'];
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentaron errores en la carga'];
                }
            } catch(Exception $e) {
                $response = ['ok' => false, 'mensaje' => $e->getMessage()];
            }
        } else {
            $response = ['ok' => false, 'mensaje' => 'Seleccione un archivo para cargar los productos'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Guarda la imagen de la producto.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $archivo
     * @param Producto $producto
     *
     * @return View
    */
    public function guardarImagen($archivo, $producto)
    {
        $descripcion = str_slug($producto->descripcion, '-');
        $imagen = $archivo->storeAs(
                    'productos', $descripcion . '.' . $archivo->extension(), 'public'
                );
        if($imagen) {
            $producto->foto_principal = $descripcion . '.' . $archivo->extension();
            $producto->save();
        }
        
        return $producto;
    }
}
