<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\ClientesDataTable;
use App\Cliente;

class ClientesController extends Controller
{
    
    /**
     * Muestra el listado de clientes.
     *
     * Obtiene todos los clientes y los muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param ClientesDataTable $dataTable
     *
     * @return View
    */
    public function listado(ClientesDataTable $dataTable)
    {
        return $dataTable->render('admin.clientes.listado');
    }
    
    /**
     * Muestra la vista para crear un cliente.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nuevo(Request $request)
    {
        return view('admin.clientes.nuevo');
    }
    
    /**
     * Guarda un cliente nuevo.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $cliente = new Cliente();
        
        $cliente->razon_social = $request->razon_social;
        $cliente->email = $request->email;
        $cliente->direccion = $request->direccion;
        $cliente->tipo_ident = $request->tipo_ident;
        $cliente->identificacion = $request->identificacion;
        $cliente->digito_verif = $request->digito_verif;
        $cliente->fecha_nacimiento = $request->fecha_nacimiento;
        $cliente->sexo = $request->sexo;
        $cliente->telefono1 = $request->telefono1;
        $cliente->telefono2 = $request->telefono2;
        $cliente->estado = $request->estado;
        
        if($cliente->save()) {
            $response = ['ok' => true, 'mensaje' => 'Cliente guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar un cliente.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function editar($id)
    {
        $cliente = Cliente::find($id);
        return view('admin.clientes.editar', compact('cliente'));
    }
    
    /**
     * Actualiza los datos de un cliente.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        if(!$cliente) {
            $response = ['ok' => false, 'mensaje' => 'Cliente no encontrado'];
            return redirect()->back()->with($response);
        }
        
        $cliente->razon_social = $request->razon_social;
        $cliente->email = $request->email;
        $cliente->direccion = $request->direccion;
        $cliente->tipo_ident = $request->tipo_ident;
        $cliente->identificacion = $request->identificacion;
        $cliente->digito_verif = $request->digito_verif;
        $cliente->fecha_nacimiento = $request->fecha_nacimiento;
        $cliente->sexo = $request->sexo;
        $cliente->telefono1 = $request->telefono1;
        $cliente->telefono2 = $request->telefono2;
        $cliente->estado = $request->estado;
        
        if($cliente->save()) {
            $response = ['ok' => true, 'mensaje' => 'Cliente guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    /**
     * Busca un cliente.
     * 
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param string $tipo_identificacion Tipo de identificación
     * @param string $identificacion Identificación
     * 
     * @return Tipo Descripción
    */
    public function buscar($tipo_identificacion, $identificacion)
    {
        $cliente = Cliente::where('tipo_ident', $tipo_identificacion)->where('identificacion', $identificacion)->first();
    }
}
