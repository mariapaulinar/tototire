<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\UsersDataTable;
use App\User;
use App\Cliente;
use App\Mail\UsuarioCreado;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;

class UsersController extends Controller
{
    
    /**
     * Muestra el listado de usuarios.
     *
     * Obtiene todos los usuarios y los muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param UsersDataTable $dataTable
     *
     * @return View
    */
    public function listado(UsersDataTable $dataTable)
    {   
        return $dataTable->render('admin.users.listado');
    }
    
    /**
     * Muestra la vista para crear un usuario.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nuevo(Request $request)
    {
        $clientes = Cliente::all();
        return view('admin.users.nuevo', compact('clientes'));
    }
    
    /**
     * Guarda un usuario nuevo.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $user = new User();
        $user_existente = User::where('email', $request->email)->first();
        
        if($user_existente) {
            $response = ['ok' => false, 'mensaje' => 'No se puede crear el usuario ya que existe otro usuario registrado con este correo electrónico'];
            return redirect()->back()->with($response);
        }
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->direccion = $request->direccion;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        $user->cliente_id = ($request->perfil == 'cliente') ? $request->cliente_id : null;
        $user->sexo = $request->sexo;
        $user->telefono1 = $request->telefono1;
        $user->telefono2 = $request->telefono2;
        $user->perfil = $request->perfil;
        $user->estado = $request->estado;
        
        if($user->save()) {
            //Enviar email de creación.
            $user->remember_token = Hash::make(str_random(8));
            $user->save();
            if($user->remember_token) {
                Mail::to($user->email)->send(new UsuarioCreado($user));
            }
            $response = ['ok' => true, 'mensaje' => 'Usuario guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar un user.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function editar($id)
    {
        $user = User::find($id);
        $clientes = Cliente::all();
        return view('admin.users.editar', compact('user', 'clientes'));
    }
    
    /**
     * Actualiza los datos de un usuario.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $user = User::find($id);
        if(!$user) {
            $response = ['ok' => false, 'mensaje' => 'Usuario no encontrado'];
            return redirect()->back()->with($response);
        }
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->direccion = $request->direccion;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        $user->cliente_id = ($request->perfil == 'cliente') ? $request->cliente_id : null;
        $user->sexo = $request->sexo;
        $user->telefono1 = $request->telefono1;
        $user->telefono2 = $request->telefono2;
        $user->perfil = $request->perfil;
        $user->estado = $request->estado;
        
        if($user->save()) {
            $response = ['ok' => true, 'mensaje' => 'Usuario guardado con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra formulario para crear contraseña.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function crearContrasena(Request $request)
    {
        $token = $request->token;
        return view('admin.users.crear-contrasena', compact('token'));
    }
    
    /**
     * Guarda los datos enviados desde form de creación de contraseña.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardarContrasena(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed|min:6',
        ], [
            'required' => 'Este campo es requerido.',
            'confirmed' => 'Las contraseñas no coinciden.',
            'min' => 'La contraseña debe contener mínimo 6 caracteres.',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user = User::where('email', $request->email)->first();
        
        if(!$user) {
            return redirect()->back()
                        ->with(['ok' => false, 'mensaje' => 'Este usuario no se encuentra registrado']);
        }
        
        if($user->remember_token != $request->token) {
            return redirect()->back()
                        ->with(['ok' => false, 'mensaje' => 'El token no es correcto']);
        }
        
        $user->password = bcrypt($request->password);
        $user->remember_token = null;
        if($user->save()) {
            return redirect()->route('admin.index');
        } else {
            return redirect()->back()
                        ->with(['ok' => false, 'mensaje' => 'Hubo problemas al guardar la información. Intente nuevamente']);
        }
        
    }
}
