<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Datatables;
use App\Promocion;
use App\Producto;
use App\Cliente;
use App\User;
use App\Mailing;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use App\DataTables\PromocionesDataTable;

class PromocionesController extends Controller
{
    
    /**
     * Muestra el listado de promociones.
     *
     * Obtiene todas las promociones y las muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function listado(PromocionesDataTable $dataTable)
    {
        return $dataTable->render('admin.promociones.listado');
        
    }
    
    /**
     * Muestra la vista para crear una promocion.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nueva(Request $request)
    {
        $productos = Producto::where('estado', '1')->get();
        return view('admin.promociones.nueva', compact('productos'));
    }
    
    /**
     * Guarda una promocion nueva.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $producto = Producto::find($request->producto_id);

        $promocion = new Promocion();
        $promocion->codigo = $request->codigo;
        $promocion->producto_id = $request->producto_id;
        $promocion->descripcion = $request->descripcion;
        $promocion->descripcion_larga = $request->descripcion;
        $promocion->fecha_hora_inicio = $request->fecha_hora_inicio;
        $promocion->fecha_hora_fin = $request->fecha_hora_fin;
        $promocion->precio_normal = ($producto) ? $producto->precio_venta : 0;
        $promocion->precio_venta = $request->precio_venta;
        $promocion->cantidad_dispuesta = $request->cantidad_dispuesta;
        $promocion->cantidad_min = $request->cantidad_min;
        $promocion->cantidad_max = $request->cantidad_max;
        if($promocion->save()) {
            $response = ['ok' => true, 'mensaje' => 'Promoción guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar una promocion.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function editar($id)
    {
        $promocion = Promocion::find($id);
        $productos = Producto::where('estado', '1')->get();
        return view('admin.promociones.editar', compact('promocion', 'productos'));
    }
    
    /**
     * Actualiza los datos de una promocion.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $producto = Producto::find($request->producto_id);

        $promocion = Promocion::find($id);
        if(!$promocion) {
            $response = ['ok' => false, 'mensaje' => 'La promoción no existe'];
            return redirect()->back()->with($response);
        }
        $promocion->codigo = $request->codigo;
        $promocion->producto_id = $request->producto_id;
        $promocion->descripcion = $request->descripcion;
        $promocion->descripcion_larga = $request->descripcion;
        $promocion->fecha_hora_inicio = $request->fecha_hora_inicio;
        $promocion->fecha_hora_fin = $request->fecha_hora_fin;
        $promocion->precio_normal = ($producto) ? $producto->precio_venta : 0;
        $promocion->precio_venta = $request->precio_venta;
        $promocion->cantidad_dispuesta = $request->cantidad_dispuesta;
        $promocion->cantidad_min = $request->cantidad_min;
        $promocion->cantidad_max = $request->cantidad_max;
        if($promocion->save()) {
            $response = ['ok' => true, 'mensaje' => 'Promoción guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para cargue masivo de excel de promociones.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function cargueMasivo()
    {
        return view('admin.promociones.cargue-masivo');
    }
    
    /**
     * Carga el archivo de promociones y las guarda.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function cargar(Request $request)
    {
        $archivo = $request->archivo;
//        dd($archivo);
        if($archivo) {
            try {
                $reader = IOFactory::createReader(ucfirst($archivo->extension()));
//                $reader->setLoadAllSheets();
                $spreadsheet = $reader->load($archivo->path());
                $worksheet = $spreadsheet->getActiveSheet();
                $highestRow = $worksheet->getHighestRow();
                $data = $spreadsheet->getActiveSheet()
                    ->rangeToArray(
                        'A1:H' . $highestRow,
                        null,
                        true,
                        true,
                        true
                    );
//                dd($data);
                $array_promociones = [];
                foreach($data as $i => $d) {
                    if($i > 1 && $d['A']) {
                        $plu = $d['A'];
                        $descripcion = $d['B'];
                        $codigo = $d['C'];
                        $descripcion_larga = $d['D'];
                        $precio_venta = $d['E'];
                        $cantidad_minima = $d['F'];
                        $cantidad_maxima = $d['G'];
                        $cantidad_dispuesta = $d['H'];
                        $producto = Producto::where('plu', $plu)->first();
                        if($producto) {
                            $array_promociones = [
                                'producto_id' => $producto->id,
                                'descripcion' => $descripcion,
                                'codigo' => $codigo,
                                'descripcion_larga' => $descripcion_larga,
                                'fecha_hora_inicio' => $request->fecha_hora_inicio,
                                'fecha_hora_fin' => $request->fecha_hora_fin,
                                'precio_normal' => $producto->precio_venta,
                                'precio_venta' => $precio_venta,
                                'cantidad_min' => $cantidad_minima,
                                'cantidad_max' => $cantidad_maxima,
                                'cantidad_dispuesta' => $cantidad_dispuesta,
                            ];
                            $promocion = Promocion::updateOrCreate(
                                ['descripcion' => $descripcion],
                                $array_promociones
                            );
                        }
                    }
                }
                if($promocion) {
                    $response = ['ok' => true, 'mensaje' => 'Promociones cargadas con éxito'];
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentaron errores en la carga'];
                }
            } catch(Exception $e) {
                $response = ['ok' => false, 'mensaje' => $e->getMessage()];
            }
        } else {
            $response = ['ok' => false, 'mensaje' => 'Seleccione un archivo para cargar las promociones'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para asignar clientes a la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function clientes($id)
    {
        $promocion = Promocion::find($id);
        switch(request()->registrados) {
            case 'no-realizado-compras':
                $clientes_filtro = User::join('pedidos', 'users.id', '<>', 'pedidos.user_id_compra')
                                ->where('users.perfil', 'cliente')
                                ->groupBy('users.cliente_id')
                                ->pluck('users.cliente_id')
                                ->toArray();
                break;
            case 'nunca-visitado-tienda':
                $clientes_filtro = User::join('log_accesos', 'users.id', '<>', 'log_accesos.user_id')
                                ->where('users.perfil', 'cliente')
                                ->groupBy('users.cliente_id')
                                ->pluck('users.cliente_id')
                                ->toArray();
                break;
            case 'no-visitan-tienda-mas-3-meses':
                $clientes_filtro = User::join('log_accesos', 'users.id', 'log_accesos.user_id')
                                ->where('users.perfil', 'cliente')
                                ->whereRaw('datediff((select max(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id), (select min(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id)) > 90')
                                ->pluck('users.cliente_id')
                                ->toArray();
                break;
            case 'no-visitan-tienda-mas-6-meses':
                $clientes_filtro = User::join('log_accesos', 'users.id', 'log_accesos.user_id')
                                ->where('users.perfil', 'cliente')
                                ->groupBy('users.cliente_id')
                                ->whereRaw('datediff((select max(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id), (select min(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id)) > 180')
                                ->pluck('users.cliente_id')
                                ->toArray();
                break;
            case 'no-visitan-tienda-mas-12-meses':
                $clientes_filtro = User::join('log_accesos', 'users.id', 'log_accesos.user_id')
                                ->where('users.perfil', 'cliente')
                                ->groupBy('users.cliente_id')
                                ->whereRaw('datediff((select max(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id), (select min(la.fecha_hora) from log_accesos la where la.user_id = log_accesos.user_id)) > 365')
                                ->pluck('users.cliente_id')
                                ->toArray();
                break;
            default:
                $clientes_filtro = [];
        }
        
        $clientes_promocion = (count($clientes_filtro) > 0) 
                ? $promocion->clientes()->whereIn('id', $clientes_filtro)
                : $promocion->clientes;
        
        $clientes = Cliente::where('estado', '1')
                ->where(function ($query) use ($clientes_promocion) {
                    if($clientes_promocion) {
                        $query->whereNotIn('id', $clientes_promocion->pluck('id')->toArray());
                    }
                })
                ->get();
        return view('admin.promociones.clientes', compact('promocion', 'clientes', 'clientes_promocion'));
    }
    
    /**
     * Guarda clientes a la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function guardarClientes($id)
    {
        $clientes = request()->cliente_id;
        if($clientes) {
            $promocion = Promocion::find($id);
            $promocion->clientes()->syncWithoutDetaching($clientes);
            $response = ['ok' => true, 'mensaje' => 'Clientes aplicados a la promoción'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'No seleccionó clientes para aplicar a la promoción'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Elimina cliente de la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function eliminarCliente($id)
    {
        $cliente_id = request()->cliente_id;
        if($cliente_id) {
            $promocion = Promocion::find($id);
            $promocion->clientes()->detach($cliente_id);
            $cliente = Cliente::find($cliente_id);
            
            if($cliente) {
                $response = [
                        'ok' => true, 
                        'mensaje' => 'Cliente ' 
                        . $cliente->identificacion . ' - ' . $cliente->razon_social 
                        . ' eliminado de la promoción'
                    ];
            } else {
                $response = ['ok' => true, 'mensaje' => 'Cliente eliminado de la promoción'];
            }
        } else {
            $response = ['ok' => false, 'mensaje' => 'No seleccionó cliente para eliminar de la promoción'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra form para crear una campaña de mailing de la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     * @param Request $request Petición
     *
     * @return View
    */
    public function mailing($id, Request $request)
    {
        $promocion = Promocion::find($id);
        
        if(!$promocion) {
            $response = ['ok' => false, 'mensaje' => 'La promoción seleccionada no existe'];
            return redirect()->back()->with($response);
        }
        
        if ($request->ajax()) {
            $mailing = $promocion->mailing;
            return Datatables::of($mailing)
                    ->editColumn('frecuencia', function ($mailing) {
                        return strtoupper($mailing->frecuencia);
                    })
                    ->addColumn('acciones', function ($mailing) {
                        return view('admin.promociones.acciones-mailing', compact('mailing'));
                    })
                    ->rawColumns(['acciones'])
                    ->make(true);
        }

        $datatable_mailing = $this->htmlBuilder
            ->addColumn(['data' => 'descripcion', 'name' => 'descripcion', 'title' => 'Descripción'])
            ->addColumn(['data' => 'fecha_hora_inicio', 'name' => 'fecha_hora_inicio', 'title' => 'Desde'])
            ->addColumn(['data' => 'fecha_hora_fin', 'name' => 'fecha_hora_fin', 'title' => 'Hasta'])
            ->addColumn(['data' => 'frecuencia', 'name' => 'frecuencia', 'title' => 'Frecuencia'])
            ->addColumn(['data' => 'acciones', 'name' => 'acciones', 'title' => 'Acciones', 'orderable' => false, 'searchable'  => false])
            ->parameters([
                'paging' => true,
                'searching' => true,
                'info' => true,
                'language' => [
                    'url' => config('datatables.lang')
                ],
            ]);
        
        return view('admin.promociones.mailing', compact('promocion', 'datatable_mailing'));
    }
    
    /**
     * Guarda una campaña de mailing de la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id Id de la promoción
     *
     * @return View
    */
    public function guardarMailing($id)
    {
        $promocion = Promocion::find($id);
        
        if(!$promocion) {
            $response = ['ok' => false, 'mensaje' => 'La promoción seleccionada no existe'];
            return redirect()->back()->with($response);
        }
        
        $mailing = Mailing::create([
                'promocion_id' => $promocion->id,
                'descripcion' => $promocion->descripcion,
                'fecha_hora_inicio' => request()->fecha_hora_inicio,
                'fecha_hora_fin' => request()->fecha_hora_fin,
                'frecuencia' => request()->frecuencia,
            ]);
        
        if($mailing) {
            $response = ['ok' => true, 'mensaje' => 'La campaña de mailing se creo con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Elimina una campaña de mailing de la promoción.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $promocion_id Id de la promoción
     * @param int $mailing_id Id de la campaña de mailing
     *
     * @return View
    */
    public function eliminarMailing($promocion_id, $mailing_id)
    {
        $mailing = Mailing::find($mailing_id);
        
        if(!$mailing) {
            $response = ['ok' => false, 'mensaje' => 'La campaña de mailing seleccionada no existe'];
            return redirect()->back()->with($response);
        }
        
        $mailing->delete();
        
        $response = ['ok' => true, 'mensaje' => 'La campaña de mailing se eliminó con éxito'];
        return redirect()->back()->with($response);
    }
}
