<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Marca;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use App\DataTables\MarcasDataTable;

class MarcasController extends Controller
{   
    /**
     * Muestra el listado de marcas.
     *
     * Obtiene todas las marcas y las muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param MarcasDataTable $dataTable
     *
     * @return View
    */
    public function listado(MarcasDataTable $dataTable)
    {
        return $dataTable->render('admin.marcas.listado');
    }
    
    /**
     * Muestra la vista para crear una marca.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nueva(Request $request)
    {
        return view('admin.marcas.nueva');
    }
    
    /**
     * Guarda una marca nueva.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $marca = new Marca();
        
        $marca->descripcion = $request->descripcion;
        $marca->tipo_marca = $request->tipo_marca;
        $marca->estado = $request->estado;
        
        if($marca->save()) {
            if ($request->hasFile('imagen')) {
                $archivo = $request->file('imagen');
                $marca = $this->guardarImagen($archivo, $marca);
            }
            $response = ['ok' => true, 'mensaje' => 'Marca guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar una marca.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param int $id
     *
     * @return View
    */
    public function editar($id)
    {
        $marca = Marca::find($id);
        return view('admin.marcas.editar', compact('marca'));
    }
    
    /**
     * Actualiza los datos de una marca.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $marca = Marca::find($id);
        if(!$marca) {
            $response = ['ok' => false, 'mensaje' => 'Marca no encontrada.'];
            return redirect()->back()->with($response);
        }
        
        $marca->descripcion = $request->descripcion;
        $marca->tipo_marca = $request->tipo_marca;
        $marca->estado = $request->estado;
        
        if($marca->save()) {
            if ($request->hasFile('imagen')) {
                $archivo = $request->file('imagen');
                $marca = $this->guardarImagen($archivo, $marca);
            }
            $response = ['ok' => true, 'mensaje' => 'Marca guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para cargue masivo de excel de marcas.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function cargueMasivo()
    {
        return view('admin.marcas.cargue-masivo');
    }
    
    /**
     * Carga el archivo de marcas y las guarda.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function cargar(Request $request)
    {
        $archivo = $request->archivo;
//        dd($archivo);
        if($archivo) {
            try {
                $reader = IOFactory::createReader(ucfirst($archivo->extension()));
//                $reader->setLoadAllSheets();
                $spreadsheet = $reader->load($archivo->path());
                $worksheet = $spreadsheet->getActiveSheet();
                $highestRow = $worksheet->getHighestRow();
                $data = $spreadsheet->getActiveSheet()
                    ->rangeToArray(
                        'A1:C' . $highestRow,
                        null,
                        true,
                        true,
                        true
                    );
//                dd($data);
                $array_marcas = [];
                foreach($data as $i => $d) {
                    if($i > 1 && $d['A']) {
                        $descripcion = $d['A'];
                        $tipo = $d['B'];
                        $estado = $d['C'];
                        
                        $array_marcas = [
                            'descripcion' => $descripcion,
                            'tipo' => $tipo,
                            'estado' => $estado,
                        ];
                        $marca = Marca::updateOrCreate(
                            ['descripcion' => $descripcion],
                            $array_marcas
                        );
                    }
                }
                if($marca) {
                    $response = ['ok' => true, 'mensaje' => 'Marcas cargadas con éxito'];
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentaron errores en la carga'];
                }
            } catch(Exception $e) {
                $response = ['ok' => false, 'mensaje' => $e->getMessage()];
            }
        } else {
            $response = ['ok' => false, 'mensaje' => 'Seleccione un archivo para cargar las marcas'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Guarda la imagen de la marca.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $archivo
     * @param Marca $marca
     *
     * @return View
    */
    public function guardarImagen($archivo, $marca)
    {
        $descripcion = str_slug($marca->descripcion, '-');
        $imagen = $archivo->storeAs(
                    'marcas', $descripcion . '.' . $archivo->extension(), 'public'
                );
        if($imagen) {
            $marca->imagen = $descripcion . '.' . $archivo->extension();
            $marca->save();
        }
        
        return $marca;
    }
}
