<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\CategoriasDataTable;
use App\Categoria;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class CategoriasController extends Controller
{
    
    /**
     * Muestra el listado de categorias.
     *
     * Obtiene todas las categorias y las muestra en un datatables.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param CategoriasDataTable $dataTable
     *
     * @return View
    */
    public function listado(CategoriasDataTable $dataTable)
    {
        return $dataTable->render('admin.categorias.listado');
    }
    
    /**
     * Muestra la vista para crear una categoria.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function nueva(Request $request)
    {
        return view('admin.categorias.nueva');
    }
    
    /**
     * Guarda una categoria nueva.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function guardar(Request $request)
    {
        $categoria = new Categoria();
        
        $categoria->descripcion = $request->descripcion;
        $categoria->estado = $request->estado;
        
        if($categoria->save()) {
            $response = ['ok' => true, 'mensaje' => 'Categoría guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para editar una categoria.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function editar($id)
    {
        $categoria = Categoria::find($id);
        return view('admin.categorias.editar', compact('categoria'));
    }
    
    /**
     * Actualiza los datos de una categoria.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     * @param int $id
     *
     * @return View
    */
    public function actualizar(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        if(!$categoria) {
            $response = ['ok' => false, 'mensaje' => 'Categoria no encontrada'];
            return redirect()->back()->with($response);
        }
        
        $categoria->descripcion = $request->descripcion;
        $categoria->estado = $request->estado;
        
        if($categoria->save()) {
            $response = ['ok' => true, 'mensaje' => 'Categoría guardada con éxito'];
        } else {
            $response = ['ok' => false, 'mensaje' => 'Se presentó un error al guardar, intente nuevamente'];
        }
        
        return redirect()->back()->with($response);
    }
    
    /**
     * Muestra la vista para cargue masivo de excel de categorias.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return View
    */
    public function cargueMasivo()
    {
        return view('admin.categorias.cargue-masivo');
    }
    
    /**
     * Carga el archivo de categorias y las guarda.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     * @param Request $request
     *
     * @return View
    */
    public function cargar(Request $request)
    {
        $archivo = $request->archivo;
//        dd($archivo);
        if($archivo) {
            try {
                $reader = IOFactory::createReader(ucfirst($archivo->extension()));
//                $reader->setLoadAllSheets();
                $spreadsheet = $reader->load($archivo->path());
                $worksheet = $spreadsheet->getActiveSheet();
                $highestRow = $worksheet->getHighestRow();
                $data = $spreadsheet->getActiveSheet()
                    ->rangeToArray(
                        'A1:B' . $highestRow,
                        null,
                        true,
                        true,
                        true
                    );
//                dd($data);
                $array_categorias = [];
                foreach($data as $i => $d) {
                    if($i > 1 && $d['A']) {
                        $descripcion = $d['A'];
                        $estado = $d['B'];
                        
                        $array_categorias = [
                            'descripcion' => $descripcion,
                            'estado' => $estado,
                        ];
                        $categoria = Categoria::updateOrCreate(
                            ['descripcion' => $descripcion],
                            $array_categorias
                        );
                    }
                }
                if($categoria) {
                    $response = ['ok' => true, 'mensaje' => 'Categorías cargadas con éxito'];
                } else {
                    $response = ['ok' => false, 'mensaje' => 'Se presentaron errores en la carga'];
                }
            } catch(Exception $e) {
                $response = ['ok' => false, 'mensaje' => $e->getMessage()];
            }
        } else {
            $response = ['ok' => false, 'mensaje' => 'Seleccione un archivo para cargar las categorías'];
        }
        
        return redirect()->back()->with($response);
    }
}
