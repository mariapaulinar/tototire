<?php

namespace App\Http\Controllers\Tienda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producto;
use App\Comentario;
use DB; 
use MP;

class ProductosController extends Controller {
    
    const NRO_RESULTADOS = 21;


    /**
     * Se hace búsqueda de llantas teniendo en cuenta marca, modelo y línea.
     *
     * @author María Paulina Ramírez Vásquez <paulinaramirez@u2.com.co>
     * 
     * @param string  $marca Descripción de la marca.
     * @param string  $modelo Descripción del modelo.
     * @param string  $linea Descripción de la línea.
     *
     * @return View
     */
    public function productosVehiculo($marca, $modelos, $linea)
    {
        //dd('okokokokoko');
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfiles = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rines = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = Producto::paginate(self::NRO_RESULTADOS);
        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();

        $tipo_busqueda = 'vehiculo';
        $descripcion_busqueda ='Llantas para el vehiculo: ' . $marca . ' ' . $linea . ' '  . $modelos;
        
        //dd($productos);
        //return view('tienda.index');
        
        //Se obtienen los códigos de marca, modelo y línea del vehículo
        //a través de las descripciones enviadas en la ruta.
        //dd("entra");
        /*$table_marcas = DB::table('marcar')->where('marcar_des_marca', $marca)->first();
        $cod_marca = ($table_marcas) ? $table_marcas->marcar_cod_marca : null;
        
        $linea = explode('_', $linea);
        $linea = implode(' ', $linea);
        $linea = explode('|', $linea);
        $linea = implode('/', $linea);
        
        $tipcar = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->where('tipcar_ano', $modelo)
                ->where('tipcar_tipo', $linea)
                ->get();
        
        $array_anchos = array();
        $array_perfiles = array();
        $array_rines = array();
        
        foreach($tipcar as $t) {
            $array_anchos[] = $t->tipcar_ancho;
            $array_perfiles[] = $t->tipcar_perfil;
            $array_rines[] = $t->tipcar_rin;
        }
        
        $sql_conten_promociones = DB::table('conten')
                ->join('marcas', 'marcas_cod_marca', 'conten_cod_marca')
                ->join('submarcas', function ($join) {
                    return $join->on('conten.conten_cod_submarca', '=', 'submarcas.submarcas_cod_submarca')
                           ->on('marcas.marcas_cod_marca', '=', 'submarcas.submarcas_cod_marca');
                })
                ->join('promocerp', 'promocerp_codigo', 'conten_cod_conten')
                ->join('tipopromoc', 'promocerp_cod_tipopromoc', 'tipopromoc_cod_tipopromoc')
                ->join('iprodu', 'iprodu.iprodu_cod_prod', '=', 'conten.conten_cod_conten')
                ->whereIn('conten_llanta_ancho', $array_anchos)
                ->whereIn('conten_llanta_perfil', $array_perfiles)
                ->whereIn('conten_llanta_rin', $array_rines)
                ->where('promocerp_cli_categ', '09')
                ->where('conten_vis_tienda', 'SI')
                ->where('promocerp_fec_desde', '<=', date('Ymd'))
                ->where('promocerp_fec_hasta', '>=', date('Ymd'))
                ->where('promocerp_cod_tipopromoc', '<>', '')
                ->where('conten_tipo_conten', config('parametros.cod_tipo_conten_unitario'))
                ->where('iprodu_estado', '<>', '9')
                ->groupBy('conten_cod_conten')
                ->orderBY('tipopromoc_orden')
                ->orderBy('marcas_cod_marca');
                
        $array_promociones = $sql_conten_promociones->pluck('conten_cod_conten')->toArray();
        $conten_promociones = $sql_conten_promociones->paginate(self::NRO_RESULTADOS);
                
        $conten = DB::table('conten')
                ->join('marcas', 'marcas_cod_marca', 'conten_cod_marca')
                ->join('submarcas', function ($join) {
                    return $join->on('conten.conten_cod_submarca', '=', 'submarcas.submarcas_cod_submarca')
                           ->on('marcas.marcas_cod_marca', '=', 'submarcas.submarcas_cod_marca');
                })
                ->join('catpre', 'catpre_cod_prod', 'conten_cod_conten')
                ->join('iprodu', 'iprodu.iprodu_cod_prod', '=', 'conten.conten_cod_conten')
                ->whereIn('conten_llanta_ancho', $array_anchos)
                ->whereIn('conten_llanta_perfil', $array_perfiles)
                ->whereIn('conten_llanta_rin', $array_rines)
                ->where('catpre_cod_cat', '09')
                ->where('conten_vis_tienda', 'SI')
                ->where('conten_tipo_conten', config('parametros.cod_tipo_conten_unitario'))
                ->where('iprodu_estado', '<>', '9');
        $conten = (count($array_promociones) > 0) ? $conten->whereNotIn('conten_cod_conten', $array_promociones) : $conten;
        $conten = $conten->groupBy('conten_cod_conten')
                ->paginate(self::NRO_RESULTADOS);
        
        //Marcas de vehículos
        $marcas_vehiculos = DB::table('marcar')
                ->get();

        //Modelos de vehículos
        $modelos_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca)
                ->groupBy('tipcar_ano')
                ->get();

        //Líneas de vehículos
        $lineas_vehiculos = DB::table('tipcar')->where('tipcar_cod_marca', $cod_marca);
        if ($modelo) {
            $lineas_vehiculos = $lineas_vehiculos->where('tipcar_ano', $modelo);
        }
        $lineas_vehiculos = $lineas_vehiculos
                ->groupBy('tipcar_tipo')
                ->get();

        //Medidas de llantas
        $anchos = DB::table('medidas')
                ->where('medidas_ancho', '>', '0')
                ->groupBy('medidas_ancho')
                ->orderBy('medidas_ancho', 'asc')
                ->get();
        
        //Marcas de llantas
        $marcas = DB::table('marcas')
                ->where('marcas_estado', config('parametros.cod_estado_activo'))
                ->limit(20)
                ->get();
        
        $perfiles = array();
        $rines = array();
        $ancho = null;
        $perfil = null;
        $rin = null;
        
        //Qué tipo de búsqueda se hizo
        $tipo_busqueda = 'vehiculo';
        $descripcion_busqueda = strtoupper($marca . '  ' . $linea . ' ' . $modelo)
                . '<br>MEDIDA: ' . $array_anchos[0] . ' ' . $array_perfiles[0] . ' R' . $array_rines[0];
        
        $marcas_seleccionadas = array();

        return view('listado')->with(compact(
                'tipo_busqueda',
                'conten',
                'marcas_vehiculos',
                'modelos_vehiculos',
                'lineas_vehiculos',
                'anchos',
                'perfiles',
                'rines',
                'ancho',
                'perfil',
                'rin',
                'marcas',
                'marcas_seleccionadas',
                'cod_marca',
                'modelo',
                'linea',
                'descripcion_busqueda',
                'conten_promociones'
        ));*/
        //dd($marcar);
        return view('tienda.productos')->with(compact('ancho', 'perfiles', 'rines', 'productos', 'marcar', 'modelo', 'tipo_busqueda','descripcion_busqueda'));
    }
    /* Listado de todas las promociones */
    public function productos ()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfiles = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rines = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = Producto::paginate(self::NRO_RESULTADOS);
        $marcar = DB::table('marcar')->select(['*'])->get();
        //dd($productos);

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();

        $tipo_busqueda = '';
        $descripcion_busqueda = 'Todas las llantas';
        
        return view('tienda.productos')->with(compact('ancho', 'perfiles', 'rines', 'productos', 'marcar', 'modelo', 'tipo_busqueda','descripcion_busqueda'));
    }

    public function productosMedida($anchos, $perfil, $rin, $marcas = null)
    {

        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfiles = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rines = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = DB::table('productos')
                    ->select(['*'])
                    ->where('ancho',$anchos)
                    ->where('perfil',$perfil)
                    ->where('rin',$rin)
                    ->paginate(self::NRO_RESULTADOS);
        //dd($productos);
        //$productos = Producto::all();
        $marcar = DB::table('marcar')->select(['*'])->get();
        //falta colocar el where
        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        $tipo_busqueda = 'medida';
        $descripcion_busqueda = 'Llantas para la medida: ' . $anchos . ' ' . $perfil . ' R' . $rin;

        return view('tienda.productos')->with(compact('ancho', 'perfiles', 'rines', 'productos', 'marcar', 'modelo','tipo_busqueda','descripcion_busqueda'));
    }

    public function detalleproducto(Request $request)
    {
        $producto = $request->producto;
        //dd($producto);
        $detalleproducto = DB::table('productos')->select(['marcas.descripcion as marca', 'productos.*'])
                           ->join('marcas', 'marcas.id', 'productos.marca_id')
                           ->where('productos.id',$producto)->first();
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = Producto::all();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        $comentarios = Comentario::where('producto_id', $producto);
        $suma_comentarios = $comentarios->sum('rating');
        $nro_comentarios = $comentarios->count();
        $promedio_comentarios = ($nro_comentarios > 0) ? $suma_comentarios/$nro_comentarios : 0;
        $comentarios = $comentarios->get();
        return view('tienda.detalle-producto')->with(compact('comentarios', 'nro_comentarios', 'promedio_comentarios', 'ancho', 'perfil', 'rin', 'marcar', 'modelo','detalleproducto', 'productos'));
        //return view('tienda.detalle-producto')->with(compact('ancho', 'perfiles', 'rines', 'productos', 'marcar', 'modelo'));
    }
     /* Listado de todas las promociones */
    public function productosPromocion ()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfiles = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rines = DB::table('productos')->select(['rin'])->groupBy('rin')->get();
        $productos = Producto::paginate(self::NRO_RESULTADOS);
        $marcar = DB::table('marcar')->select(['*'])->get();
        //dd($productos);

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        return view('tienda.productosPromocion')->with(compact('ancho', 'perfiles', 'rines', 'productos', 'marcar', 'modelo'));
    }
    
    public function guardarCalificacion(Request $request)
    {
        $producto = Producto::find($request->producto_id);
        
        if($producto) {
            $comentario = new Comentario();
            
            $comentario->producto_id = $producto->id;
            $comentario->nombres = $request->nombres;
            $comentario->apellidos = $request->apellidos;
            $comentario->email = $request->email;
            $comentario->rating = $request->rating;
            $comentario->comentario = $request->comentario;
            $comentario->fecha = date('Y-m-d H:i:s');
            
            $comentario->save();
        }
        
        return redirect()->back();
    }
}
