<?php

namespace App\Http\Controllers\Tienda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pedido;
use DB; 

class PedidosController extends Controller {

    public function carrito(Request $request)
    {
        switch($request->accion) {
            case 'agregar':
                $productos = $this->agregar($request);
                break;
            case 'borrar':
                $productos = $this->borrar($request);
                break;
            default:
                $productos = Pedido::obtenerProductosCarrito();
        }

        return view('tienda.carrito');
    }
    
    public function validarDisponibilidad(Request $request)
    {

        $id = $request->cod_prod;
        $cantidad = $request->cantidad;
        $producto = DB::table('productos')->where('id', $id)->first();
        
        if (!$producto) {
            return [
                'ok' => false,
                'mensaje' => 'No existe el producto'
            ];
        }

        $existencia = (int) $producto->existencia;

        if ($existencia < $cantidad) {
            $response = [
                'ok' => false,
                'mensaje' => 'No hay disponibilidad para la cantidad pedida.'
            ];
        } else {
            $response = [
                'ok' => true,
                'mensaje' => ''
            ];
        }

        return response()->json($response);
    }

     /**
     * Agrega un producto al carrito de compras.
     *
     * @author María Paulina Ramírez Vásquez    <paulinaramirez@u2.com.co>
     * @param Illuminate\Http\Request $request Datos envidos en petición.
     *
     * @return Array
    */
    public function agregar(Request $request)
    {
        $productos = Pedido::obtenerProductosCarrito();
        $producto = DB::table('productos')->where('id', $request->cod_prod)->first();
        if($producto) {
            $productos[$producto->id] = [
                        'foto' => $producto->foto_principal,
                        'descripcion' => $producto->descripcion,
                        'cantidad' => $request->cantidad,
                        'precio_venta' => $producto->precio_venta,
                    ];
            
            session(['productos' => $productos]);
        }
        
        return $productos;
    }
    
    
    /**
     * Borra un producto del carrito de compras.
     *
     * @author María Paulina Ramírez Vásquez    <paulinaramirez@u2.com.co>
     * @param Illuminate\Http\Request $request Datos envidos en petición.
     *
     * @return Array
    */
    public function borrar(Request $request)
    {
        $productos = Pedido::obtenerProductosCarrito();
        unset($productos[$request->cod_conten]);
        session(['productos' => $productos]);
        //dd($productos);
        
        return $productos;
    }
    

    public function detallecarrito()
    {

        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.detalle-carrito')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }

    public function finalizarpedido()
    {
        $ancho = DB::table('productos')->select(['ancho'])->groupBy('ancho')->get();
        $perfil = DB::table('productos')->select(['perfil'])->groupBy('perfil')->get();
        $rin = DB::table('productos')->select(['rin'])->groupBy('rin')->get();

        $marcar = DB::table('marcar')->select(['*'])->get();
        

        $modelo = DB::table('tipcar')
              ->select(['tipcar_ano'])
              ->groupBy('tipcar_ano' )
              ->orderBy('tipcar_ano','DESC')
              ->get();
        //dd($productos);
        //return view('tienda.index');
       return view('tienda.finalizar-pedido')->with(compact('ancho', 'perfil', 'rin', 'marcar', 'modelo'));
       
    }

}
