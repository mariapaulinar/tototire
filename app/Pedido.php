<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Pedido extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
            'fecha_hora',
            'user_id_compra',
            'cantidad_total',
            'valor_total',
            'valor_iva_total',
            'observaciones',
            'estado',
            'comentarios',
            'calificacion',
        ];
    
    protected $dates = [
        'fecha_hora',
    ];
    
        
    /**
     * Obtiene el modelo User asociado al pedido.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\User
    */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id_compra');
    }
    
    /**
     * Obtiene el modelo Producto asociado al pedido.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return App\Producto
    */
    public function productos()
    {
        return $this->belongsToMany('App\Producto', 'pedidos_productos', 'pedido_id', 'producto_id');
    }
    
    /**
     * Obtiene productos que se han agregado al carrito de compras.
     *
     * @author María Paulina Ramírez Vásquez <paulinaramirez@u2.com.co>
     *
     * @return Array
    */
    public static function obtenerProductosCarrito()
    {
//        var_dump(session()->get('productos'));
        //Verificar si hay productos ya agregados en la sesión.
        if(session()->has('productos')) {
            $productos = session()->get('productos');
        } else {
            session(['productos' => array()]);
            $productos = session()->get('productos');
        }
        
        return $productos;
    }
}
