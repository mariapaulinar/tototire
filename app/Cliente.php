<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cliente extends Model
{
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_modificacion';
    
    protected $fillable = [
        'name', 'email',
    ];
    
    protected $dates = [
        'fecha_creacion',
        'fecha_modificacion',
    ];
    
    /**
     * Obtiene antigüedad del cliente.
     *
     * @author María Paulina Ramírez Vásquez    <maria.paulina.r.v@gmail.com>
     *
     * @return string
    */
    public function getAntiguedadAttribute()
    {
        $dias = $this->fecha_creacion->diffInDays(Carbon::now());
        $res = ($dias == 1) ? ' día' : ' días';
        
        if($dias > 7) {
            $res = $this->fecha_creacion->diffInWeeks(Carbon::now());
            $res .= ($res == 1) ? ' semana' : ' semanas';
        }
        
        if($dias > 30) {
            $res = $this->fecha_creacion->diffInMonths(Carbon::now());
            $res .= ($res == 1) ? ' mes' : ' meses';
        }
        
        if($dias > 365) {
            $res = $this->fecha_creacion->diffInYears(Carbon::now());
            $res .= ($res == 1) ? ' año' : ' años';
        }
        
        return $res;
    }
}
